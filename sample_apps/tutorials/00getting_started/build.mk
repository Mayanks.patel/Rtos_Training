exec-y += smart_plug

smart_plug-objs-y := src/main.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

smart_plug-supported-toolchain-y := arm_gcc iar
