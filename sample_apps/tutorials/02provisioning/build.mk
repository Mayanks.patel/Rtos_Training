exec-y += 02provisioning

02provisioning-objs-y := src/main.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

02provisioning-supported-toolchain-y := arm_gcc iar
