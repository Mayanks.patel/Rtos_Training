/*
 *  Copyright (C) 2008-2016 Marvell International Ltd.
 *  All Rights Reserved.
 */
#include <cli.h>
#include <rfget.h>
#include "smart_plug.h"

/* Include the CA certificate for validating the github.com server's
 * certificate */
#include "github-ca-cert.pem.c"

static httpc_cfg_t fw_upg_httpc_cfg;
static tls_client_t fw_upg_tls_cfg;

static httpc_cfg_t *get_my_http_cfg()
{
	/* Create TLS Configuration: In this configuration, we basically
	 * instruct the TLS stack to perform the server certificate's
	 * authentication. For this purpose, the CA certificate, that has
	 * signed the server's certificate is programmed into the device.
	 */
	memset(&fw_upg_tls_cfg, 0x00, sizeof(fw_upg_tls_cfg));
	fw_upg_tls_cfg.ca_cert = github_ca_cert;
	fw_upg_tls_cfg.ca_cert_size = github_ca_cert_size - 1;

	/* From TLS Configuration, create SSL Context */
	fw_upg_httpc_cfg.ctx = tls_create_client_context(&fw_upg_tls_cfg);
	if (!fw_upg_httpc_cfg.ctx)
		return NULL;

	/* If server authentication should NOT be performed, this function can
	 * return NULL, else it must return the appropriate HTTP configuration
	 * structure for performing server authentication.
	 */
	return &fw_upg_httpc_cfg;
}

/* This is the function that will be called by any entity that gets a firmware
 * upgrade command (typically a cloud agent). The URL contains the upgraded
 * firmware image.
 */
static int upgrade_my_firmware(const char *url)
{
	struct partition_entry *p = rfget_get_passive_firmware();
	httpc_cfg_t *httpc_cfg = get_my_http_cfg();
	if (!httpc_cfg) {
		wmprintf("[fw_upg] No valid httpc context\r\n");
		return -WM_FAIL;
	}

	/* If you are still setting up your servers, you could pass NULL instead
	 * of httpc_cfg in the following call. This will disable the server
	 * certificate authentication. Please note that this should be used only
	 * and only in the development mode, not for the release builds.
	 */
	int ret = rfget_client_mode_update(url, p, httpc_cfg);
	if (ret == WM_SUCCESS) {
		wmprintf("[fw_upg] Firmware update succeeded\r\n");
	} else {
		wmprintf("[fw_upg] Firmware update failed\r\n");
	}
	return ret;
}

static void cmd_fw_upg(int argc, char **argv)
{
	const char *url_str = argv[1];

	if (argc != 2) {
		wmprintf("Usage: %s <http-url>\r\n", argv[0]);
		wmprintf("Invalid number of arguments.\r\n");
		return;
	}
	upgrade_my_firmware(url_str);
}

static struct cli_command fw_upg_command = {"fw_upg", "<http_url>", cmd_fw_upg};

int fw_upgrade_cli_init()
{
	return cli_register_command(&fw_upg_command);
}
