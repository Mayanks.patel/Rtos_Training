exec-y                       += 07led_pushbutton
07led_pushbutton-objs-y      := src/main.c src/plug_handlers.c src/fw_upgrade.c

07led_pushbutton-ftfs-y      := 07led_pushbutton.ftfs
07led_pushbutton-ftfs-dir-y  := $(d)/www
07led_pushbutton-ftfs-api-y  := 100

07led_pushbutton-supported-toolchain-y := arm_gcc iar

ifneq ($(XIP),1)
  $(error This app can only be built with XIP=1)
endif

ifneq ($(CONFIG_ENABLE_HTTPC_SECURE),y)
  $(error Please enable 'HTTPS support' in 'HTTP Client' from SDK configuration)
endif
