/*
 *  Copyright (C) 2008-2016 Marvell International Ltd.
 *  All Rights Reserved.
 */
/* Tutorial 6: Firmware Upgrades
 *
 * Create a function for managing firmware upgrades. Currently can be triggered
 * from a CLI.
 */
#include <wmstdio.h>
#include <cli.h>
#include <psm-utils.h>
#include <wmtime.h>
#include <app_framework.h>
#include "smart_plug.h"

#define PLUG_TXT_SIZE		32
#define MDNS_DOMAIN_NAME	"local"
#define MDNS_HOSTNAME		"Plug_hostname"
#define MDNS_SERVNAME		"Plug_service"

static char plug_txt[PLUG_TXT_SIZE];

static struct mdns_service plug_service = {
	.servname = MDNS_SERVNAME,
	.servtype = "http",
	.domain = MDNS_DOMAIN_NAME,
	.proto = MDNS_PROTO_TCP,
	.port = 80,
};

/* Handle Critical Error Handlers */
void critical_error_handler(void *data)
{
	while (1)
		;
	/* do nothing -- stall */
}

/*
 * Event: DHCP_RENEW
 *
 * DHCP Lease has been renewed. Reannounce our mDNS service, in case the IP
 * Address changed. Services must be reannounced if any mDNS rdata changes.
 */
static void event_normal_dhcp_renew(void *data)
{
	void *iface_handle = net_get_sta_handle();
	mdns_reannounce_service_all(iface_handle);
}

/*
 * Handler invoked on WLAN_INIT_DONE event.
 */
static void event_wlan_init_done(void *data)
{
	/* We receive provisioning status in data */
	int provisioned = (int)data;

	wmprintf("Event: WLAN_INIT_DONE provisioned=%d\r\n", provisioned);

	if (provisioned) {
		wmprintf("Starting station\r\n");
		app_sta_start();
	} else {
		wmprintf("Not provisioned\r\n");
#define PROV_SSID   "Smart Plug"
#define PROV_PIN    "device_pin"
		app_ezconnect_provisioning_start(PROV_SSID,
			 (unsigned char *)PROV_PIN, strlen(PROV_PIN));
	}

	if (app_httpd_only_start() != WM_SUCCESS)
		wmprintf("Error: Failed to start HTTPD");

	register_httpd_plug_handlers();
#define FTFS_API_VERSION    100
#define FTFS_PART_NAME	    "ftfs"
	static struct fs *fs_handle;
	app_ftfs_init(FTFS_API_VERSION, FTFS_PART_NAME, &fs_handle);

	mdns_start(MDNS_DOMAIN_NAME, MDNS_HOSTNAME);
}

static void event_normal_connected(void *data)
{
	void *iface_handle;
	static bool mdns_announced;
	char ip[16];

	app_network_ip_get(ip);
	wmprintf("Connected to Home Network with IP address = %s", ip);

	iface_handle = net_get_sta_handle();
	if (!mdns_announced) {
		snprintf(plug_txt, PLUG_TXT_SIZE, "api_ver=2|model=ABC");
		mdns_set_txt_rec(&plug_service, plug_txt, '|');
		mdns_announce_service(&plug_service, iface_handle);
		mdns_announced = true;
	} else {
		/* If device reconnects to network after any network
		 * abruptions it must re-probe and announce services again */
		mdns_announce_service_all(iface_handle);
	}

	int ret = psm_cli_init(sys_psm_get_handle(), NULL);
	if (ret != WM_SUCCESS)
		wmprintf("Error: psm_cli_init failed\r\n");

}

/*
 * Event: PROV_DONE
 *
 * Provisioning is complete. We can stop the provisioning
 * service.
 */
static void event_prov_done(void *data)
{
	wmprintf("Provisioning Completed");
	app_ezconnect_provisioning_stop();
}

/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_NORMAL_CONNECTED:
		event_normal_connected(data);
		break;
	case AF_EVT_PROV_DONE:
		event_prov_done(data);
		break;
	case AF_EVT_NORMAL_DHCP_RENEW:
		event_normal_dhcp_renew(data);
		break;
	default:
		break;
	}

	return WM_SUCCESS;
}

int main()
{
	wmstdio_init(UART0_ID, 0);

	cli_init();

	wmtime_init();
	wmtime_cli_init();
	/* Set the system time to 1 Sept 2016. This is required for firmware
	 * upgrade server's certificate validation.
	 * - Your hardware could be designed to either preserve time counters
	 *   across power cut-offs OR
	 * - You could fetch the current time from a remote server
	 */
	wmtime_time_set_posix(1472688000);

	fw_upgrade_cli_init();

	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		wmprintf("Failed to start application framework");
		critical_error_handler((void *) -WM_FAIL);
	}

	return 0;
}
