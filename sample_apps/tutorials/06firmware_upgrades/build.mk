exec-y                          += 06firmware_upgrades
06firmware_upgrades-objs-y      := src/main.c src/plug_handlers.c src/fw_upgrade.c

06firmware_upgrades-ftfs-y      := 06firmware_upgrades.ftfs
06firmware_upgrades-ftfs-dir-y  := $(d)/www
06firmware_upgrades-ftfs-api-y  := 100

06firmware_upgrades-supported-toolchain-y := arm_gcc iar

ifneq ($(XIP),1)
  $(error This app can only be built with XIP=1)
endif

ifneq ($(CONFIG_ENABLE_HTTPC_SECURE),y)
  $(error Please enable 'HTTPS support' in 'HTTP Client' from SDK configuration)
endif
