exec-y                      += 05local_control
05local_control-objs-y      := src/main.c src/plug_handlers.c

05local_control-ftfs-y      := 05local_control.ftfs
05local_control-ftfs-dir-y  := $(d)/www
05local_control-ftfs-api-y  := 100

05local_control-supported-toolchain-y := arm_gcc iar
