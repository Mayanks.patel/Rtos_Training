exec-y += 01app_framework

01app_framework-objs-y := src/main.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

01app_framework-supported-toolchain-y := arm_gcc iar
