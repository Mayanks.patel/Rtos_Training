exec-y                       += 08mfg_partition
08mfg_partition-objs-y      := src/main.c src/plug_handlers.c src/fw_upgrade.c

08mfg_partition-ftfs-y      := 08mfg_partition.ftfs
08mfg_partition-ftfs-dir-y  := $(d)/www
08mfg_partition-ftfs-api-y  := 100

08mfg_partition-mfg-cfg-y   := config.mfg

08mfg_partition-supported-toolchain-y := arm_gcc iar

ifneq ($(XIP),1)
  $(error This app can only be built with XIP=1)
endif

ifneq ($(CONFIG_ENABLE_HTTPC_SECURE),y)
  $(error Please enable 'HTTPS support' in 'HTTP Client' from SDK configuration)
endif
