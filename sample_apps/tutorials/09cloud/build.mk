exec-y              += 09cloud
09cloud-objs-y      := src/main.c src/plug_handlers.c src/fw_upgrade.c src/aws_cloud.c

09cloud-ftfs-y      := 09cloud.ftfs
09cloud-ftfs-dir-y  := $(d)/www
09cloud-ftfs-api-y  := 100

09cloud-mfg-cfg-y   := config.mfg

09cloud-supported-toolchain-y := arm_gcc iar

ifneq ($(XIP),1)
  $(error This app can only be built with XIP=1)
endif

ifneq ($(CONFIG_ENABLE_HTTPC_SECURE),y)
  $(error Please enable 'HTTPS support' in 'HTTP Client' from SDK configuration)
endif
