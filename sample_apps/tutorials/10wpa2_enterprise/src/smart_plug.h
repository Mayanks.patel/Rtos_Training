/*
 *  Copyright (C) 2008-2016 Marvell International Ltd.
 *  All Rights Reserved.
 */
#ifndef _SMART_PLUG_APP_H_
#define _SMART_PLUG_APP_H_

int register_httpd_plug_handlers();
int fw_upgrade_cli_init();
void configure_push_button();
void aws_cloud_start();
void notify_cloud();

bool plug_get_state();
void plug_set_state(bool new_state);

#endif /* ! _SMART_PLUG_APP_H_ */
