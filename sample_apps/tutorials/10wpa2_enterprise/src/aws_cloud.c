/*
 *  Copyright (C) 2008-2016 Marvell International Ltd.
 *  All Rights Reserved.
 */

#include <wm_os.h>
#include <wmstdio.h>
#include <wlan.h>
#include <aws_iot_mqtt_client_interface.h>
#include <aws_iot_shadow_interface.h>
#include "smart_plug.h"

#include "aws_starter_root_ca_cert.h"

/************************************************************/
/* AWS Configurable Items: Please configure these items as per your AWS IoT
 * configuration */
#define AWS_REGION     "us-west-2"
#define AWS_THING_NAME "sdktutorial"
#define AWS_THING_CERT   "-----BEGIN CERTIFICATE-----\n\
CHANGE_THIS_TO_YOUR_THING_CERTIFICATE\n\
-----END CERTIFICATE-----\n\
"
#define AWS_THING_KEY "-----BEGIN RSA PRIVATE KEY-----\n\
CHANGE_THIS_TO_YOUR_THING_KEY\n\
-----END RSA PRIVATE KEY-----\n\
"

/* Create the AWS Key and Certificate from your AWS IoT configuration and then
 * populate them above. Once done, remove this following line.
 */
#error Please copy the AWS Certificate-Key from your AWS-IoT configuration

/************************************************************/

/* AWS Global Variables */
static os_thread_t aws_thread_handle;
static os_thread_stack_define(aws_stack, 12 * 1024);
static AWS_IoT_Client mqtt_client;

/* Derive URL from the AWS region string */
#define AWS_URL       "data.iot."AWS_REGION".amazonaws.com"

static int aws_init_params_init(ShadowInitParameters_t *sip)
{
	memset(sip, 0, sizeof(*sip));
	sip->pHost               = AWS_URL;
	sip->port                = AWS_IOT_MQTT_PORT;
	sip->enableAutoReconnect = true;
	sip->disconnectHandler   = NULL;
	sip->pClientCRT          = AWS_THING_CERT;
	sip->pClientKey          = AWS_THING_KEY;
	sip->pRootCA             = aws_rootCA;
	return WM_SUCCESS;
}

static void aws_init_params_free(ShadowInitParameters_t *sip)
{
	/* No need to free() anything, since we aren't allocating anything */
	memset(sip, 0, sizeof(*sip));
}

static int aws_connect_params_init(ShadowConnectParameters_t *scp)
{
#define MAC_ADDRESS_SIZE   6
	uint8_t device_mac[MAC_ADDRESS_SIZE];
	wlan_get_mac_address(device_mac);

	memset(scp, 0, sizeof(*scp));
	scp->pMyThingName    = AWS_THING_NAME;
	scp->pMqttClientId   = os_mem_alloc(MAX_SIZE_OF_UNIQUE_CLIENT_ID_BYTES);
	if (scp->pMqttClientId == NULL) {
		wmprintf("Couldn't allocate connection params\r\n");
		return -WM_FAIL;
	}
	snprintf(scp->pMqttClientId, MAX_SIZE_OF_UNIQUE_CLIENT_ID_BYTES,
		 "%s-%02x%02x%02x%02x%02x%02x", AWS_IOT_MQTT_CLIENT_ID,
		 device_mac[0], device_mac[1], device_mac[2],
		 device_mac[3], device_mac[4], device_mac[5]);
	scp->mqttClientIdLen = (uint16_t) strlen(scp->pMqttClientId);
	return WM_SUCCESS;
}

static void aws_connect_params_free(ShadowConnectParameters_t *scp)
{
	if (scp->pMqttClientId)
		os_mem_free(scp->pMqttClientId);
	memset(scp, 0, sizeof(*scp));
}

/* Publishing stuff */
static void publish_device_state(AWS_IoT_Client *mqtt_cp,
				 ShadowConnectParameters_t *scp)
{
#define BUFSIZE 100
#define JSON_POWER_VAR  "power"
	char buf[BUFSIZE];
	bool state = plug_get_state();

	snprintf(buf, sizeof(buf),
		 "{\"state\":{\"reported\":{\""JSON_POWER_VAR"\": %d},"
			"\"desired\":{\""JSON_POWER_VAR"\": %d}}}",
		 state, state);
	wmprintf("Publishing to cloud: %s\r\n", buf);

	int ret = aws_iot_shadow_update(mqtt_cp, scp->pMyThingName, buf,
					NULL, NULL, 10, true);
	if (ret != WM_SUCCESS) {
		wmprintf("Publish failed\r\n");
	}
}

static bool plug_state_changed;
static bool state_changed()
{
	bool last_state = plug_state_changed;
	plug_state_changed = false;
	return last_state;
}

/* The plug handlers will notify through this function whether the plug state
 * has changed.
 */
void notify_cloud()
{
	plug_state_changed = true;
}

static unsigned long get_cmd_timestamp(const char *p_json_string)
{
	/* To extract timestamp value, parsing string in the form:
	 * 1},"metadata":{"led":{"timestamp":1460703773}}}
	 */
	unsigned long time;
#define NEEDLE "\"timestamp\":"
	char *ptr = strstr(p_json_string, NEEDLE);
	ptr += strlen(NEEDLE);
	sscanf(ptr, "%lu}}}", &time);
	return time;
}

/* Subscription stuff */
static void plug_cmd_cb(const char *p_json_string, uint32_t json_string_datalen,
		 jsonStruct_t *p_context)
{
	wmprintf("Callback function invoked with state change to %d...",
		 *(int *)(p_context->pData));

	static unsigned long processed_timestamp;
	unsigned long cmd_timestamp = get_cmd_timestamp(p_json_string);
	/* Not updating device state if delta timestamp is already processed */
	if (cmd_timestamp <= processed_timestamp) {
		wmprintf("Ignored (stale)\r\n");
		return;
	}
	wmprintf("Processed\r\n");
	processed_timestamp = cmd_timestamp;
	plug_set_state(*(int *)(p_context->pData));
}

static int manage_subscriptions(AWS_IoT_Client *mqtt_cp)
{
	static jsonStruct_t plug_cmds;
	static int store;
	memset(&plug_cmds, 0, sizeof(plug_cmds));
	plug_cmds.cb     = plug_cmd_cb;
	plug_cmds.pData  = &store;
	plug_cmds.pKey   = JSON_POWER_VAR;
	plug_cmds.type   = SHADOW_JSON_INT8;
	int ret = aws_iot_shadow_register_delta(mqtt_cp, &plug_cmds);
	if (ret != AWS_SUCCESS) {
		wmprintf("Failed to subscribe to shadow delta %d\r\n", ret);
	}
	return WM_SUCCESS;
}

static void aws_thread(os_thread_arg_t data)
{
	int ret;

	ShadowInitParameters_t sip;
	aws_init_params_init(&sip);

	ShadowConnectParameters_t scp;
	aws_connect_params_init(&scp);

	/* Initialize AWS IoT */
	ret = aws_iot_shadow_init(&mqtt_client, &sip);
	if (ret != WM_SUCCESS) {
		wmprintf("aws shadow init failed: %d\r\n", ret);
		goto out;
	}

	/* Connect to AWS Shadow */
	ret = aws_iot_shadow_connect(&mqtt_client, &scp);
	if (ret != WM_SUCCESS) {
		wmprintf("aws shadow connect failed: %d\r\n", ret);
		goto out;
	}
	ret = aws_iot_shadow_set_autoreconnect_status(&mqtt_client, true);
	if (ret != WM_SUCCESS) {
		wmprintf("Unable to set Auto Reconnect to true : %d", ret);
		goto out;
	}

	/* Handle subscription to events */
	manage_subscriptions(&mqtt_client);

	/* Publish first state */
	publish_device_state(&mqtt_client, &scp);
	while (1) {
		ret = aws_iot_shadow_yield(&mqtt_client, 200);
		if (ret != AWS_SUCCESS) {
			wmprintf("AWS IoT shadow yield failure %d\r\n", ret);
			os_thread_sleep(os_msec_to_ticks(5000));
			continue;
		}

		/* Update cloud if something changed */
		if (state_changed()) {
			publish_device_state(&mqtt_client, &scp);
		}

		os_thread_sleep(os_msec_to_ticks(1000));
	}
out:
	aws_iot_shadow_disconnect(&mqtt_client);
	aws_connect_params_free(&scp);
	aws_init_params_free(&sip);
	os_thread_self_complete(NULL);
	return;
}

void aws_cloud_start()
{
	/* Start the cloud thread */
	int ret = os_thread_create(
			/* thread handle */
			&aws_thread_handle,
			/* thread name */
			"awsStarterDemo",
			/* entry function */
			aws_thread,
			/* argument */
			0,
			/* stack */
			&aws_stack,
			/* priority */
			OS_PRIO_3);
	if (ret != WM_SUCCESS) {
		wmprintf("Failed to start cloud_thread: %d\r\n", ret);
	}
}
