/*
 *  Copyright (C) 2008-2016 Marvell International Ltd.
 *  All Rights Reserved.
 */
/* Tutorial 7: LED and Push-button
 *
 * Implement LED indicators based on connectivity status.
 * Implement LED indication for Plug State.
 * Implement push-button based control for Plug State.
 * Implement push-button for reset to factory handling.
 */
#include <wmstdio.h>
#include <cli.h>
#include <psm-utils.h>
#include <wmtime.h>
#include <led_indicator.h>
#include <app_framework.h>
#include <mfg_psm.h>
#include <cli_utils.h>

#include "smart_plug.h"
#include "ca-cert.h"

/* psm details   */
/* Max WPA2 passphrase can be upto 64 ASCII chars */
#define MAX_PSM_VAL		65
#define PART_NAME		"refpart"
#define DEF_NET_NAME		"default"
#define VAR_NET_SSID		"network.ssid"
#define VAR_NET_BSSID		"network.bssid"
#define VAR_NET_CHANNEL		"network.channel"
#define VAR_NET_SECURITY	"network.security"
#define VAR_NET_PASSPHRASE	"network.passphrase"
#define VAR_NET_POLLINT		"pollinterval"
#define VAR_NET_SENSORNAME	"sensorname"
#define VAR_NET_CONFIGURED	"network.configured"
#define VAR_NET_LAN		"network.lan"
#define VAR_NET_IP_ADDRESS	"network.ip_address"
#define VAR_NET_SUBNET_MASK	"network.subnet_mask"
#define VAR_NET_GATEWAY		"network.gateway"
#define VAR_NET_DNS1		"network.dns1"
#define VAR_NET_DNS2		"network.dns2"
#define DEF_BSSID		"00:00:00:00:00:00"
#define LAN_TYPE_STATIC		"STATIC"
#define LAN_TYPE_AUTOIP		"AUTOIP"
#define LAN_TYPE_DHCP		"DYNAMIC"


#define PLUG_TXT_SIZE		32
#define MDNS_DOMAIN_NAME	"local"
#define MDNS_HOSTNAME		"Plug_hostname"
#define MDNS_SERVNAME		"Plug_service"
#define MFG_NAME_CURRENT_TIME   "current_time"
#define MFG_NAME_IDENTITY	"identity"
#define MFG_NAME_CLIENT_CERT	"client_cert"
#define MFG_NAME_CLIENT_KEY	"client_key"


#define PLUG_TXT_SIZE		32
#define MDNS_DOMAIN_NAME	"local"
#define MDNS_HOSTNAME		"Plug_hostname"
#define MDNS_SERVNAME		"Plug_service"

static char plug_txt[PLUG_TXT_SIZE];
static int identity_len, client_cert_len, client_key_len;
static char identity[32], client_cert[2048], client_key[2048];

static struct mdns_service plug_service = {
	.servname = MDNS_SERVNAME,
	.servtype = "http",
	.domain = MDNS_DOMAIN_NAME,
	.proto = MDNS_PROTO_TCP,
	.port = 80,
};

/* Handle Critical Error Handlers */
void critical_error_handler(void *data)
{
	while (1)
		;
	/* do nothing -- stall */
}

/*
 * Event: DHCP_RENEW
 *
 * DHCP Lease has been renewed. Reannounce our mDNS service, in case the IP
 * Address changed. Services must be reannounced if any mDNS rdata changes.
 */
static void event_normal_dhcp_renew(void *data)
{
	void *iface_handle = net_get_sta_handle();
	mdns_reannounce_service_all(iface_handle);
}

static void get_prov_pin(char *prov_pin, int *max_curr_len)
{
#define MFG_NAME_PROV_PIN  "prov_pin"
	int pin_len = mfg_get_variable(MFG_NAME_PROV_PIN, prov_pin,
					*max_curr_len);
	if (pin_len <= 0) {
		/* If mfg partition doesn't have the provisioning pin,
		 * use a default value
		 */
#define DEFAULT_PROV_PIN "device_pin"
		strncpy(prov_pin, DEFAULT_PROV_PIN, *max_curr_len);
		pin_len = strlen(prov_pin);
		wmprintf("[warn]: Using default prov pin\r\n");
	}
	*max_curr_len = pin_len;
	wmprintf("prov_pin = %s, len = %d\r\n", prov_pin, *max_curr_len);
}

/* Get the currently configured wireless network from the PSM */
int app_network_get_nw(struct wlan_network *network)
{
	char psm_val[MAX_PSM_VAL];

	memset(network, 0, sizeof(*network));
	memcpy(network->name, DEF_NET_NAME, strlen(DEF_NET_NAME) + 1);

	/*
	 * Perform error handling only for those fields that are a must. For
	 * other fields, make safe assumptions if the information doesn't exist
	 * in psm.
	 */
	int ret = psm_get_variable_str(sys_psm_get_handle(), VAR_NET_SSID,
				   network->ssid, sizeof(network->ssid));

	/*
	 * Please do not set BSSID during Wi-Fi Certification process.
	 */
	ret = psm_get_variable_str(sys_psm_get_handle(), VAR_NET_BSSID,
			       psm_val, sizeof(psm_val));

	if (ret > 0)
		get_mac(psm_val, network->bssid, ':');
	else
		get_mac(DEF_BSSID, network->bssid, ':');

	/*
	 * Please do not set channel during Wi-Fi Certification process.
	 */
	ret = psm_get_variable_str(sys_psm_get_handle(), VAR_NET_CHANNEL,
			 psm_val, sizeof(psm_val));

	if (ret > 0)
		network->channel = atoi(psm_val);
	else
		network->channel = 0;

	network->type = WLAN_BSS_TYPE_STA;
	network->role = WLAN_BSS_ROLE_STA;

	int var_size;
	var_size = psm_get_variable_str(sys_psm_get_handle(), VAR_NET_SECURITY,
			       psm_val, sizeof(psm_val));
	if (var_size > 0) {
		ret = 0;
	} else
		ret = var_size;

	network->security.type = (enum wlan_security_type)atoi(psm_val);
	if (network->security.type != WLAN_SECURITY_NONE) {
		if (network->security.type != WLAN_SECURITY_WEP_OPEN &&
			network->security.type != WLAN_SECURITY_WEP_SHARED) {
			if (network->security.type == WLAN_SECURITY_EAP_TLS) {
				if (identity_len > 0 && ca_der_len > 0 &&
				client_cert_len > 0 && client_key_len > 0) {
					memcpy(network->identity, identity,
							identity_len);
					network->security.tls_cfg.ca_cert =
						ca_der;
					network->security.tls_cfg.
						ca_cert_size = ca_der_len;
					network->security.tls_cfg.
						ca_cert_type =
							SSL_FILETYPE_ASN1;
					network->security.tls_cfg.client_cert =
						(unsigned char *) client_cert;
					network->security.tls_cfg.
						client_cert_size =
							client_cert_len;
					network->security.tls_cfg.
						client_cert_type =
							SSL_FILETYPE_ASN1;
					network->security.tls_cfg.
						client_key =
						(unsigned char *) client_key;
					network->security.tls_cfg.
						client_key_size =
							client_key_len;
					network->security.tls_cfg.
						client_key_type =
							SSL_FILETYPE_ASN1;
				} else
					ret = -1;
			} else {
				var_size = psm_get_variable_str(
						sys_psm_get_handle(),
						VAR_NET_PASSPHRASE,
						network->security.psk,
						sizeof(network->security.psk));
				if (var_size <= 0)
					ret |= var_size;
				network->security.psk_len =
					strlen(network->security.psk);
			}
		} else {
			/* Converting the wep key written in hex back to binary
			   form */
			var_size = psm_get_variable_str(sys_psm_get_handle(),
					VAR_NET_PASSPHRASE,
					psm_val, sizeof(psm_val));
			if (var_size <= 0)
				ret |= var_size;
			network->security.psk_len =
				hex2bin((unsigned char *)psm_val,
					(unsigned char *)network->security.psk,
					sizeof(psm_val));
		}
	}
	/* Connect specific to the SSID */
	network->ssid_specific = 1;

	psm_val[0] = 0;
	var_size = psm_get_variable_str(sys_psm_get_handle(), VAR_NET_LAN,
				    psm_val, sizeof(psm_val));
	if (!strcasecmp(psm_val, LAN_TYPE_STATIC)) {

		network->ip.ipv4.addr_type = ADDR_TYPE_STATIC;

		psm_val[0] = 0;
		var_size = psm_get_variable_str(sys_psm_get_handle(),
			VAR_NET_IP_ADDRESS,
				 psm_val, sizeof(psm_val));
		network->ip.ipv4.address = net_inet_aton(psm_val);

		psm_val[0] = 0;
		var_size = psm_get_variable_str(sys_psm_get_handle(),
			VAR_NET_SUBNET_MASK,
			psm_val, sizeof(psm_val));
		network->ip.ipv4.netmask = net_inet_aton(psm_val);

		psm_val[0] = 0;
		var_size = psm_get_variable_str(sys_psm_get_handle(),
						VAR_NET_GATEWAY,
						psm_val, sizeof(psm_val));
		network->ip.ipv4.gw = net_inet_aton(psm_val);

		psm_val[0] = 0;
		var_size = psm_get_variable_str(sys_psm_get_handle(),
						VAR_NET_DNS1,
						psm_val, sizeof(psm_val));
		network->ip.ipv4.dns1 = net_inet_aton(psm_val);

		psm_val[0] = 0;
		var_size = psm_get_variable_str(sys_psm_get_handle(),
						VAR_NET_DNS2,
						psm_val, sizeof(psm_val));
		network->ip.ipv4.dns2 = net_inet_aton(psm_val);
	} else if (!strcasecmp(psm_val, LAN_TYPE_AUTOIP))
		network->ip.ipv4.addr_type = ADDR_TYPE_LLA;
	else
		network->ip.ipv4.addr_type = ADDR_TYPE_DHCP;

	if (ret != 0)
		ret = -WM_E_AF_NW_RD;
	return ret;
}

/*
 * Handler invoked on WLAN_INIT_DONE event.
 */
static void event_wlan_init_done(void *data)
{
	/* We receive provisioning status in data */
	int provisioned = (int)data;

	wmprintf("Event: WLAN_INIT_DONE provisioned=%d\r\n", provisioned);

	/* Allow only WPA2 Enterprise AP in scan results and
	 * station network connection
	 */
	wlan_enable_wpa2_enterprise_ap_only();

	identity_len = mfg_get_variable(MFG_NAME_IDENTITY, identity,
				sizeof(identity));
	if (identity_len > 0) {
		identity[identity_len] = '\0';
	}

	client_cert_len = mfg_get_variable(MFG_NAME_CLIENT_CERT, client_cert,
				sizeof(client_cert));
	client_key_len = mfg_get_variable(MFG_NAME_CLIENT_KEY, client_key,
				sizeof(client_key));

	if (provisioned) {
		wmprintf("Starting station\r\n");
		app_sta_start();
	} else {
		wmprintf("Not provisioned\r\n");
#define PROV_SSID   "Smart Plug"
#define MAX_PROV_PIN_LEN    16
		char prov_pin[MAX_PROV_PIN_LEN];
		int prov_pin_len = sizeof(prov_pin);
		get_prov_pin(prov_pin, &prov_pin_len);
		app_ezconnect_provisioning_start(PROV_SSID,
				(uint8_t *)prov_pin, prov_pin_len);
	}

	if (app_httpd_only_start() != WM_SUCCESS)
		wmprintf("Error: Failed to start HTTPD\r\n");

	register_httpd_plug_handlers();
#define FTFS_API_VERSION    100
#define FTFS_PART_NAME	    "ftfs"
	static struct fs *fs_handle;
	app_ftfs_init(FTFS_API_VERSION, FTFS_PART_NAME, &fs_handle);

	mdns_start(MDNS_DOMAIN_NAME, MDNS_HOSTNAME);

	int ret = psm_cli_init(sys_psm_get_handle(), NULL);
	if (ret != WM_SUCCESS)
		wmprintf("Error: psm_cli_init failed\r\n");
}

/* The GPIO number below is valid only for mw300_rd board.
 * Please use appropriate number as per the connections
 * on your board.
 */
output_gpio_cfg_t connection_led = {
	.gpio = GPIO_40,
	.type = GPIO_ACTIVE_LOW,
};

static void event_normal_connected(void *data)
{
	void *iface_handle;
	static bool mdns_announced;
	char ip[16];

	app_network_ip_get(ip);
	wmprintf("Connected to Home Network with IP address = %s", ip);

	iface_handle = net_get_sta_handle();
	if (!mdns_announced) {
		snprintf(plug_txt, PLUG_TXT_SIZE, "api_ver=2|model=ABC");
		mdns_set_txt_rec(&plug_service, plug_txt, '|');
		mdns_announce_service(&plug_service, iface_handle);
		mdns_announced = true;
	} else {
		/* If device reconnects to network after any network
		 * abruptions it must re-probe and announce services again */
		mdns_announce_service_all(iface_handle);
	}

	static bool cloud_started;
	if (!cloud_started) {
		aws_cloud_start();
	}
	led_on(connection_led);
}

static void event_normal_connecting(void *data)
{
	led_fast_blink(connection_led);
}

/*
 * Event: PROV_DONE
 *
 * Provisioning is complete. We can stop the provisioning
 * service.
 */
static void event_prov_done(void *data)
{
	struct wlan_network *network = (struct wlan_network *) data;
	wmprintf("Provisioning Completed\r\n");
	app_ezconnect_provisioning_stop();

	if (network->security.type == WLAN_SECURITY_EAP_TLS) {
		if (identity_len > 0 && ca_der_len > 0 &&
			client_cert_len > 0 && client_key_len > 0) {
			memcpy(network->identity, identity, identity_len);
			network->security.tls_cfg.ca_cert = ca_der;
			network->security.tls_cfg.ca_cert_size = ca_der_len;
			network->security.tls_cfg.ca_cert_type =
					SSL_FILETYPE_ASN1;
			network->security.tls_cfg.client_cert =
					(unsigned char *) client_cert;
			network->security.tls_cfg.client_cert_size =
					client_cert_len;
			network->security.tls_cfg.client_cert_type =
					SSL_FILETYPE_ASN1;
			network->security.tls_cfg.client_key =
					(unsigned char *) client_key;
			network->security.tls_cfg.client_key_size =
					client_key_len;
			network->security.tls_cfg.client_key_type =
					SSL_FILETYPE_ASN1;
		}
	}
}

/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_NORMAL_CONNECTED:
		event_normal_connected(data);
		break;
	case AF_EVT_NORMAL_CONNECTING:
		event_normal_connecting(data);
		break;
	case AF_EVT_PROV_DONE:
		event_prov_done(data);
		break;
	case AF_EVT_NORMAL_DHCP_RENEW:
		event_normal_dhcp_renew(data);
		break;
	default:
		break;
	}

	return WM_SUCCESS;
}

int main()
{
	wmstdio_init(UART0_ID, 0);

	cli_init();

	wmtime_init();
	wmtime_cli_init();
	/* Set the system time to 1 Sept 2016. This is required for firmware
	 * upgrade server's certificate validation.
	 * - Your hardware could be designed to either preserve time counters
	 *   across power cut-offs OR
	 * - You could fetch the current time from a remote server
	 */
	wmtime_time_set_posix(1474458308);

	fw_upgrade_cli_init();

	configure_push_button();

	mfg_psm_init();
	mfg_cli_init();

	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		wmprintf("Failed to start application framework\r\n");
		critical_error_handler((void *) -WM_FAIL);
	}

	return 0;
}
