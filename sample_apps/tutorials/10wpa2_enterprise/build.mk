exec-y              += 10wpa2_enterprise
10wpa2_enterprise-objs-y      := src/main.c src/plug_handlers.c src/fw_upgrade.c src/aws_cloud.c

10wpa2_enterprise-ftfs-y      := 10wpa2_enterprise.ftfs
10wpa2_enterprise-ftfs-dir-y  := $(d)/www
10wpa2_enterprise-ftfs-api-y  := 100

10wpa2_enterprise-mfg-cfg-y   := config.mfg

10wpa2_enterprise-supported-toolchain-y := arm_gcc iar

ifneq ($(XIP),1)
  $(error This app can only be built with XIP=1)
endif

ifneq ($(CYASSL_FEATURE_PACK), fp5)
  $(error This app can only be built with CYASSL_FEATURE_PACK=fp5)
endif

ifneq ($(CONFIG_ENABLE_HTTPC_SECURE),y)
  $(error Please enable 'HTTPS support' in 'HTTP Client' from SDK configuration)
endif
