
*** This document shows the wifi_ble_demo application usage ****

A. Pre-requisite:

1. You should have the appropriate chipsets that support both Wi-Fi + BLE.
2. wifi_ble_demo app should be compiled and flashed on the board.
3. Need a remote device with BLE support. If remote accepts only incoming
connection (say a BLE enabled mouse), then it can be used to test central role
of DUT (Device Under Test). If you want to test peripheral role of DUT, then
remote device should be able to scan and find our device (say a BLE enabled
phone).

B. Steps to execute on console (minicom) for demo.

1. When wifi_ble_demo.bin file is flashed, you will observe below prints, which
indicate that firmware is loaded, BT driver is registered and bluetooth stack is
initialized.

# [sdio] Card detected
[sdio] Card reset successful
[sdio] Card Version - (0x32)
[sdio] Card initialization successful
[bt_drv] revision=0x10
[bt_drv] IOPORT: (0x10000)
[bt_drv] Firmware Ready
[bt_drv] BT driver initialized
[af] app_ctrl: prev_fw_version=0
[net] Initializing TCP/IP stack
wifi_ble_demo app handler: event id 1
BT_ENABLE_COMPLETED_EVENT


2. Press "Enter" key to go to command prompt
3. Type "help" at command prompt to show ble related test options

# help

<snip>

ble-opt
ble-create-bond
ble-gattc-connect
ble-gattc-disconnect

<snip>

4. You can press respective options which show its usage. for e.g. If you type ble-opt

# ble-opt 
Please enter valid option 
Usage: ble-opt <number>
 where number = 1 - Start advertising (broadcaster role)
       number = 2 - Stop advertising
       number = 3 - Start Discovery <Remote address> <address type> (observer role)
       number = 4 - Send Mouse reports (broadcaster role)
 

5. To start advertising, so that remote devices can search us, use option 1

Pre-condition: On a remote device, which supports BLE, make sure bluetooth is
switched ON

# ble-opt 1
Selected menu option =1
Start Advertising

If you perform discovery (Scanning) from remote device, device with name
"MRVL-WMSDK" should now show up. This is friendly name of our Bluetooth device.

6. To stop advertisement, use option 2.

# ble-opt 2
 Selected menu option =2
 Stop Advertising


7. In order to perform scan to find nearby remote devices, use option 3.
  In case you want to scan only one device, then pass the optional parameters of remote device address 
  and its address type. If you want to discover all devices, then these two parameters are not needed

Pre-condition: Make sure, that remote device with BLE support has its bluetooth
switched ON and is advertising, only then the remote device would show up in our
discovery result.

# ble-opt 3 [<ES:DR:AD:TE:MO:RE> <Address_type>]  ( For a particular device ) 
 OR
# ble-opt 3  (Default case )

Selected menu option =3
Start Scanning

Scanning is performed and it displays devices found on console.
Sample output is as below:

BT_GAP_ADVERTISEMENT_REPORT_EVENT Device 0x9A:0x00:0x00:0x6B:0x0B:0x00
Device Type 1 Address Type 0
Report Type: 4 Len 19 rssi 0xc5
0x03 0x03 0x01 0x18 0x02 0x1c 0x01 0x0b 0x09 0x4d 0x52 0x56 0x4c 0x2d 0x57 0x4d 0x53 0x44 0x4b


 BT_DISCOVERY_RESULTS_EVENT: Device 0x9A:0x00:0x00:0x6B:0x0B:0x00
 Device Type 1 Address Type 0
 RSSI value -59 dbm
 Device Name Len 10 MRVL-WMSDK

ble demo app handler: BT_DISCOVERY_COMPLETED_EVENT Discovery Completed

Note: Number of devices found can vary in each scan. Also multiple entries of
same device can be seen, since the remote device may be advertising for more
than once in stipulated time.


8. If you want to create bond with any BLE enabled device, use ble-create-bond 

# ble-create-bond

Usage: ble-create-bond <BLE device address> <address type>
Take <BLE device address> string from Discovery result, e.g. AA: BB: CC: DD: EE:FF
<address type> Set 0 for Public, 1 for random

Pre-condition: Ensure remote device is in discoverable mode before you initiate bond.

 e.g.
# ble-create-bond 0x23:0x03:0x11:0x9E:0x15:0x00 0

# opcode 1
BT_GAP_LINK_CONNECT_COMPLETE_EVT: Status 0
GAP_CB: BT_GAP_LINK_CONNECT_COMPLETE_EVT: Device 0x23:0x03:0x11:0x9E:0x15:0x00
GAP_CB: BT_GAP_LINK_CONNECT_COMPLETE_EVT: Device Type 1 Address Type 0 opcode 0
BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT: Status 0
GAP_CB:  BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT: Device 0x23:0x03:0x11:0x9E:0x15:0x00

# Bonding is completed Status 0 Security Level 0 UPDATED CONNECTION PARAMETERS: ConnINT=6, ConnLatency=64, Supervision_TO=4e2, BT_RESULT=0

Note: In above, we use "Just works" (No Input / Output capability) model of SMP.
Hence, without any pin exchange, pairing is completed.

9. Once bonding is done, then for outgoing connection use ble-gattc-connect

This command can be used to connect to any remote device in peripheral role,
which is also havingGATT Server role.  Examples of such devices are BLE Mouse,
BLE Sensors etc. Once connection is up, remote device (mouse, senor, etc) can
send notifications to DUT.


# ble-gattc-connect
Usage: ble-gattc-connect <BLE device address> <address type> <connect type>
Take <BLE device address> string from Discovery result
<address type> Set 0 for Public, 1 for random
<connect type> Set 1for Direct connect, 0 for Indirect connect

              In below example, we use BLE mouse.
 # ble-gattc-connect 0x23:0x03:0x11:0x9E:0x15:0x00 0 1


---Primary Service discovery Started---


Discover Primary Service Response: 
Service UUID: 0x1800
Start Handle: 0x1
End Handle: 0x7 

Service UUID: 0x1801
Start Handle: 0x8
End Handle: 0x8 

Service UUID: 0x1812
Start Handle: 0x9
End Handle: 0x2c 

Discover Primary Service Response: 
Service UUID: 0x0
Start Handle: 0x2d
End Handle: 0x2f 

Discover Primary Service Response: 
Service UUID: 0x180f
Start Handle: 0x30
End Handle: 0x33 

Service UUID: 0x1813
Start Handle: 0x34
End Handle: 0x39 

Service UUID: 0x180a
Start Handle: 0x3a
End Handle: 0xffff 

Discover Primary Service Response: 
---Primary Service discovery Completed---


---Included Service discovery Started---


---Included Service discovery Completed---


---Characteristics discovery Started---


Discover Characteristic Response: 
Characteristic Handle: 0x2
Properties: 0xa
Characteristic Value Handle: 0x3
Characteristic UUID: 0x2a00

Characteristic Handle: 0x4
Properties: 0x2
Characteristic Value Handle: 0x5
Characteristic UUID: 0x2a01

Characteristic Handle: 0x6
Properties: 0x2
Characteristic Value Handle: 0x7
Characteristic UUID: 0x2a04

Discover Characteristic Response: 
Characteristic Handle: 0xa
Properties: 0x2
Characteristic Value Handle: 0xb
Characteristic UUID: 0x2a4a

---Characteristics discovery Completed---

---Characteristics descriptors discovery Started---

CCC Handle: 0x10
CCC Handle: 0x13
CCC Handle: 0x1a
CCC Handle: 0x33
CCC Handle: 0x39
----Completed All discovery Requests----

---Configuring all notifications---


Note: At this point, DUT is now ready to receive notifications. Remote device
can send notifications. If, you had paired with a BLE mouse, then, when you move
the mouse, you should observe reports on console indicating data is received,
like below

Got Notification with handle = 0X12 Value is:   0X0 0X0 0X2 0X0 0X0 0X0 0X0 0X0

Got Notification with handle = 0X12 Value is: 0X0 0X0 0X10 0X0 0X3 0X0 0X0 0X0

Got Notification with handle = 0X12 Value is:  0X0 0X0 0X11 0X0 0Xff 0Xff 0X0 0X0

Got Notification with handle = 0X12 Value is:  0X0 0X0 0X2 0X0 0X0 0X0 0X0 0X0

Got Notification with handle = 0X12 Value is:  0X0 0X0 0X0 0X0 0X4 0X0 0X0 0X0


10. To initiate disconnect, use ble-gattc-disconnect 

# ble-gattc-disconnect

# Disconnected with GATT server
[HOGP TEST APP][CLOSE_EVT]Stop Idle Timer, ID
BT_GAP_LINK_DISCONNECT_COMPLETE_EVT: Status 22
GAP_CB:  BT_GAP_LINK_DISCONNECT_COMPLETE_EVT: Device 0x23:0x03:0x11:0x9E:0x15:0x00 


11. In the event of remote device has initiated connection, and you want to send
GATT data to remote, you can use the below option

#ble-opt 4 

This will send HID mouse events to remote device. If it is a phone with a
display you should see a moving pointer to indicate mouse events are being
received by remote device.
