/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   bas_service.h
 * \brief  This file contains the data structures and function API declarations 
 *        that would be used by different BAS Service.
 * \author Marvell Semiconductor
 */


#ifndef _BAS_SERVICE_H
#define _BAS_SERVICE_H

#include "mrvlstack_config.h"
#include "main_gatt_api.h"
#include "service_conf.h"


/***********************************************************************

  Macro Definitions & Data Sturctures

 ************************************************************************/

/*!
* \brief Battery Level Characteristic Value
*/
typedef struct {
  tgatt_uuid16_charval_decl_record_std bal_CharValDecl; ///< Battery Level Characteristic Attribute Record
  uint8    BALVal;  ///< Battery Level Characteristic Value
}tBAS_BAL_CharValDecl;

/*!
* \brief Battery Level Client Characteristic Configuration Descriptor
*/
typedef struct {
  tgatt_char_desc_CCC_record_std    baLevel_ccc;  ///< Battery Level Client Characteristic Configuration Attribute Record
  uint16                            nty_Ind[MAX_CONN_PER_DEV];    ///< CCC Value, maintained per Connection/Client[0x01 - Notify; 0x02 - Indicate]
}tBAS_BAL_CCC;

/*!
* \brief Battery Service Instance Data
*/
typedef struct {
  tgatt_uuid16_service_record *BAS_SrvcRecord;
  tgatt_uuid16_char_decl_record *CharDecl_BALevel;
  tBAS_BAL_CharValDecl *CharVal_BALevel;
  tBAS_BAL_CCC *BALCCC;
}tBAS_Inst_Info;

/*!
* \brief Battery Service Data to be saved to NVM(Non Volatile Memory)
*/
typedef struct {
  boolean valid_data;          ///< Set TRUE while storing the data, so that while loading only valid data can be loaded, else dont load.
  uint16 bal_nty_Ind_ccc;    ///< 0x01 - Notify; 0x02 - Indicate
}tBAS_nvm_data;


/***********************************************************************

  Function Declarations

 ************************************************************************/
void bas_serv_init(void);
thandle_range bas_service_to_be_included(void);
void bas_srvc_send_notify(tconn_id conn_id, uint16 val_len, uint8 * pval);


#endif //_BAS_SERVICE_H
