/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   hidd_api.h
 * \brief  This file contains the HID Device role Data structures & API's
 *         that are exposed to Upper Applications.
 * \author Marvell Semiconductor
 */

#ifndef _HIDD_API_H
#define _HIDD_API_H

#if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == 1)

#include "main_gap_declares.h"
#include "main_gatt_api.h"


/***********************************************************************

  Macro Definitions & Data Sturctures

 ************************************************************************/

//#define MAX_CONN_PER_APP  1  // Maximum number of Connections supported per HOGP application registration.
#define MAX_APP_ID          1  ///< Maximum number of HOGP application registrations supported. It is designed to extend No of Apps supported
//per device.    

/*!
* \brief AppID's given to Application
*/
typedef enum
{
  APP_ID_1 = 0,      ///< App ID shall start with 0, as it will be used as INDEX to access the APP context.
  APP_ID_2,
  APP_ID_ERR = 0xFF  ///< Exceeded Maximum App Limit. 
}hidd_appid;
typedef uint8 appID;


/*!
* \brief tHIDD_STATUS: HIDD result codes, returned by HID functions 
*/
typedef enum
{
  HIDD_SUCCESS,
  HIDD_ERR_NOT_REGISTERED,
  HIDD_ERR_ALREADY_REGISTERED,
  HIDD_ERR_NO_RESOURCES,
  HIDD_ERR_NO_CONNECTION,
  HIDD_ERR_INVALID_PARAM,
  HIDD_ERR_UNSUPPORTED,
  HIDD_ERR_UNKNOWN_COMMAND,
  HIDD_ERR_CONGESTED,
  HIDD_ERR_CONN_IN_PROCESS,
  HIDD_ERR_ALREADY_CONN,
  HIDD_ERR_DISCONNECTING,
  HIDD_ERR_SET_CONNABLE_FAIL,
  /* Device specific error codes */
  HIDD_ERR_HOST_UNKNOWN,
  HIDD_ERR_L2CAP_FAILED,
  HIDD_ERR_AUTH_FAILED,
  HIDD_ERR_SDP_BUSY,
  HIDD_ERR_GATT,

  HIDD_ERR_INVALID = 0xFF
}hidd_status;
typedef uint8 tHIDD_STATUS;


/*!
* \brief Events Sent to Application 
*/
typedef enum {
  HIDD_INT_APP_REGISTERED_EVT = 0 ,
  HIDD_INT_APP_UNREGISTERED_EVT,
  HIDD_INT_APP_OPEN_EVT,
  HIDD_INT_APP_CLOSE_EVT
}hidd_app_event;
typedef uint8 tHIDD_EVT;


/*!
* \brief App Registration Info
*/
typedef struct
{
  tHIDD_STATUS  status;
  appID         appid;
}tHIDD_AppInfo;


/*!
* \brief App Connection Info
*/
typedef struct
{
  tHIDD_STATUS  status;
  tconn_id      connID;
  tbt_device_id dev_id;
}tHIDD_ConnInfo;


/*!
* \brief Union of data associated with HD callback
*/
typedef union
{     
  /* TO DO: Based on the Gatt CallBack Events, define Event Data structures per event */
  tHIDD_STATUS status;     ///< Status event will be sent to HIDD_INT_APP_REGISTERED_EVT, HIDD_INT_APP_UNREGISTERED_EVT, HIDD_INT_APP_CLOSE_EVT
  tHIDD_ConnInfo connEvt;  ///< Status event will be sent to HIDD_INT_APP_OPEN_EVT
} tHIDD_CBACK_EVT;


/*!
* \brief HIDD callback function
*/
typedef void tHIDD_CBACK(tHIDD_EVT event, tHIDD_CBACK_EVT *status);


/*!
* \brief API Connection Params
*/
typedef struct
{
  appID         appid;
  tbt_device_id devid;
}tConn_params;


/*!
* \brief API Disconnection Params
*/
typedef struct
{
  appID    appid;
  tconn_id connID;
}tDisConn_params;


/*!
* \brief API Send Report Params
*/
typedef struct
{
  appID    appid;       ///< Application Id, which is mapping index of Application Callback Array
  tconn_id connID;      ///< Gatt Server Connection Id
  uint8    rpt_type;    ///< Report Type
  uint8    rpt_ID;      ///< Report ID
  uint16   rpt_len;     ///< Report Length
  uint8    *p_data;     ///< Pointer to Report data
}tSendReport_params;


/* 
   typedef void (*hidd_api_fcn_ptr)(t_hidd_api_params *hidd_params);

   typedef struct {
   hidd_api_fcn_ptr  api_handler;
   }t_hidd_api_handlers;
 */

/***************************************************************************************************

  HIDD State Machine Function Declarations

 ****************************************************************************************************/

/*!
 * @brief  Function API for Application to Register with HOGP.
 *
 * @param app_reg_params Application Callback function pointer
 *
 * @return tHIDD_AppInfo Returns Appid and this api status.
 */
tHIDD_AppInfo Hidd_app_reg_api(tHIDD_CBACK *app_reg_params);


/*!
 * @brief  Function API for Application to DeRegister with HOGP.
 *
 * @param app_dereg_params Pointer to Application Id returned while app registration
 *
 * @return tHIDD_STATUS Returns api status.
 */
tHIDD_STATUS Hidd_app_dereg_api(appID *app_dereg_params);

#if 0
/*******************************************************************************
 **
 ** Function      Hidd_connect_api
 **
 ** Description   Connect API for APP.
 **
 ** Parameters:   Takes tConn_params as input argument and Return tHIDD_STATUS.
 **
 *******************************************************************************/
tHIDD_STATUS Hidd_connect_api(tConn_params *conn_params);
#endif

/*!
 * @brief  Function API for Disconnect for given connection id
 *
 * @param disconn_params Pointer to Disconnection parameters
 *
 * @return tHIDD_STATUS Returns api status.
 */
tHIDD_STATUS Hidd_disconnect_api(tDisConn_params *disconn_params);


/*!
 * @brief  Function API for  Send Report API
 *
 * @param sendReport_params Pointer to Report parameters to be sent to remote
 *
 * @return tHIDD_STATUS Returns api status.
 */
tHIDD_STATUS Hidd_send_report_api(tSendReport_params *sendReport_params);//Input Data Pointer should be global data as context switch happens

#endif //#if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == TRUE)
#endif /*_HIDD_API_H*/
