/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   dis_service.h
 * \brief  This file contains the data structures and function API declarations 
 *        that would be used by different DIS Service.
 * \author Marvell Semiconductor
 */


#ifndef _DIS_SERVICE_H
#define _DIS_SERVICE_H

#include "mrvlstack_config.h"
#include "main_gatt_api.h"


/***********************************************************************

  Macro Definitions & Data Sturctures

 ************************************************************************/

/*!
* \name Device PnP Id Value
*/
//@{
#define VENDORID_SRC              0x01    // As of now hardcoding these values for Marvell,
#define VENDORID                  0x0048    //But, must be changed based on the Client's Product. 
#define PRODUCTID                 0x01    
#define PRODUCT_VER               0x01
//@}

/*!
* \brief Device PnP Id Characteristic Value
*/
typedef struct {
  tgatt_uuid16_charval_decl_record_std pnpid_CharValDecl; ///< PnP ID Characteristic Value Declaration Record
  struct {
    uint8    vendorIDsrc;        
    uint16    vendorID;
    uint16    productID;
    uint16    productVersion;
  }pnpidVal;  ///< PnP ID Characteristic Value
}tDIS_PNPID_CharValDecl;

/*!
* \brief Device System Id Characteristic Value
*/
typedef struct{
    tgatt_uuid16_charval_decl_record_std sysid_CharValDecl; ///< System ID Characteristic Value Declaration Record
    struct {
        uint8 *manufDefID;
        uint8 *orgUniqID;
    }sysidVal;  ///< System ID Characteristic Value
}tDIS_SYSID_CharValDecl;


/***********************************************************************

  Function Declarations

 ************************************************************************/
void dis_serv_init(void);


#endif //_DIS_SERVICE_H
