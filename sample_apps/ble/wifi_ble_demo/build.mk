
exec-y += wifi_ble_demo
WIFI_BLE_DEMO_MDNS_ENABLE=y

wifi_ble_demo-objs-y := src/main.c \
		src/reset_prov_helper.c \
		src/wps_helper.c \
		src/hidd_main.c \
		src/hidd_le.c \
		src/hidd_api.c \
		src/gap_service.c \
		src/bas_service.c \
		src/dis_service.c \
		src/scps_service.c \
		src/ble.c

wifi_ble_demo-cflags-y := -DWM_IOT_PLATFORM=TRUE  -DHID_DEVICE_ENABLED=1 -I$(d)/src

wifi_ble_demo-objs-$(WIFI_BLE_DEMO_MDNS_ENABLE) += src/mdns_helper.c
wifi_ble_demo-cflags-$(WIFI_BLE_DEMO_MDNS_ENABLE) += -DAPPCONFIG_MDNS_ENABLE

wifi_ble_demo-ftfs-y := wifi_ble_demo.ftfs
wifi_ble_demo-ftfs-dir-y := $(d)/www
wifi_ble_demo-ftfs-api-y := 100


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
