/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   hps_service.c
 * \brief  This file contains the implementation of the HTTP Proxy Service Handling
 * \author Marvell Semiconductor
 */

#include "main_gap_declares.h"
#include "main_gatt_api.h"
#include "hps_service.h"
#include "memory_manager.h"
#include "utils.h"
#include "oss.h"
//#include "stkqueue.h"


/*********************************************************************************************************
 ** Macro Definitions
 
 **********************************************************************************************************/

/*********************************************************************************************************
 ** Structure Definitions
 
 **********************************************************************************************************/

/*********************************************************************************************************
 ** Global Variables.
 
 **********************************************************************************************************/
Queue_t hps_client_conn_q;  /* Queue of the node type thps_service_conn_context for each connection*/

static tgatt_uuid16_char_decl_record chardecl_uri;
static tgatt_uuid16_charval_decl_record_std charval_uri;
static tgatt_uuid16_char_decl_record chardecl_http_header;
static tgatt_uuid16_charval_decl_record_std charval_http_header;
static tgatt_uuid16_char_decl_record chardecl_http_entity_body;
static tgatt_uuid16_charval_decl_record_std charval_http_entity_body;
static tgatt_uuid16_char_decl_record chardecl_http_control_point;
static tgatt_uuid16_charval_decl_record_std charval_http_control_point;
static tgatt_uuid16_char_decl_record chardecl_http_status_code;
static tgatt_uuid16_charval_decl_record_std charval_http_status_code;
static tgatt_char_desc_CCC_record_std http_status_code_ccc;
static tgatt_uuid16_char_decl_record chardecl_https_security;
static tgatt_uuid16_charval_decl_record_std charval_https_security;

#if ((defined MRVL_STACK_DEBUG_ENABLED) && (MRVL_STACK_DEBUG_ENABLED == TRUE))
uint32 HPS_MSG_LEVEL = ALL; 
#endif /*MRVL_STACK_DEBUG_ENABLED == TRUE*/

uint8 gbuffer[512*3];
uint16 gbuff_offset;



/*********************************************************************************************************
 ** External Functions 
 
 **********************************************************************************************************/
extern boolean http_request_handler(thps_http_req_id http_req, void *handle, uint8* uri, uint8 *header, uint8 *body, uint8 *txheader,
                                     thps_http_status_code *http_status, thps_http_data_status_code*data_status,uint16 *header_len,
                                     uint16 *dlen);

extern void *http_server_connect(uint8 *url );

/*********************************************************************************************************
 ** Global Function Definitions
 
 **********************************************************************************************************/
 static tgatts_cb_result hps_serv_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data);

 /* Primary service - HPS Service 
  ********** Start **************
  */
static tgatt_uuid16_service_record hps_srvc_record =
{
   /* SERVICE DEFINITION */
   NULL,                                                                    ///< Link pointer for all_service_q 
   {&chardecl_uri, GATT_PRIM_SERVICE_RECORD_UUID16, GATT_START_HANDLE},   ///< record header.next_ptr is set to address of next record of the service 
    13,                                ///< Service End handle, set this value to total number of attributes associated with this service excluding service record itself 
   UUID_DECL_PRIMARY_SERVICE,        ///< 0x2800 for «Primary Service» 
   hps_serv_req_handler,             ///< Service Callback
   UUID_SERVICE_HTTP_PROXY,         ///< Bluetooth HPS service 
};
 
static tgatt_uuid16_char_decl_record chardecl_uri =
{
   /* Characteristics Declaration */
   {&charval_uri, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
   UUID_DECL_CHARACTERISTIC,         ///< 0x2803 for «Characteristics Declaration» 
   GATT_CH_PROP_WRITE,                ///< Read and Write
   GATT_NEXT_HANDLE,                 ///< Set to Next attribute record
   GATT_UUID_HPS_CHAR_URI            ///< UUID for URI characteristic 
};
 
static tgatt_uuid16_charval_decl_record_std charval_uri =
{
   /* Characteristics Value Declaration */
   {&chardecl_http_header, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
   GATT_UUID_HPS_CHAR_URI,                                                     ///< UUID for URI characteristic
   GATT_PERM_WRITE_NO_SECURITY_LEVEL
   
};
 
static tgatt_uuid16_char_decl_record chardecl_http_header =
{
   /* Characteristics Declaration */
   {&charval_http_header, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
   UUID_DECL_CHARACTERISTIC,         ///< 0x2803 for «Characteristics Declaration» 
   GATT_CH_PROP_READ | GATT_CH_PROP_WRITE,                ///< Read and Write
   GATT_NEXT_HANDLE,                 ///< Set to Next attribute record
   GATT_UUID_HPS_CHAR_HTTP_HEADER    ///< UUID for HTTP_HEADER
};
 
static tgatt_uuid16_charval_decl_record_std charval_http_header =
{
   /* Characteristics Value Declaration */
  {&chardecl_http_entity_body, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HPS_CHAR_HTTP_HEADER,                                                     ///< UUID for HTTP_HEADER
  GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL
  
};
 
static tgatt_uuid16_char_decl_record chardecl_http_entity_body =
{
   /* Characteristics Declaration */
   {&charval_http_entity_body, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},   ///< record header.next_ptr is set to address of next record of the service 
   UUID_DECL_CHARACTERISTIC,           ///< 0x2803 for «Characteristics Declaration» 
   GATT_CH_PROP_READ | GATT_CH_PROP_WRITE,                  ///< Read and Write
   GATT_NEXT_HANDLE,                   ///< Set to Next attribute record
   GATT_UUID_HPS_CHAR_HTTP_BODY        ///< UUID for HTTP_BODY  
   
};
 
static tgatt_uuid16_charval_decl_record_std charval_http_entity_body =
{
   /* Characteristics Value Declaration */
   {&chardecl_http_control_point, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
   GATT_UUID_HPS_CHAR_HTTP_BODY,                                           ///< UUID for HTTP_BODY  
   GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL
};
 

static tgatt_uuid16_char_decl_record chardecl_http_control_point =
{
   /* Characteristic Declaration */
   {&charval_http_control_point, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},
   UUID_DECL_CHARACTERISTIC,
   GATT_CH_PROP_WRITE, ///< Read Property
   GATT_NEXT_HANDLE,
   GATT_UUID_HPS_CHAR_CONTROL_POINT ///< UUID for CONTROL_POINT
};
 
static tgatt_uuid16_charval_decl_record_std charval_http_control_point = 
{
   /* Characteristic Value Declaration */
   {&chardecl_http_status_code, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE}, 
   GATT_UUID_HPS_CHAR_CONTROL_POINT,                                    ///< UUID for CONTROL_POINT
   GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL
};
 
static tgatt_uuid16_char_decl_record chardecl_http_status_code =
{
   /* Characteristic Declaration for Hardware Revision String */
   {&charval_http_status_code, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},
   UUID_DECL_CHARACTERISTIC,
   GATT_CH_PROP_NOTIFY,          ///< Read Property 
   GATT_NEXT_HANDLE,
   GATT_UUID_HPS_CHAR_STATUS_CODE   ///< UUID for STATUS_CODE 
 };
 
static tgatt_uuid16_charval_decl_record_std charval_http_status_code = 
{
   /* Characteristic Value Declatation for Hardware Revision String */
   {&http_status_code_ccc, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},
   GATT_UUID_HPS_CHAR_STATUS_CODE,
   GATT_PERM_READ_NO_SECURITY_LEVEL
   
}; 
 
static tgatt_char_desc_CCC_record_std http_status_code_ccc =
{
   {&chardecl_https_security, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
   UUID_DESCPTR_CLIENT_CHARACTERISTIC_CONFIGURATION,                    ///< UUID for CCC 
   GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL | GATT_PERM_NEED_AUTHORIZATION_FLAG
   
};
    
static tgatt_uuid16_char_decl_record chardecl_https_security = 
{
     /* Characteristic Declaration for Firmware Revision String */
  {&charval_https_security, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},
  UUID_DECL_CHARACTERISTIC,
  GATT_CH_PROP_READ,          ///< Read Property  
  GATT_NEXT_HANDLE,
  GATT_UUID_HPS_CHAR_SECURIY_CODE   ///< UUID for SECURIY_CODE
};
 
static tgatt_uuid16_charval_decl_record_std charval_https_security = 
{
    /* Characteristic Value Declaration for Firmware Revision String */
   {NULL, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},
   GATT_UUID_HPS_CHAR_SECURIY_CODE,
   GATT_PERM_READ_NO_SECURITY_LEVEL
};
 
 /* Primary service - HPS Service 
  ********** End **************
  */


 /*!
  * @brief	 HTTP Proxy Service Init API for APP/Profile.
  * 		   This API Call Instantiates a Single HTTP Proxy Service Instance
  * 		   NOTE: To Add Multiple HTTP Proxy Service Instances, Need to create Copy File of this file and
  * 					 Modify the names of APIs, Global Variables and Callback function names.
  *
  * @param Void Takes No Parameters
  *
  * @return Void Returns None
  */
void hps_service_init(void)
{
  
  initq(&hps_client_conn_q,0x00,0x00);
  
  /* Add the DIS service record to GATT Only once, as there can be only single instance for Device Info Service*/
  /* Create HPS Service */
  GATTS_Add_Service((void *) &hps_srvc_record);
  
  return;  
}


/*!
 * @brief  Function to send status as Gatt Notification to HTTP client
 *
 * @param device_id Pointer to Remote Device Id
 * @param http_status HTTP Response status
 * @param dstatus HTTP data status
 *
 * @return Void Returns None
 */
void hps_service_set_and_send_status(tbt_device_id *device_id, uint16 http_status, uint8 dstatus)
{
  
  uint8 status[MAX_STATUS_CODE_CHAR_SIZE],*p;
  
  BufferDesc_t *pbuff = (BufferDesc_t*)peekq_head(&hps_client_conn_q);
  
  thps_service_conn_context *hps_service_conn_context = pbuff ?
                   (thps_service_conn_context *)BML_DATA_PTR(pbuff):NULL;
  
  HPS_DEBUG("Request ID 0x%02X ", hps_service_conn_context->control);
  
  p = status;
  
  oss_memset(p,0x00,MAX_STATUS_CODE_CHAR_SIZE);
  
  UINT16_TO_LE_ENDIAN(http_status,p);
  
  UINT8_TO_LE_ENDIAN(dstatus,p);
  
  GATTS_Send_Handlevalue(hps_service_conn_context->conn_id,
               charval_http_status_code.header.handle,FALSE,sizeof(hps_service_conn_context->status_code),status);
  /*Once Response is sent and Status is indicated to the client, can reset the control field and assume No Op State*/
  hps_service_conn_context->control = HPS_HTTP_NO_REQ;    
  
}

/*!
 * @brief  Service Internal Function to Validate the request
 *
 * @param hps_service_conn_context Pointer to service connection context
 * @param req_id HTTP request id
 *
 * @return boolean Returns TRUE on successful validation else returns FALSE.
 */
boolean hps_validate_req(thps_service_conn_context *hps_service_conn_context, thps_http_req_id req_id)
{
  
  if ( ((req_id == HPS_HTTP_GET_REQ || req_id == HPS_HTTPS_GET_REQ)
          && ( hps_service_conn_context->char_val_updated_mask & HPS_GET_REQ_CHECK_CHAR_VALUES_UPDATED_MASK ) != HPS_GET_REQ_CHECK_CHAR_VALUES_UPDATED_MASK )
        ||
       ((req_id == HPS_HTTP_HEAD_REQ || req_id == HPS_HTTPS_HEAD_REQ)
          && ( hps_service_conn_context->char_val_updated_mask & HPS_HEAD_REQ_CHECK_CHAR_VALUES_UPDATED_MASK ) != HPS_HEAD_REQ_CHECK_CHAR_VALUES_UPDATED_MASK )
        ||
       ((req_id == HPS_HTTP_PUT_REQ || req_id == HPS_HTTPS_PUT_REQ)
          && ( hps_service_conn_context->char_val_updated_mask & HPS_PUT_REQ_CHECK_CHAR_VALUES_UPDATED_MASK ) != HPS_PUT_REQ_CHECK_CHAR_VALUES_UPDATED_MASK )
       ||
       ((req_id == HPS_HTTP_POST_REQ || req_id == HPS_HTTPS_POST_REQ)
          && ( hps_service_conn_context->char_val_updated_mask & HPS_POST_REQ_CHECK_CHAR_VALUES_UPDATED_MASK ) != HPS_POST_REQ_CHECK_CHAR_VALUES_UPDATED_MASK )
       ||
       ((req_id == HPS_HTTP_DEL_REQ || req_id == HPS_HTTPS_DEL_REQ)
          && ( hps_service_conn_context->char_val_updated_mask & HPS_DELETE_REQ_CHECK_CHAR_VALUES_UPDATED_MASK ) != HPS_DELETE_REQ_CHECK_CHAR_VALUES_UPDATED_MASK )
     )
  {
     
    HPS_DEBUG("char_val_updated_mask: 0x%02X",hps_service_conn_context->char_val_updated_mask);
      
    return FALSE;
    
  }
   
  return TRUE;
}

/*!
 * @brief  Service Request Handler Registered with Local Gatt Server
 *            This Callback funtion handles all the Remote Gatt Client Requests & Events of this service
 *
 * @param conn_id Gatt Server Connection Id
 * @param req_type Gatt Server's request
 * @param p_req_data Request/Event Parameters
 *
 * @return Void Returns None
 */
 

thps_http_status_code http_status; 

thps_http_data_status_code data_status;

extern void utils_get_device_id (uint8** params, tbt_device_id *device_id);
    
void hps_server_http_reply(uint8 *params)
{
  
  thps_service_conn_context *hps_service_conn_context = NULL; 
  
  BufferDesc_t *pbuff = NULL; 
  
  tbt_device_id device_id;
  
  utils_get_device_id (&params,&device_id);
  
  while( ( pbuff = peekq_head(&hps_client_conn_q)) )
  {
    
    hps_service_conn_context = (thps_service_conn_context *)BML_DATA_PTR(pbuff);
    
    if(oss_memcmp(hps_service_conn_context->device_id.address,BD_ADDRESS_SIZE,device_id.address,BD_ADDRESS_SIZE))
      break; //Found matching HPS Service Context.
    
    pbuff = pbuff->NextBuffer;
    
  }
  
  HPS_DEBUG("Service Context %p Control %d\n",hps_service_conn_context,hps_service_conn_context->control);
  
  if(hps_service_conn_context->control)
    hps_service_set_and_send_status(&hps_service_conn_context->device_id, 
                                            http_status, data_status);
  
}

static tgatts_cb_result hps_serv_req_handler(tconn_id conn_id, tgatt_event_ID req_type, tgatts_event_param *p_req_data)
{
  
  tgatts_cb_result result;
  
  tgatts_rsp hps_rsp;
  
  BufferDesc_t *pbuff = NULL; 
  
  thps_service_conn_context *hps_service_conn_context = NULL; 
  
  hps_rsp.rsp_of = req_type;
  
  result.status = ATT_SUCCESS;
  
  void *handle = NULL;
  
  uint16 *body_len = NULL;
  
  while( ( pbuff = peekq_head(&hps_client_conn_q)) )
  {
    
    hps_service_conn_context = (thps_service_conn_context *)BML_DATA_PTR(pbuff);
    
    if(hps_service_conn_context->conn_id == conn_id )
      break; //Found matching HPS Service Context.
    
    pbuff = pbuff->NextBuffer;
    
  }
  
  switch(req_type)
  {
    case GATT_EVENT_CONNECTED:
      {
        BufferDesc_t *pbuff = (BufferDesc_t*)STACK_ALLOC_BUFFER((sizeof(thps_service_conn_context)));
        
        thps_service_conn_context *phps_conn_context = (thps_service_conn_context *)BML_DATA_PTR(pbuff);
        
        oss_memset(phps_conn_context,0x00,sizeof(thps_service_conn_context));
        
        phps_conn_context->conn_id = conn_id;
        
        phps_conn_context->device_id = p_req_data->conn_disc_param.device_id;
        
        putq(&hps_client_conn_q,pbuff);
        
      }
      break;
    case GATT_EVENT_DISCONNECTED:
      {
      
        if(hps_service_conn_context)
        {
          
          removeElementIfInq(&hps_client_conn_q,pbuff);
          
          STACK_FREE_BUFFER(pbuff);
          
        }
        
        oss_memset(gbuffer,0x00,sizeof(gbuffer));
        
        gbuff_offset = 0x00;
        
      }
      break;
    case GATT_EVENT_READ_REQ:
    case GATT_EVENT_WRITE_REQ:
    case GATT_EVENT_WRITE_CMD:
      {
        
        tgatts_req *gatt_req = (tgatts_req*)p_req_data;
        
        boolean send_rsp = TRUE; /* Should be set to False For WRITE COMMAND*/
        
        trecord_header  *p_next_record;
               
        uint8 **ptr = NULL,*p1,*plen = NULL,mask = 0x00;
        
        tgatt_uuid16_charval_decl_record_std *pchar_val = NULL;
        
        HPS_DEBUG("REQ: Handle 0x%02X Offset 0x%02X",gatt_req->handle,gatt_req->offset);
        
        for(p_next_record = (trecord_header *)&hps_srvc_record.header; (p_next_record = p_next_record->p_next_record); )
        {
          
          HPS_DEBUG("Record Handle 0x%02X Record Type 0x%02X",p_next_record->handle,p_next_record->record_type);
          
          if(p_next_record->handle == gatt_req->handle)
            break;
          
        }
        
        hps_rsp.handle = p_next_record->handle;
        
        switch(p_next_record->record_type)
         {
           case GATT_CHARVAL_DECL_RECORD_UUID16_STD:
             {
              
              pchar_val = (tgatt_uuid16_charval_decl_record_std *)p_next_record;
              
              HPS_DEBUG("characterstic uuid 0x%02X Service Context %p",pchar_val->attr_type_uuid16,
                                                                                hps_service_conn_context);
              
              switch(pchar_val->attr_type_uuid16)
               {
                
                case GATT_UUID_HPS_CHAR_URI:
                   ptr = &hps_service_conn_context->uri;
                   hps_rsp.val_len = hps_service_conn_context->uri_len;
                   plen = (uint8 *)&hps_service_conn_context->uri_len ;
                   mask = HPS_HTTP_URI_UPDATE_REQ_RCVD_MASK;
                   break;
                case GATT_UUID_HPS_CHAR_HTTP_HEADER:
                   ptr = &hps_service_conn_context->header;
                   hps_rsp.val_len = hps_service_conn_context->header_len;
                   plen = (uint8 *)&hps_service_conn_context->header_len ;
                   mask = HPS_HTTP_HEADER_UPDATE_REQ_RCVD_MASK;
                   break;
                case GATT_UUID_HPS_CHAR_HTTP_BODY:
                   ptr = &hps_service_conn_context->body;
                   hps_rsp.val_len = hps_service_conn_context->body_len;
                   plen = (uint8 *)&hps_service_conn_context->body_len ;
                   mask = HPS_HTTP_BODY_UPDATE_REQ_RCVD_MASK;
                   break;
                case GATT_UUID_HPS_CHAR_CONTROL_POINT:
                   {
                     
                     thps_http_req_id http_req = *(gatt_req->p_value);
                     
                     p1 = (uint8 *)&hps_service_conn_context->control;
                     
                     ptr = &p1;
                     
                     hps_rsp.val_len = sizeof(uint8);
                     
                     HPS_DEBUG("GATT_UUID_HPS_CHAR_CONTROL_POINT: Req %d Mask 0x%02X",
                                  http_req,hps_service_conn_context->char_val_updated_mask);
                     
                     if(http_req == HPS_HTTP_GET_REQ || http_req == HPS_HTTPS_GET_REQ)
                         body_len = &hps_service_conn_context->body_len;
                     
                     //Need to check 
                     // 1. If req is received in correct state, verify if the req code is valid request code.
                     // 2. is any pending HTTP Request, and new Request is other than Cancel. Send Error.
                     // 3. If all the Characterstics are updated for this HTTP Request. before processing.
                     // 4. Before Start Processing the Request check if Status Code Notification is enabled or not
                     //     In case notification not enabled send error.
                     if( (! http_req ) || ( http_req > HPS_HTTP_CANCEL_REQ ) )
                     {
                        //Invalid Request..
                        result.status = HPS_INVALID_REQUEST;
                     }
                     else if(hps_service_conn_context->control && http_req != HPS_HTTP_CANCEL_REQ)
                     {
                       //HTTP Request is in progress, and othe HTTP Request is received other than CANCEL Req.
                       result.status = ATT_PROCEDURE_ALREADY_IN_PROGRESS;
                       
                     }
                     else if(http_req == HPS_HTTP_CANCEL_REQ)
                     {
                       
                       hps_service_conn_context->control =  HPS_HTTP_NO_REQ;
                       
                       GATTS_Send_Rsp(conn_id,result.status,&hps_rsp);
                       
                       return result;
                       
                     }
                     else if ( hps_validate_req(hps_service_conn_context, http_req) == FALSE )
                     {
                        //Some of the characterstic was not updated by remote for this HTTP Request.
                        result.status = HPS_INVALID_REQUEST;
                        
                        //GATTS_Send_Rsp(conn_id,result.status,&hps_rsp);
                        //return;
                     }
                     else if (!hps_service_conn_context->status_code_ccc )
                     {
                       result.status = ATT_CCCD_IMPROPERLY_CONFIGURED;
                     }
                     else if ( !(handle = http_server_connect( hps_service_conn_context->uri )))
                     {
                     
                       HPS_DEBUG("URI: %s",hps_service_conn_context->uri);
                       
                       //network status Down.
                       result.status = HPS_NETWORK_NOT_AVAILABLE;
                       
                     }
                   }
                default:
                   break;
               }
             }
             break;
           case GATT_CHAR_DESC_CCC_RECORD_STD:
             {
               p1 = (uint8 *)&hps_service_conn_context->status_code_ccc;
               ptr = &p1;
               hps_rsp.val_len = sizeof(hps_service_conn_context->status_code_ccc);
               
             }
             break;
           default:
             break;
             
         }
        
        //If Request is for write then write then update the charval.
        if(req_type == GATT_EVENT_WRITE_REQ )
        {
          if( result.status == ATT_SUCCESS )
          {
            
            if(!(*ptr))
            {
              
              *ptr = (uint8*)( gbuffer + gbuff_offset ); gbuff_offset+=MAX_HTTP_RECORD_SIZE;
              
            }
            else
            {
              
              oss_memset(*ptr,0x00,( plen?*plen : hps_rsp.val_len));
              
            }
            
            if(gatt_req->val_len)
              oss_memcopy(((*ptr) + gatt_req->offset),gatt_req->p_value,gatt_req->val_len);
            
            if(plen)
             *plen = gatt_req->val_len;
            
            hps_service_conn_context->char_val_updated_mask |= mask;
            
            hps_rsp.val_len = 0x00;
            
            hps_rsp.p_val = NULL;
            
          }
           
          STACK_FREE_BUFFER((BML_SUB_DESC(gatt_req->p_value)));
          
        }
        else if(req_type == GATT_EVENT_READ_REQ )
        {
          hps_rsp.val_len -= gatt_req->offset; 
          hps_rsp.p_val = *ptr + gatt_req->offset; /* Added offset to handle read long request */
        }
        
        if(send_rsp)
        {
          
          GATTS_Send_Rsp(conn_id,result.status,&hps_rsp);
          
        }
        
        if( result.status == ATT_SUCCESS &&
              ( pchar_val && pchar_val->attr_type_uuid16 == GATT_UUID_HPS_CHAR_CONTROL_POINT) )
        {
          HPS_DEBUG("URI: %s",hps_service_conn_context->uri);
          
          HPS_DEBUG("Header: Len %d %d %s", hps_service_conn_context->header_len , 
                                             strlen((const char *)hps_service_conn_context->header),hps_service_conn_context->header);
          
          http_request_handler(hps_service_conn_context->control, handle, hps_service_conn_context->uri,
                                hps_service_conn_context->header, hps_service_conn_context->body, 
                                hps_service_conn_context->header,&http_status, &data_status,
                                &hps_service_conn_context->header_len, body_len);
          
          HPS_DEBUG("http_status 0x%02x dstatus 0x%02x Rcvd Hlen %d Rcvd Dlen %d",
                       http_status,data_status,hps_service_conn_context->header_len,
                       hps_service_conn_context->body_len);
          
          //hps_service_set_and_send_status(&hps_service_conn_context->device_id, 
          //                                        http_status, data_status);
          
          //Reset the mask as the operation is completed for now...
          hps_service_conn_context->char_val_updated_mask = 0x00;

          /* temp */
           hps_service_set_and_send_status(&hps_service_conn_context->device_id, 
                                            http_status, data_status);

          
        }
      }
      break;
    case GATT_EVENT_PREP_WRITE_REQ:
      {
        tgatts_prep_write_req_info *p_pw_info;
        void *pbuff;
        
        if(hps_service_conn_context->prep_write_q.q_len < (MAX_PREP_WRITE_Q_SIZE + 1))
        {
        
          /* Copy and enqueue Prepare Write Request */
          pbuff =  (void *)STACK_ALLOC_BUFFER(sizeof(tgatts_prep_write_req_info) + p_req_data->request_param.val_len);
          p_pw_info = (tgatts_prep_write_req_info *)BML_DATA_PTR(pbuff);
          p_pw_info->handle = p_req_data->request_param.handle;
          p_pw_info->offset = p_req_data->request_param.offset;
          p_pw_info->val_len = p_req_data->request_param.val_len;
          
          HPS_DEBUG(" Prepare Write handle =0x%x", p_pw_info->handle);

          oss_memcopy((uint8 *)(p_pw_info +1), p_req_data->request_param.p_value, p_req_data->request_param.val_len);
          putq(&hps_service_conn_context->prep_write_q, pbuff);

          hps_rsp.offset = p_req_data->request_param.offset;
          hps_rsp.val_len = p_req_data->request_param.val_len;
          hps_rsp.p_val = p_req_data->request_param.p_value;
          
        }else
        {
          result.status = ATT_PREP_QUEUE_FULL;
        }

        hps_rsp.handle = p_req_data->request_param.handle;
        
        GATTS_Send_Rsp(conn_id,result.status,&hps_rsp);
        
      }
      break;
    case GATT_EVENT_EXEC_WRITE_REQ:
      {
        
        void *pbuff;
        
        uint8 mask = 0x00, **p ;
        
        uint16 *p_len;
        
        tgatts_prep_write_req_info *p_pw_info;
        
        while((pbuff = getq(&hps_service_conn_context->prep_write_q)))
        {
          
          p_pw_info = (tgatts_prep_write_req_info *)BML_DATA_PTR(pbuff);
           
          HPS_DEBUG(" Execute Write handle =0x%x", p_pw_info->handle);
          
          if(p_req_data->request_param.exec_flag && result.status == ATT_SUCCESS)
          {
            
            HPS_DEBUG(" Execute Write : Writing");
            
            result.error_handle = p_pw_info->handle; /* In case if any Error */ 
            
            /* Check the validity of offset and Value length. For HPS, No Characteristic is bigger than 512 bytes */
            if(p_pw_info->offset > MAX_HTTP_RECORD_SIZE)
            {
              result.status = ATT_INVALID_OFFSET;
            }
            else if((p_pw_info->offset + p_pw_info->val_len) > MAX_HTTP_RECORD_SIZE)
            {
              result.status = ATT_INVALID_ATTR_VALUE_LEN;
            }
            else
            {
              
              if(p_pw_info->handle == charval_uri.header.handle)
              {
                
                p = &hps_service_conn_context->uri;
                p_len = &hps_service_conn_context->uri_len;
                
                mask = HPS_HTTP_URI_UPDATE_REQ_RCVD_MASK;
                
              }
              else if(p_pw_info->handle == charval_http_header.header.handle)
              {
                p = &hps_service_conn_context->header;
                p_len = &hps_service_conn_context->header_len;
                mask = HPS_HTTP_HEADER_UPDATE_REQ_RCVD_MASK;
              }
              else
              {
                p = &hps_service_conn_context->body;
                p_len = &hps_service_conn_context->body_len;
                mask = HPS_HTTP_BODY_UPDATE_REQ_RCVD_MASK;
              }
              
              if(!(*p))
              {
                
                *p = (uint8*)( gbuffer + gbuff_offset ); gbuff_offset += MAX_HTTP_RECORD_SIZE;
                
              }
              else if( ( hps_service_conn_context->char_val_updated_mask & mask ) != mask)
              {
                
                oss_memset(*p,0x00,*p_len);
                
              }
              
              hps_service_conn_context->char_val_updated_mask |= mask;
              
              oss_memcopy(((*p) + p_pw_info->offset), (uint8 *)(p_pw_info + 1),p_pw_info->val_len);
              
              *p_len = p_pw_info->offset + p_pw_info->val_len;
              
            }
              
          }
           
          STACK_FREE_BUFFER(pbuff);
          
        }
        
      }
      break;
    default:
      break;
  }

  return result;
  
}

