
//#include <string.h>
#include "hps_service.h"
#include "types.h"
#include "httpc.h"


/*!
* \brief HPS Data Status Codes
*/

static uint16  gheader_size, gbody_size;
static thps_http_data_status_code gdata_status;

/*!
* \brief  http_request_handler
*            This function is called by the http server to prepare HTTP Request and send it to the HTTP Server.
*
* \param       
* \param       
*
* \returns     
*
*/

boolean http_request_handler(thps_http_req_id http_req, void *handle, uint8* uri, uint8 *header, uint8 *body, uint8 *txheader,
                                 thps_http_status_code *http_status, thps_http_data_status_code*data_status,uint16 *header_len,
                                 uint16 *body_len)
{
	http_req_t req;
	int status;
	http_session_t s_handle = (http_session_t)handle;
	char *name = NULL, *value = NULL;
	int i;
	
	gheader_size = 0x00, gbody_size = 0x00, gdata_status = 0x00;
  
	if(s_handle) 
	{
		switch(http_req)
		{
		case HPS_HTTP_GET_REQ:
		case HPS_HTTPS_GET_REQ:
		{

			/* Setup the parameters for the HTTP Request. This only prepares the
			 * request to be sent out.  */
			req.type = HTTP_GET;
			req.content = NULL;
			req.content_len  = 0;
         
		}
		break;
		case HPS_HTTP_PUT_REQ:
		case HPS_HTTPS_PUT_REQ:
		{
			         
			/* Setup the parameters for the HTTP Request. This only prepares the
			 * request to be sent out.  */
			req.type = HTTP_PUT;
			req.content = (const char *)body;
			req.content_len = strlen((const char *)body);
             
           }
           break;
         case HPS_HTTP_POST_REQ:
         case HPS_HTTPS_POST_REQ:
            break;
         case HPS_HTTP_DEL_REQ:
         case HPS_HTTPS_DEL_REQ:
           {
		req.type = HTTP_DELETE;
		req.content = NULL;
           }
         default:
            break;
       }

	req.resource = (const char *)uri;
	req.version = HTTP_VER_1_1;
	
	status = http_prepare_req(s_handle, &req, STANDARD_HDR_FLAGS);
	if (status != WM_SUCCESS) {
		wmprintf("Request prepare failed \r\n");
	   return false;
	}

	name = strtok((char *)header, ":");
	while(name)
	{
	 	value = strtok(NULL, "\n");
		value[strlen(value) - 1] = '\0'; /* replacing \r by NULL */
       		
		status = http_add_header(s_handle, &req, name,
					value);
		
		if (status != WM_SUCCESS) {
			wmprintf("Error while adding header \r\n");
			return false;
		}
	 	name = strtok(NULL, ":");
	}
		
	/* Send the HTTP request, that was prepared earlier, to the web
	 * server */
	status = http_send_request(s_handle, &req);
	if (status != WM_SUCCESS) {
		wmprintf("Request send failed \r\n");
       return false;
	}

	/* Read the response */
	http_resp_t *resp;
	status = http_get_response_hdr(s_handle, &resp);
	if (status != WM_SUCCESS) {
		wmprintf("Unable to get response header: %d \r\n", status);
       return false;
	}
	
	wmprintf("status code received from server: %d \r\n",
			  resp->status_code);

	*http_status =  resp->status_code;

	/*Preparing raw http header */
	{
		char tmp_str[5] = "1.0 ";
		http_header_pair_t pair_list[30]; /* Assuming Maximum 30 pairs in response header */
		int pair_count = 30;
 
		
		header[0] = 0;
		strcat((char *)header, resp->protocol); /* HTTP */
		strcat((char *)header, "/");
		tmp_str[2] = resp->version + 0x30; 
		strcat((char *)header, tmp_str); /* 1.0 or 1.1 */

		sprintf(tmp_str, "%d ", resp->status_code);
		strcat((char *)header, tmp_str); /* eg. 200 */
		strcat((char *)header, resp->reason_phrase); /* eg. OK */
		strcat((char *)header, "\r\n");

		status = http_get_response_hdr_all(s_handle, pair_list, &pair_count);

		for(i =0 ; i < pair_count; i++)
		{
			if(strlen((const char *)header) + strlen((const char *)pair_list[i].name) + strlen((const char *)pair_list[i].value) + 5 > MAX_BODY_SIZE)
			{
				gdata_status |= HPS_DSTATUS_HEADERS_TRUNCATED;
				break;
			}
  			
			strcat((char *)header, pair_list[i].name);
			strcat((char *)header, ": ");
			strcat((char *)header, pair_list[i].value);
			strcat((char *)header, "\r\n");
		}
			strcat((char *)header, "\r\n");

			if(!(gdata_status & HPS_DSTATUS_HEADERS_TRUNCATED))
				gdata_status |= HPS_DSTATUS_HEADERS_RECEIVED;
#if ((defined (DUMP_HPS_HTTP_RESPONSE) && DUMP_HPS_HTTP_RESPONSE == 1))  
		wmprintf("\r\nReceived HTTP Header:\r\n");
		for(i = 0; i < strlen((const char *)header); i++)
			wmprintf("%c", header[i]);
#endif

		  if(header_len)
			*header_len = strlen((const char *)header) ;
		
	}
		
	/* Keep reading data over the HTTP session */
	while (1) 
	{
		int dsize,i, free_space;
		char buf[1];

		free_space = (MAX_BODY_SIZE -gbody_size);

		if(free_space)
		{
			dsize = http_read_content(s_handle, body + gbody_size, free_space);
#if ((defined (DUMP_HPS_HTTP_RESPONSE) && DUMP_HPS_HTTP_RESPONSE == 1))  
		wmprintf("\r\nReceived HTTP Body:\r\n");
		for (i = 0; i < dsize; i++) {
			if (body[gbody_size + i] == '\r')
				continue;
			if (body[gbody_size + i] == '\n') {
				wmprintf("\n\r");
				continue;
			}
			wmprintf("%c", body[gbody_size + i]);
		}
#endif
			gbody_size +=  dsize;
		}
		else
		{
			dsize = http_read_content(s_handle, buf, 1);
			
			if(dsize)
			{
				gdata_status |= HPS_DSTATUS_BODY_TRUNCATED;
				break;
			}
		}
		if (dsize < 0) {
			wmprintf("Unable to read data on http session: %d",
				   dsize);
			break;
		}
		if (dsize == 0) {
			wmprintf("********* All data read ********** \r\n");
			gdata_status |= HPS_DSTATUS_BODY_RECEIVED;
			break;
		}

	}
	
	/* Close the HTTP session */
	http_close_session(&s_handle);
	
       *data_status = gdata_status;
       
       
       if(body_len)
         *body_len = gbody_size ;
       
       wmprintf("http_code : %d data %d header %d dstatus 0x%02x header_len %d data_len %d \r\n", 
                 *http_status, gbody_size, gheader_size,*data_status,*header_len,gbody_size);
  
	return true;     
  }
  return false;
}


/*!
* \brief  http_server_connect
*            This function is called by the http server to connnect to the specified URL.
*
* \param       
* \param       
*
* \returns     
*
*/

void *http_server_connect(uint8 *url )
{

	http_session_t handle; 
	int status;

	wmprintf("open session url =%s \r\n",url);

	status = http_open_session(&handle, (const char *)url, NULL);
	if (status != WM_SUCCESS) {
	wmprintf("Open session failed \r\n");
	return NULL;
	}

	return (void *)handle;
  
}




