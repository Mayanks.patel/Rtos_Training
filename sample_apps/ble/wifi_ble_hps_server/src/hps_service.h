/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   hps_service.h
 * \brief  This file contains the data structures and function API declarations 
 *        that would be used by HTTP Proxy Service
 * \author Marvell Semiconductor
 *         
 */


#ifndef _HPS_SERVICE_H
#define _HPS_SERVICE_H

#include "mrvlstack_config.h"
#include "main_gatt_api.h"
#include "log.h"

/***********************************************************************

  Macro Definitions 

 ************************************************************************/
#define MAX_URI_SIZE                    0x200
#define MAX_HEADER_SIZE                 0x200
#define MAX_BODY_SIZE                   0x200
#define MAX_HTTP_RECORD_SIZE            0x200
#define MAX_STATUS_CODE_CHAR_SIZE           3
#define HPS_HTTP_SECURED_REQ_MASK        0x80 

#if HPS_DEBUG_ENABLED == TRUE
extern uint32 HPS_MSG_LEVEL; 

#define HPS_ERROR(m, arg...)                    {if (HPS_MSG_LEVEL >= ERROR) print(m, ##arg);}
#define HPS_WARNING(m, arg...)                  {if (HPS_MSG_LEVEL >= WARNING) print(m, ##arg );}
#define HPS_DEBUG(m, arg...)                    {if (HPS_MSG_LEVEL >= DEBUG) print(m, ##arg);}
#define HPS_ALL(m, arg...)                      {if (HPS_MSG_LEVEL >= ALL) print(m, ##arg);}

#else

#define HPS_ERROR(m, arg...)
#define HPS_WARNING(m, arg...)
#define HPS_DEBUG(m, arg...)
#define HPS_ALL(m, arg...)

#endif /*HPS_DEBUG_ENABLED == TRUE*/

/*!
* \name Error Codes
*/
#define HPS_INVALID_REQUEST                       0x81
#define HPS_NETWORK_NOT_AVAILABLE                 0x82

/*!
* \name HPS Char Val Update Masks
*/
//@{
#define HPS_HTTP_URI_UPDATE_REQ_RCVD_MASK         0x01

#define HPS_HTTP_HEADER_UPDATE_REQ_RCVD_MASK      0x02

#define HPS_HTTP_BODY_UPDATE_REQ_RCVD_MASK        0x04

#define HPS_GET_REQ_CHECK_CHAR_VALUES_UPDATED_MASK  ( HPS_HTTP_URI_UPDATE_REQ_RCVD_MASK | HPS_HTTP_HEADER_UPDATE_REQ_RCVD_MASK | \
                                                      HPS_HTTP_BODY_UPDATE_REQ_RCVD_MASK )

#define HPS_HEAD_REQ_CHECK_CHAR_VALUES_UPDATED_MASK ( HPS_HTTP_URI_UPDATE_REQ_RCVD_MASK | HPS_HTTP_HEADER_UPDATE_REQ_RCVD_MASK \
                                                      | HPS_HTTP_BODY_UPDATE_REQ_RCVD_MASK )

#define HPS_POST_REQ_CHECK_CHAR_VALUES_UPDATED_MASK ( HPS_HTTP_URI_UPDATE_REQ_RCVD_MASK | HPS_HTTP_HEADER_UPDATE_REQ_RCVD_MASK \
                                                      | HPS_HTTP_BODY_UPDATE_REQ_RCVD_MASK )

#define HPS_PUT_REQ_CHECK_CHAR_VALUES_UPDATED_MASK        ( HPS_HTTP_URI_UPDATE_REQ_RCVD_MASK | HPS_HTTP_HEADER_UPDATE_REQ_RCVD_MASK \
                                                      | HPS_HTTP_BODY_UPDATE_REQ_RCVD_MASK )

#define HPS_DELETE_REQ_CHECK_CHAR_VALUES_UPDATED_MASK  ( HPS_HTTP_URI_UPDATE_REQ_RCVD_MASK | HPS_HTTP_HEADER_UPDATE_REQ_RCVD_MASK )
//@}

/*!
* \name Default HTTP Request Parameters. 
*/

#define HPS_HTTP_GET_URI                "http://httpbin.org/get"
#define HPS_HTTP_DELETE_URI             "http://httpbin.org/delete"
#define HPS_HTTP_HEAD_URI               "http://httpbin.org"
#define HPS_HTTP_POST_URI               "http://httpbin.org/post"
#define HPS_HTTP_PUT_URI                "http://httpbin.org/put"
#define HPS_HTTPS_GET_URI               "https://httpbin.org/get"
#define HPS_HTTP_HEADER                 "Host: httpbin.org\r\nConnection: Close\r\n"
#define HPS_HTTP_BODY                   "Hello World!\r\n"
#define HPS_HTTPS_HEADER                HPS_HTTP_HEADER

/*!
* \name Maximum size of Prepare Write Queue. 
*/
#define MAX_PREP_WRITE_Q_SIZE (23) /* Considering Max Characteristic length of 512 bytes and default MTU of 23 bytes */

/*********************************************************************************************************
  Structure Definitions
 
 **********************************************************************************************************/

/*!
* \brief HPS Request State
*/
enum {
  
  HPS_HTTP_REQ_STATE_IDLE,
  HPS_HTTP_REQ_STATE_IN_PROGRESS,
  
};

typedef uint8 thps_http_req_st;

/*!
* \brief HPS Request Opcode/Id
*/
enum {
  HPS_HTTP_NO_REQ = 0x00,
  HPS_HTTP_GET_REQ,
  HPS_HTTP_HEAD_REQ,
  HPS_HTTP_POST_REQ,
  HPS_HTTP_PUT_REQ,
  HPS_HTTP_DEL_REQ,
  HPS_HTTPS_GET_REQ,
  HPS_HTTPS_HEAD_REQ,
  HPS_HTTPS_POST_REQ,
  HPS_HTTPS_PUT_REQ,
  HPS_HTTPS_DEL_REQ,
  HPS_HTTP_CANCEL_REQ
  
};

typedef uint8 thps_http_req_id;

/*!
* \brief HPS Request Status Codes
*/
enum {
  
  HPS_HTTP_STATUS_CONT = 100,
  HPS_HTTP_STATUS_SWITCH_PROTO,
  HPS_HTTP_STATUS_OK = 200,
  HPS_HTTP_STATUS_MOVED_PERMANENT = 301,
  HPS_HTTP_STATUS_FOUND,
  HPS_HTTP_STATUS_SEE_OTHER,
  HPS_HTTP_STATUS_USE_PROXY = 305,
  HPS_HTTP_TEMPORARY_REDIRECT,
  HPS_HTTP_BAD_REQUEST = 400,
  HPS_HTTP_RESOURCE_NOT_FOUND = 404
  
};

typedef uint16 thps_http_status_code;

/*!
* \brief HPS Data Status Codes
*/
typedef enum {
  HPS_DSTATUS_HEADERS_RECEIVED = 0x01,
  HPS_DSTATUS_HEADERS_TRUNCATED,
  HPS_DSTATUS_BODY_RECEIVED = 0x04,
  HPS_DSTATUS_BODY_TRUNCATED = 0x08
  
}hps_data_status_code;
typedef uint8 thps_http_data_status_code;

/*!
* \brief HPS Device/Connection Context
*/
typedef struct {
  
  tbt_device_id device_id;
  tconn_id conn_id;
  uint8 *uri;
  uint16 uri_len;
  uint8 *header;
  uint16 header_len;
  uint8 *body;
  uint16 body_len;
  thps_http_req_id control;
  uint8 status_code[MAX_STATUS_CODE_CHAR_SIZE];
  uint16 status_code_ccc;
  boolean is_secured;
  uint8  char_val_updated_mask;
  Queue_t prep_write_q;
  
} thps_service_conn_context;


/***********************************************************************

  Function Declarations

 ************************************************************************/

/*!
 * @brief	HTTP Proxy Service Init API for APP/Profile.
 *			  This API Call Instantiates a Single HTTP Proxy Service Instance
 *			  NOTE: To Add Multiple HTTP Proxy Service Instances, Need to create Copy File of this file and
 *						Modify the names of APIs, Global Variables and Callback function names.
 *
 * @param Void Takes No Parameters
 *
 * @return Void Returns None
 */
void hps_service_init(void);


/*!
 * @brief  Function to send status as Gatt Notification to HTTP client
 *
 * @param device_id Pointer to Remote Device Id
 * @param http_status HTTP Response status
 * @param dstatus HTTP data status
 *
 * @return Void Returns None
 */
void hps_service_set_and_send_status(tbt_device_id *device_id, uint16 http_status, uint8 dstatus);

#endif //_DIS_SERVICE_H

