
/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */


#include <wmstdio.h>
#include <wm_os.h>
#include <mdev.h>
#include <mdev_uart.h>
#include <mdev_sdio.h>
#include <cli.h>
#include <wlan.h>
#include <partition.h>
#include <flash.h>
#include <wmstdio.h>
#include <stdlib.h>

#include "mrvlstack_config.h"
#include "main_gap_api.h"
#include "memory_manager.h"

#include "gap_service.h"
#include "hps_service.h"


t_device_config_data config;


static void ble_app_cmd_menu(int argc, char *argv[])
{

   int opt = 0;

   if (argc == 2) {
      opt = atoi(argv[1]);
   } else {
	wmprintf("Please enter valid option \r\n");
	wmprintf("Usage: ble-opt <number>\r\n");

#if ((defined (MRVL_STACK_PERIPHERAL_ROLE_ENABLED) && MRVL_STACK_PERIPHERAL_ROLE_ENABLED == TRUE)) 
	wmprintf(" where number = 1 - Start advertising\r\n");
	wmprintf("       number = 2 - Stop advertising\r\n");
#endif

      return;
   }

  switch(opt)
  {
#if ((defined (MRVL_STACK_PERIPHERAL_ROLE_ENABLED) && MRVL_STACK_PERIPHERAL_ROLE_ENABLED == TRUE))

  case 1:
     {
        
        //Enable Advertising
        uint8 adv_data[] =  { 0x02,0x01,0x00, 0x02,0x0A,0xFF,0x07,0x03,0x00,0x18,0x01,0x18,0x12,0x18, 0x02, 0x1C, 0x01};
        uint8 scan_data[] = { 0x03, 0x03, 0x01, 0x18, 0x02, 0x1C, 0x01, 0x0b,0x09,'M','R','V','L','-','W','M','S','D','K'};
        
        uint8 buffer[64];
        
       
        tbt_advertising_data_params *gap_adv_data = (tbt_advertising_data_params *)buffer ;
        
        tble_advertising_params default_advt_params = { GAP_BLE_DEFAULT_ADVERTISING_INT_MIN,GAP_BLE_DEFAULT_ADVERTISING_INT_MAX,
          GAP_BLE_DEFAULT_ADVERTISING_TYPE,GAP_BLE_DEFAULT_OWN_ADDRESS_TYPE,0,
          {0,0,0,0,0,0},GAP_BLE_DEFAULT_ADVERTISING_CHANNEL_MAP,GAP_BLE_DEFAULT_ADVERTISING_FILTER_POLICY };

        wmprintf(" Start Advertising\r\n");         
       
        gap_adv_data->length = sizeof(adv_data);
        
        gap_adv_data->scan_rsp_data = FALSE;
         
        memcpy(gap_adv_data->data, adv_data, sizeof(adv_data));
        
        config.type = BT_CONFIG_T_ADVERTISING_DATA;
        
        config.len = sizeof(tbt_advertising_data_params) + gap_adv_data->length ;
        
        config.p.adv_data = *gap_adv_data;
        
        GAP_Config_Device(&config);
              
        gap_adv_data->length = sizeof( scan_data);
        
        memcpy(gap_adv_data->data, scan_data, sizeof( scan_data));
        
        gap_adv_data->scan_rsp_data = TRUE;
        
        config.type = BT_CONFIG_T_ADVERTISING_DATA;
        
        config.len = sizeof(tbt_advertising_data_params) + gap_adv_data->length ;
        
        config.p.adv_data = *gap_adv_data;
        
        GAP_Config_Device(&config);
        
        config.len = sizeof(tble_advertising_params);
        
        config.type = BT_CONFIG_T_ADVERTISING_PARAMS;
        
        //default_advt_params.interval_max = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MAX;
        
        //default_advt_params.interval_min = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MIN;
        
        config.p.adv_params= default_advt_params;
        
        GAP_Config_Device(&config);
        
        config.len = 0x02;
        
        config.type = BT_CONFIG_T_BLE_DEVICE_MODE;
        
        //config.p.mode = GAP_BLE_GENERAL_DISCOVERABLE | GAP_BLE_NON_CONNECTABLE_BROADCASTING;
        
        config.p.mode = GAP_BLE_GENERAL_DISCOVERABLE | GAP_BLE_CONNECTABLE;
        
        GAP_Config_Device(&config);
        
        //Enable Advertising~~   
     }
     break;
  case 2:
     {
        
        //Disable Advertising     
     
        tble_advertising_params default_advt_params = { GAP_BLE_DEFAULT_ADVERTISING_INT_MIN,GAP_BLE_DEFAULT_ADVERTISING_INT_MAX,
          GAP_BLE_DEFAULT_ADVERTISING_TYPE,GAP_BLE_DEFAULT_OWN_ADDRESS_TYPE,0,
          {0,0,0,0,0,0},GAP_BLE_DEFAULT_ADVERTISING_CHANNEL_MAP,GAP_BLE_DEFAULT_ADVERTISING_FILTER_POLICY };

        wmprintf(" Stop Advertising\r\n");     
        
        config.len = sizeof(tble_advertising_params);
        config.type = BT_CONFIG_T_ADVERTISING_PARAMS;

        default_advt_params.interval_max = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MAX;
        default_advt_params.interval_min = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MIN;
       
        config.p.adv_params= default_advt_params;
  
        GAP_Config_Device(&config);
  
        config.len = 0x02;
  
        config.type = BT_CONFIG_T_BLE_DEVICE_MODE;
        config.p.mode = GAP_BLE_NON_DISCOVERABLE | GAP_BLE_NON_CONNECTABLE;
  
  
        GAP_Config_Device(&config);
     }
     break;
#endif

  default:
     break;
  }
   
}


static struct cli_command commands[] = {
      {"ble-opt", NULL, ble_app_cmd_menu},
};

static int ble_app_cli_init(void)
{
   int i;
   for (i = 0; i < sizeof(commands) / sizeof(struct cli_command); i++)
      if (cli_register_command(&commands[i]))
         return 1;
   return 0;
}



void ble_app_handler(t_bt_gap_event_id gap_event_id,
	t_bt_gap_events_data *gap_event_data)
{
  
  
  switch(gap_event_id)
  {
    case BT_ENABLE_COMPLETED_EVENT:
      {
        tbt_security_property_data sec_prop_data;
        
        wmprintf("BT_ENABLE_COMPLETED_EVENT\r\n");

        /* CLI Interface for BLE application */
        ble_app_cli_init();

	/* GAP Service Init */
  	gap_serv_init();

	/*HPS Service Init */
	hps_service_init();

        wmprintf("HTTP Proxy Service Registered\r\n");

        sec_prop_data.bond_mode = BT_LE_GENERAL_BONDING_MODE;  /* No persistance storage */
        sec_prop_data.io_cap = BT_SEC_CAP_NO_IO;               /* To enforce, Just works model */
        sec_prop_data.key_size = 0x10;
        sec_prop_data.sec_level = BT_SEC_UNAUTHENTICATED_SECURITY_LEVEL;

        //Setting Security Properties
        config.len = sizeof(tbt_security_property_data);

        config.type = BT_CONFIG_T_SECURITY_PROPERTY_DATA;

        config.p.sec_prop_data = sec_prop_data;

        GAP_Config_Device(&config);
        return;

      }
      break;


#if (((defined (SMP_MASTER_ENABLED) && SMP_MASTER_ENABLED == TRUE)) \
    || ((defined (SMP_SLAVE_ENABLED) && SMP_SLAVE_ENABLED == TRUE)))


    case BT_GAP_SEC_START_BOND_REQ_EVT:
      {
        t_bt_security_event_data  *sec_event_data = (t_bt_security_event_data*)gap_event_data;
        tbt_sec_events_resp_parms resp_params;

        wmprintf("ble demo app handler: BT_GAP_SEC_START_BOND_REQ_EVT: sec_event_data->bond_mode = %d \r\n", sec_event_data->bond_mode);
        resp_params.bond_mode = sec_event_data->bond_mode;
        resp_params.confirm = TRUE;
        resp_params.device_id = sec_event_data->device_id;
        resp_params.sec_event_id = sec_event_data->sec_event_id;
        GAP_Sec_Events_Response(&resp_params);
      }
      break;

    case BT_GAP_SEC_PASSKEY_DISPLAY_REQ_EVT:
      {  
        wmprintf("Passkey %ld\n",gap_event_data->sec_event_data.p.passkey);
        /*No Response Required*/
      }
      break;

    case BT_GAP_SEC_BONDING_COMPLETE_EVT:
      {  
        
        t_bt_security_event_data  *sec_event_data = (t_bt_security_event_data*)gap_event_data;
        
        wmprintf(" Bonding is completed Status %d Security Level %d\r\n",sec_event_data->p.sec_cmplt.result,
            sec_event_data->p.sec_cmplt.sec_level);
      }
      break;

#endif

    case BT_GAP_CONNECTION_PARAMS_UPDATED_EVT:
      {
        t_bt_ble_connect_updated_params *connParams = (t_bt_ble_connect_updated_params *)gap_event_data;
        
        wmprintf("UPDATED CONNECTION PARAMETERS: ConnINT=%x, ConnLatency=%x, Supervision_TO=%x, BT_RESULT=%d \r\n",connParams->conn_interval,connParams->conn_latency,
            connParams->super_tmo,connParams->result);
        
      }
      break;

#if (((defined (MRVL_STACK_PERIPHERAL_ROLE_ENABLED) && MRVL_STACK_PERIPHERAL_ROLE_ENABLED == TRUE)) \
     || ((defined (MRVL_STACK_CENTRAL_ROLE_ENABLED) && MRVL_STACK_CENTRAL_ROLE_ENABLED == TRUE)))


    case BT_GAP_LINK_CONNECT_COMPLETE_EVT:
      {
        t_bt_connect_state_update_params *conn_params = (t_bt_connect_state_update_params *)gap_event_data;
        wmprintf("GAP_CB: BT_GAP_LINK_CONNECT_COMPLETE_EVT: Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X \r\n",
            conn_params->device_id.address[0],conn_params->device_id.address[1],
            conn_params->device_id.address[2],conn_params->device_id.address[3],
            conn_params->device_id.address[4],conn_params->device_id.address[5]);
        
        wmprintf("GAP_CB: BT_GAP_LINK_CONNECT_COMPLETE_EVT: Device Type %d Address Type %d \r\n",
                             conn_params->device_id.device_type,conn_params->device_id.address_type);



      }
      break;
    case BT_GAP_LINK_DISCONNECT_COMPLETE_EVT:
      {
        t_bt_connect_state_update_params *conn_params = (t_bt_connect_state_update_params *)gap_event_data;
        wmprintf("GAP_CB:  BT_GAP_LINK_DISCONNECT_COMPLETE_EVT: Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X \r\n",
            conn_params->device_id.address[0],conn_params->device_id.address[1],
            conn_params->device_id.address[2],conn_params->device_id.address[3],
            conn_params->device_id.address[4],conn_params->device_id.address[5]);
      }
      break;
#endif


#if ((defined BT_SECURITY_FEATURE_ENABLED) && (BT_SECURITY_FEATURE_ENABLED == TRUE) )
    case BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT:
      {
         t_bt_encrypt_state_change_params *encrypt_params = (t_bt_encrypt_state_change_params*)gap_event_data;
         wmprintf("GAP_CB:  BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT: Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X \r\n",
                  encrypt_params->device_id.address[0],encrypt_params->device_id.address[1],
                  encrypt_params->device_id.address[2],encrypt_params->device_id.address[3],
                  encrypt_params->device_id.address[4],encrypt_params->device_id.address[5]);
      }
      break;
#endif
    default:
      break;
  }
}
