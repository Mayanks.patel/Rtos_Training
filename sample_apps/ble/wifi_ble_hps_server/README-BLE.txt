
*** This document shows the wifi_ble_hps_server.bin application usage ****

A. Pre-requisite:

1. You should have the appropriate chipsets that support both Wi-Fi + BLE.
2. wifi_ble_hps_server.bin app should be compiled and flashed on the board.
3. Need a remote device with BLE support. This application makes DUT
   as HTTP Proxy Service Server, So we need Remote device with HTTP
   Proxy Service Client.
4. DUT must have access to Network to execute HTTP Requests 

B. Steps to execute on console (minicom) for demo.

1. When wifi_ble_hps_server.bin file is flashed, you will observe below prints, which
indicate that firmware is loaded, BT driver is registered and bluetooth stack is
initialized.

[sdio] Card detected
[sdio] Card reset successful
[sdio] Card Version - (0x32)
[sdio] Card initialization successful
wmsdk ble init starting bluetooth 
[bt_drv] revision=0x10
[bt_drv] IOPORT : (0x10000)
[bt_drv] Firmware Ready
[bt_drv] BT driver initialized
[af] app_ctrl: prev_fw_version=0
[net] Initializing TCP/IP stack
[ftfs] part1:  be_ver: 100
[fi_ble_hps_server.binftfs] part2:  be_ver: -1
[ftfs] be_ver = 100 active = 0
[af] app_ctrl [sta]: State Change: INIT_SYSTEM => CONFIGURED
[af] app_ctrl [sta]: State Change: CONFIGURED => NORMAL_INIT
[af] app_ctrl [sta]: State Change: NORMAL_INIT => NORMAL_CONNECTING
BT_ENABLE_COMPLETED_EVENT
HTTP Proxy Service Registered
[af] network_mgr: network loaded successfully
[net] configuring interface mlan (with DHCP client)
[af] app_ctrl [sta]: State Change: NORMAL_CONNECTING => NORMAL_CONNECTED

# [net] configuring interface mlan (with DHCP client)
[af] app_ctrl [sta]: State Change: NORMAL_CONNECTING => NORMAL_CONNECTED





2. Press "Enter" key to go to command prompt
3. Type "help" at command prompt to show ble related test options

# help

<snip>

ble-opt

<snip>

4. You can press respective options which show its usage. for e.g. If you type ble-opt

# ble-opt 
Please enter valid option 
Usage: ble-opt <number>
 where number = 1 - Start advertising
       number = 2 - Stop advertising
 

5. To start advertising, so that remote devices can search us, use option 1

Pre-condition: On a remote device, which supports BLE, make sure bluetooth is
switched ON

# ble-opt 1
Selected menu option =1
Start Advertising

If you perform discovery (Scanning) from remote device, device with name
"MRVL-WMSDK" should now show up. This is friendly name of our Bluetooth device.



"When Remote device Start Pairing, we can see below log on console (minicom)"

GAP_CB: BT_GAP_LINK_CONNECT_COMPLETE_EVT: Device 0x8F:0xBA:0x21:0x43:0x50:0x00
GAP_CB: BT_GAP_LINK_CONNECT_COMPLETE_EVT: Device Type 1 Address Type 0
ble demo app handler: BT_GAP_SEC_START_BOND_REQ_EVT: sec_event_data->bond_mode = 0
GAP_CB:  BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT: Device 0x8F:0xBA:0x21:0x43:0x50:0x00
 Bonding is completed Status 0 Security Level 0


"When Remote HPS Client device initiates GET request, we can see something like below on console (minicom)"

open session url =http://httpbin.org/get

#
# status code received from server: 200

Received HTTP Header:
HTTP/1.1 200 OK
Server: nginx
Date: Fri, 15 May 2015 11:40:20 GMT
Content-Type: application/json
Content-Length: 159
Connection: close
Access-Control-Allow-Origin: *
Access-Control-Allow-Credentials: true


Received HTTP Body:
{
  "args": {},
  "headers": {
    "Host": "httpbin.org",
    "User-Agent": "WMSDK"
  },
  "origin": "122.182.31.132",
  "url": "http://httpbin.org/get"
}

Received HTTP Body:
********* All data read **********
http_code : 200 data 159 header 0 dstatus 0x05 header_len 215 data_len 159


