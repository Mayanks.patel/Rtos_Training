exec-y += wifi_ble_hps_server
HPS_MDNS_ENABLE=y

wifi_ble_hps_server-objs-y := src/main.c \
	src/reset_prov_helper.c \
	src/wps_helper.c \
	src/ble.c \
	src/gap_service.c \
	src/hps_service.c \
	src/simple_http_req_handler.c

wifi_ble_hps_server-cflags-y := -DWM_IOT_PLATFORM=TRUE -DDUMP_HPS_HTTP_RESPONSE=1 -I$(d)/src


wifi_ble_hps_server-objs-$(HPS_MDNS_ENABLE) += src/mdns_helper.c
wifi_ble_hps_server-cflags-$(HPS_MDNS_ENABLE) += -DAPPCONFIG_MDNS_ENABLE


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
