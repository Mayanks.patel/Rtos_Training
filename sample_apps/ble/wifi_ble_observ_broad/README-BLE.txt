
*** This document shows the wifi_ble_observ_broad application usage ****

A. Pre-requisite:

1. You should have the appropriate chipsets that support both Wi-Fi + BLE.
2. wifi_ble_observ_board app should be compiled and flashed on the board.
3. Need a remote device with BLE support. 

B. Steps to execute on console (minicom) for demo.

1. When wifi_ble_observ_board.bin file is flashed, you will observe below prints, which
indicate that firmware is loaded, BT driver is registered and bluetooth stack is
initialized.

# [sdio] Card detected
[sdio] Card reset successful
[sdio] Card Version - (0x32)
[sdio] Card initialization successful
[bt_drv] revision=0x10
[bt_drv] IOPORT: (0x10000)
[bt_drv] Firmware Ready
[bt_drv] BT driver initialized
[af] app_ctrl: prev_fw_version=0
[net] Initializing TCP/IP stack
wifi_ble_demo app handler: event id 1
BT_ENABLE_COMPLETED_EVENT


2. Press "Enter" key to go to command prompt
3. Type "help" at command prompt to show ble related test options

# help

<snip>

ble-opt
<snip>

4. You can press respective options which show its usage. for e.g. If you type ble-opt

# ble-opt 
Please enter valid option 
Usage: ble-opt <number>
 where number = 1 - Start Broadcasting
       number = 2 - Stop Broadcasting
       number = 3 - Start Observer [<Remote address> <address type>] (observer role) 
       number = 4 - Stop Observer
 

5. To start broadcast, so that remote devices can search us, use option 1

Pre-condition: On a remote device, which supports BLE, make sure bluetooth is
switched ON

# ble-opt 1
Selected menu option =1
Start Broadcasting

 If remote device now moves to "observer" mode, it would see our device show up.

6. To stop broadcast, use option 2.

# ble-opt 2
 Selected menu option =2
 Stop Broadcasting


7. In order to enter "observer" mode to find nearby remote devices, use option 3.
  In case you want to scan a known device address only, then pass the optional parameters of 
  remote device address to be observed and its address type. If you want to observe all devices, 
  then these two parameters are not needed

Pre-condition: Make sure, that remote device with BLE support has its bluetooth
switched ON and is advertising, only then the remote device would show up in our
discovery result.

# ble-opt 3 [<ES:DR:AD:TE:MO:RE> <Address_type>]  ( For a particular device )
 OR
# ble-opt 3  (Default case )
 Start Observer 

We will start to observe for nearby devices (or only one, if address is passed as parameter) and it 
displays devices found on console.

Sample output is as below:

----

# BT_GAP_ADVERTISEMENT_REPORT_EVENT Device 0x8F:0xBA:0x21:0x43:0x50:0x00
Device Type 1 Address Type 0 
Report Type: 3 Len 24 rssi 0xbc
0x02 0x01 0x04 0x02 0x0a 0x00 0x05 0x03 0x00 0x18 0x01 0x18 0x0b 0x09 0x4d 0x52 0x56 0x6c 0x5f 0x53 0x44 0x4b 0x2d 0x31 

----

Note: In observer role, we keep on searching for nearby devices and it will display all found 
      devices on console. There is no timer to automatically stop this.

8. In order to stop observer role, use option 4

# ble-opt 4

# Stopped Observer 


