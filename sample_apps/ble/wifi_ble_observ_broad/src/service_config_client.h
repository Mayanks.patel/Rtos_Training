/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   service_config_client.h
 * \brief  This file contains the Common Macros, data structures and function API declarations 
 *        that would be used by different Gatt Services(ex: bas_service, scps service, etc).
 * \author Marvell Semiconductor
 */


#ifndef _SERVICE_CONFIG_CLIENT_H
#define _SERVICE_CONFIG_CLIENT_H

#include "mrvlstack_config.h"
#include "main_gap_declares.h"
#include "main_gatt_api.h"
#include "log.h"


/* Trace Statements */
#if ((defined MRVL_STACK_DEBUG_ENABLED) && (MRVL_STACK_DEBUG_ENABLED == TRUE))  && defined CL_SRVC_DEBUG_ENABLED

extern uint8 CL_SRVC_MSG_level; 

#define CL_SRVC_ERROR(m, arg...)                    {if (CL_SRVC_MSG_level >= ERROR) print(m, ##arg);}
#define CL_SRVC_WARNING(m, arg...)                  {if (CL_SRVC_MSG_level >= WARNING) print(m, ##arg );}
#define CL_SRVC_DEBUG(m, arg...)                    {if (CL_SRVC_MSG_level >= DEBUG) print(m, ##arg);}
#define CL_SRVC_ALL(m, arg...)                      {if (CL_SRVC_MSG_level >= ALL) print(m, ##arg);}
#else
#define CL_SRVC_ERROR(m, arg...)
#define CL_SRVC_WARNING(m, arg...)
#define CL_SRVC_DEBUG(m, arg...)
#define CL_SRVC_ALL(m, arg...)
#endif


/***********************************************************************

  Macro Definitions & Data Sturctures

 ************************************************************************/
#define TO_NOTIFY                (0x0001)
#define TO_INDICATE              (0x0002)
#define MAX_SRVC_DATA            10         ///< Maximum Gatt Server Attribute Value that will be READ from or WRITTEN into server DB.

typedef void *tSrvc_connid; ///< Pointer to app/profile connection context(tDIS_AppRegInfo) is passed as connection id to App/Profile 

/*!
* \brief Host state machine states for Service
*/
typedef enum {
  SRVC_IDLE_ST = 0,
  SRVC_CONNECTING_ST,
  SRVC_CONNECTED_ST,
  SRVC_REDISCOVERY_ST,    
  //   SRVC_DISCONNECTED_ST,
  //   SRVC_W4_SEC - TBD: Security procedure 
};
typedef uint8 tSRVC_STATE;

/*!
* \brief Client Characteristic Configuration Descriptor
*/
typedef struct {
  uint16 ccc_hdl;
  //  uint16 ccc_val;
}tSRVC_CharDscr_CCC;

/*!
* \brief HOGP Host Application data to be saved to NVM(Non Volatile Memory)
*/
typedef struct
{
  boolean valid_data;
  tbt_remote_device remote_dev;
  boolean is_bg_conn;
  thandle_range hids_handle_range;
  thandle_range dis_handle_range;
  thandle_range bas_handle_range;
  thandle_range scps_handle_range;  
}tHOGP_app_client_nvm_data;

/***********************************************************************

  Function Declarations

 ************************************************************************/


#endif //_SERVICE_CONFIG_CLIENT_H
