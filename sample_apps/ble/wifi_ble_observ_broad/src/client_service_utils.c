/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   client_service_utils.c
 * \brief  This file contains the utility functions used by different services
 * \author Marvell Semiconductor
 */

#include "service_config_client.h"
#include "dis_client.h"
#include "bas_client.h"
#include "scps_client.h"
#include "hids_client.h"
#include "memory_manager.h"
#include "oss.h"
#include "main_gatt_api.h"
#include "log.h"


/***********************************************************************

  Sturcture & Function Declarations

 ************************************************************************/


/***********************************************************************

  Macro Definitions, Global Variables & Data Sturctures

 ************************************************************************/
#if ((defined MRVL_STACK_DEBUG_ENABLED) && (MRVL_STACK_DEBUG_ENABLED == TRUE))
#ifndef CL_SRVC_DEBUG_ENABLED
#define CL_SRVC_DEBUG_ENABLED
#endif

extern uint8 CL_SRVC_MSG_level = ALL; 
#endif

/*!
 * @brief  Function to get the Device Context from Device Id/Gatt Client Connection Id
 *
 * @param p_dev_id Pointer to Remote Device Id
 * @param connid Gatt Client Connection Id
 * @param p_dev_context_q Pointer to Device Context Queue
 * @return tHIDS_dev_context* Pointer to Device Context
 */
tHIDS_dev_context* hids_cl_utils_find_dev_context(tbt_device_id *p_dev_id, tconn_id connid, Queue_t *p_dev_context_q)
{
  tHIDS_dev_context *p_dev_cont = NULL;
  qp pq_element = NULL;

  for(pq_element = peekq_head(p_dev_context_q); pq_element != NULL; pq_element = pq_element->qElement_next)
  {
    p_dev_cont = (tHIDS_dev_context *)BML_DATA_PTR(pq_element);
    if(p_dev_cont->gattc_connid == connid ||
        (p_dev_id && oss_memcmp((uint8 *)(&(p_dev_cont->device_id)), sizeof(tbt_device_id), (uint8 *)p_dev_id, sizeof(tbt_device_id))))
    {
      return p_dev_cont;
    }
  }

  return (tHIDS_dev_context *)NULL;
}


/*!
 * @brief  Function to Allocate Memory for Transport Context and Enqueue it in given Queue
 *
 * @param p_dev_context_q Pointer to Device Context Queue
 * @return tHIDS_dev_context* Pointer to Device Context
 */
tHIDS_dev_context* hids_cl_utils_alloc_enqueue_trans_context(Queue_t *p_dev_context_q)
{
  BufferDesc_t *pbuf = NULL;
  tHIDS_dev_context *p_dev_cont = NULL;

  pbuf = (BufferDesc_t *)STACK_ALLOC_BUFFER(sizeof(tHIDS_dev_context));

  if(!pbuf)
  {
    CL_SRVC_DEBUG("Not enough Memory!! Required tHIDS_dev_context size = %d, pbuf=%p\n",sizeof(tHIDS_dev_context), pbuf);
    return NULL;
  }

  p_dev_cont = (tHIDS_dev_context *)BML_DATA_PTR(pbuf);

  oss_memset((uint8 *)p_dev_cont, 0x00, sizeof(tHIDS_dev_context));
  CL_SRVC_DEBUG("queue head =%p", ((tHIDS_dev_context *)(p_dev_cont))->hids_srvc[0].rpt_char.q_head);
  p_dev_cont->mtu = GATT_DEF_LE_MTU_SIZE;

  putq(p_dev_context_q, (void *)pbuf);
  return p_dev_cont;      
}


/*!
 * @brief  Function to get the HIDS Report Characteristic from Report Value Handle
 *
 * @param rpt_val_hdl HIDS Report Characteristic Value Handle
 * @param p_rpt_char_q Pointer to HIDS Report Characteristic Queue
 * @return tHIDS_RPT_Char* Pointer to HIDS Report Characteristic
 */
tHIDS_RPT_Char* hids_cl_utils_find_hids_report(uint16 rpt_val_hdl, Queue_t *p_rpt_char_q)
{
  tHIDS_RPT_Char *p_rpt_char = NULL;
  qp pq_element = NULL;

  //CL_SRVC_DEBUG("Not enough Memory!! Required tHIDS_dev_context size = %d, pbuf=%p\n",sizeof(tHIDS_dev_context), pbuf);

  for(pq_element = peekq_head(p_rpt_char_q); pq_element != NULL; pq_element = pq_element->qElement_next)
  {
    CL_SRVC_DEBUG("hids_cl_utils_find_hids_report: pq_element=%p\n",pq_element);
    p_rpt_char = (tHIDS_RPT_Char *)BML_DATA_PTR(pq_element);
    if(p_rpt_char->rpt_val_hdl == rpt_val_hdl)
    {
      return p_rpt_char;
    }
  }

  return (tHIDS_RPT_Char *)NULL;
}


/*!
 * @brief  Function to Allocate Memory for HIDS Report Characteristic and Enqueue it in given Queue
 *
 * @param p_rpt_char_q Pointer to HIDS Report Characteristic Queue
 * @return tHIDS_RPT_Char* Pointer to HIDS Report Characteristic
 */
tHIDS_RPT_Char* hids_cl_utils_alloc_enqueue_hids_report(Queue_t *p_rpt_char_q)
{
  BufferDesc_t *pbuf = NULL;
  tHIDS_RPT_Char *p_rpt_char = NULL;

  pbuf = (BufferDesc_t *)STACK_ALLOC_BUFFER(sizeof(tHIDS_RPT_Char));

#if defined(CL_SRVC_DEBUG_ENABLED)
  if(!pbuf)
  {
    CL_SRVC_DEBUG("Not enough Memory!!\n");
    return NULL;
  }
#endif

  p_rpt_char = (tHIDS_RPT_Char *)BML_DATA_PTR(pbuf);

  oss_memset((uint8 *)p_rpt_char, 0x00, sizeof(tHIDS_RPT_Char));

  putq(p_rpt_char_q, (void *)pbuf);
  return p_rpt_char;      
}


/*!
 * @brief  Function to Allocate Memory for Transport Context and Enqueue it in given Queue
 *
 * @param p_dev_context_q Pointer to Device Context Queue
 * @return tDIS_dev_context* Pointer to Device Context
 */
tDIS_dev_context* dis_cl_utils_alloc_enqueue_trans_context(Queue_t *p_dev_context_q)
{
  BufferDesc_t *pbuf = NULL;
  tDIS_dev_context *p_dev_cont = NULL;

  pbuf = (BufferDesc_t *)STACK_ALLOC_BUFFER(sizeof(tDIS_dev_context));

  if(!pbuf)
  {
    CL_SRVC_DEBUG("Not enough Memory!!\n");
    return NULL;
  }

  p_dev_cont = (tDIS_dev_context *)BML_DATA_PTR(pbuf);

  oss_memset((uint8 *)p_dev_cont, 0x00, sizeof(tDIS_dev_context));
  p_dev_cont->mtu = GATT_DEF_LE_MTU_SIZE;

  putq(p_dev_context_q, (void *)pbuf);
  return p_dev_cont;      
}


/*!
 * @brief  Function to get the Device Context from Device Id/Gatt Client Connection Id
 *
 * @param p_dev_id Pointer to Remote Device Id
 * @param connid Gatt Client Connection Id
 * @param p_dev_context_q Pointer to Device Context Queue
 * @return tDIS_dev_context* Pointer to Device Context
 */
tDIS_dev_context* dis_cl_utils_find_dev_context(tbt_device_id *p_dev_id, tconn_id connid, Queue_t *p_dev_context_q)
{
  tDIS_dev_context *p_dev_cont = NULL;
  qp pq_element = NULL;

  for(pq_element = peekq_head(p_dev_context_q); pq_element != NULL; pq_element = pq_element->qElement_next)
  {
    p_dev_cont = (tDIS_dev_context *)BML_DATA_PTR(pq_element);
    if(p_dev_cont->gattc_connid == connid ||
        (p_dev_id && oss_memcmp((uint8 *)(&(p_dev_cont->device_id)), sizeof(tbt_device_id), (uint8 *)p_dev_id, sizeof(tbt_device_id))))
    {
      return p_dev_cont;
    }
  }     
  return (tDIS_dev_context *)NULL;
}


/*!
 * @brief  Function to Allocate Memory for Transport Context and Enqueue it in given Queue
 *
 * @param p_dev_context_q Pointer to Device Context Queue
 * @return tSCPS_dev_context* Pointer to Device Context
 */
tSCPS_dev_context* scps_cl_utils_alloc_enqueue_trans_context(Queue_t *p_dev_context_q)
{
  BufferDesc_t *pbuf = NULL;
  tSCPS_dev_context *p_dev_cont = NULL;

  pbuf = (BufferDesc_t *)STACK_ALLOC_BUFFER(sizeof(tSCPS_dev_context));

  if(!pbuf)
  {
    CL_SRVC_DEBUG("Not enough Memory!!\n");
    return NULL;
  }

  p_dev_cont = (tSCPS_dev_context *)BML_DATA_PTR(pbuf);

  oss_memset((uint8 *)p_dev_cont, 0x00, sizeof(tSCPS_dev_context));
  p_dev_cont->mtu = GATT_DEF_LE_MTU_SIZE;

  putq(p_dev_context_q, (void *)pbuf);
  return p_dev_cont;      
}


/*!
 * @brief  Function to get the Device Context from Device Id/Gatt Client Connection Id
 *
 * @param p_dev_id Pointer to Remote Device Id
 * @param connid Gatt Client Connection Id
 * @param p_dev_context_q Pointer to Device Context Queue
 * @return tSCPS_dev_context* Pointer to Device Context
 */
tSCPS_dev_context* scps_cl_utils_find_dev_context(tbt_device_id *p_dev_id, tconn_id connid, Queue_t *p_dev_context_q)
{
  tSCPS_dev_context *p_dev_cont = NULL;
  qp pq_element = NULL;

  for(pq_element = peekq_head(p_dev_context_q); pq_element != NULL; pq_element = pq_element->qElement_next)
  {
    p_dev_cont = (tSCPS_dev_context *)BML_DATA_PTR(pq_element);
    if(p_dev_cont->gattc_connid == connid ||
        (p_dev_id && oss_memcmp((uint8 *)(&(p_dev_cont->device_id)), sizeof(tbt_device_id), (uint8 *)p_dev_id, sizeof(tbt_device_id))))
    {
      return p_dev_cont;
    }
  }     
  return (tSCPS_dev_context *)NULL;
}


/*!
 * @brief  Function to Allocate Memory for Transport Context and Enqueue it in given Queue
 *
 * @param p_dev_context_q Pointer to Device Context Queue
 * @return tBAS_dev_context* Pointer to Device Context
 */
tBAS_dev_context* bas_cl_utils_alloc_enqueue_trans_context(Queue_t *p_dev_context_q)
{
  BufferDesc_t *pbuf = NULL;
  tBAS_dev_context *p_dev_cont = NULL;

  pbuf = (BufferDesc_t *)STACK_ALLOC_BUFFER(sizeof(tBAS_dev_context));

  if(!pbuf)
  {
    CL_SRVC_DEBUG("Not enough Memory!!\n");
    return NULL;
  }

  p_dev_cont = (tBAS_dev_context *)BML_DATA_PTR(pbuf);

  oss_memset((uint8 *)p_dev_cont, 0x00, sizeof(tBAS_dev_context));
  p_dev_cont->mtu = GATT_DEF_LE_MTU_SIZE;

  putq(p_dev_context_q, (void *)pbuf);
  return p_dev_cont;      
}


/*!
 * @brief  Function to get the Device Context from Device Id/Gatt Client Connection Id
 *
 * @param p_dev_id Pointer to Remote Device Id
 * @param connid Gatt Client Connection Id
 * @param p_dev_context_q Pointer to Device Context Queue
 * @return tBAS_dev_context* Pointer to Device Context
 */
tBAS_dev_context* bas_cl_utils_find_dev_context(tbt_device_id *p_dev_id, tconn_id connid, Queue_t *p_dev_context_q)
{
  tBAS_dev_context *p_dev_cont = NULL;
  qp pq_element = NULL;

  for(pq_element = peekq_head(p_dev_context_q); pq_element != NULL; pq_element = pq_element->qElement_next)
  {
    p_dev_cont = (tBAS_dev_context *)BML_DATA_PTR(pq_element);
    if(p_dev_cont->gattc_connid == connid ||
        (p_dev_id && oss_memcmp((uint8 *)(&(p_dev_cont->device_id)), sizeof(tbt_device_id), (uint8 *)p_dev_id, sizeof(tbt_device_id))))
    {
      return p_dev_cont;
    }
  }     
  return (tBAS_dev_context *)NULL;
}

