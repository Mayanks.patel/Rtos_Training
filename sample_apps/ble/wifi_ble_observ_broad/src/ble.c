
/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */


#include <wmstdio.h>
#include <wm_os.h>
#include <mdev.h>
#include <mdev_uart.h>
#include <mdev_sdio.h>
#include <cli.h>
#include <wlan.h>
#include <partition.h>
#include <flash.h>
#include <wmstdio.h>
#include <stdlib.h>

#include "mrvlstack_config.h"
#include "main_gap_api.h"
#include "memory_manager.h"

#include "oss.h"

t_device_config_data config;


#if ((defined MRVL_STACK_OBSERVER_ROLE_ENABLED ) &&   ( MRVL_STACK_OBSERVER_ROLE_ENABLED == TRUE ))

static void utils_skip_delimits(uint8 **ptr, uint8 delimits)
{
  while(**ptr == delimits)(*ptr)++;
}

static void utils_get_device_addr(uint8** params, tbt_device_id *device_id)
{
  uint8 *endptr;
  uint8 i;

  utils_skip_delimits(params,' ');

  for(i=0;i<BD_ADDRESS_SIZE;i++)
  {
    device_id->address[i] = strtol((char *)*params,(char **)&endptr,16);
    *params = endptr + 1;
  }
}
#endif

static void ble_app_cmd_menu(int argc, char *argv[])
{
    int opt = 0;

    if (argc >= 2) {
	opt = atoi(argv[1]);
    } else {
	wmprintf("Please enter valid option \r\n");
	wmprintf("Usage: ble-opt <number>\r\n");

	wmprintf(" where number = 1 - Start Broadcasting\r\n");
	wmprintf("       number = 2 - Stop Broadcasting \r\n");
	wmprintf("       number = 3 - Start Observer [<Remote addr><addr type>]\r\n\r");
	wmprintf("       number = 4 - Stop Observer \r\n");

	return;
    }

    switch(opt)
    {
 	case 1:
	{
	    //Enable Broadcasting
	    uint8 adv_data[]=   { 0x02,0x01,0x00, 0x02,0x0A,0xFF,0x05,0x03,0x00,0x18,0x01,0x18,0x0B,0x09,'M','R','V','L','_','W','M','S','D','K'};

	    uint8 buffer[64];

	    tbt_advertising_data_params *gap_adv_data = (tbt_advertising_data_params *)buffer ;

            tble_advertising_params default_advt_params = { GAP_BLE_DEFAULT_ADVERTISING_INT_MIN,GAP_BLE_DEFAULT_ADVERTISING_INT_MAX,
			GAP_BLE_DEFAULT_ADVERTISING_TYPE,GAP_BLE_DEFAULT_OWN_ADDRESS_TYPE,0,
			{0,0,0,0,0,0},GAP_BLE_DEFAULT_ADVERTISING_CHANNEL_MAP,GAP_BLE_DEFAULT_ADVERTISING_FILTER_POLICY };	

	    wmprintf(" Start Broadcasting\r\n");        

	   /* 1. Call GAP_Config_Device with type BT_CONFIG_T_ADVERTISING_DATA to set 
		 advertisement data. scan_rsp_data should be FALSE
              2. Call GAP_Config_Device with type BT_CONFIG_T_ADVERTISING_PARAMS to
	         set advertisement parameters (default_advt_params)
              3. Call GAP_Config_Device with type BT_CONFIG_T_BLE_DEVICE_MODE to set 
		 mode 
	    */
				 

	    gap_adv_data->length = sizeof(adv_data);
	    gap_adv_data->scan_rsp_data = FALSE;
	    memcpy(gap_adv_data->data, adv_data, sizeof(adv_data));
	    config.type = BT_CONFIG_T_ADVERTISING_DATA;
	    config.len = sizeof(tbt_advertising_data_params) + gap_adv_data->length ;
	    config.p.adv_data = *gap_adv_data;
	    GAP_Config_Device(&config);

	    config.len = sizeof(tble_advertising_params);
	    config.type = BT_CONFIG_T_ADVERTISING_PARAMS;
	    default_advt_params.interval_max = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MAX;
	    default_advt_params.interval_min = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MIN;
	    config.p.adv_params= default_advt_params;
	    GAP_Config_Device(&config);

	    config.len = 0x02;
	    config.type = BT_CONFIG_T_BLE_DEVICE_MODE;
	    config.p.mode = GAP_BLE_NON_DISCOVERABLE | GAP_BLE_NON_CONNECTABLE_BROADCASTING;
	    GAP_Config_Device(&config);

	}
	break;

	case 2:
	{
	    //Disable Broadcasting     

	    tble_advertising_params default_advt_params = { GAP_BLE_DEFAULT_ADVERTISING_INT_MIN,GAP_BLE_DEFAULT_ADVERTISING_INT_MAX,
			GAP_BLE_DEFAULT_ADVERTISING_TYPE,GAP_BLE_DEFAULT_OWN_ADDRESS_TYPE,0,
			{0,0,0,0,0,0},GAP_BLE_DEFAULT_ADVERTISING_CHANNEL_MAP,GAP_BLE_DEFAULT_ADVERTISING_FILTER_POLICY };

	    wmprintf(" Stop Broadcasting\r\n");     

	    config.len = sizeof(tble_advertising_params);
	    config.type = BT_CONFIG_T_ADVERTISING_PARAMS;
	    default_advt_params.interval_max = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MAX;
	    default_advt_params.interval_min = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MIN;
	    config.p.adv_params= default_advt_params;
	    GAP_Config_Device(&config);

	    config.len = 0x02;
	    config.type = BT_CONFIG_T_BLE_DEVICE_MODE;
	    config.p.mode = GAP_BLE_NON_DISCOVERABLE | GAP_BLE_NON_CONNECTABLE;
	    GAP_Config_Device(&config);
        }
	break;

#if ((defined MRVL_STACK_OBSERVER_ROLE_ENABLED ) &&   ( MRVL_STACK_OBSERVER_ROLE_ENABLED == TRUE ))
 
	case 3:
	{
	    wmprintf(" Start Observer \r\n");    

	    tbt_discovery_params *pdiscovery_params;

            BufferDesc_t *pbuff;
            tbt_remote_device remote_dev;

            if(argc > 2 )
            {
          
    	      utils_get_device_addr((uint8 **)&argv[2], &remote_dev.device_id);
         
	      remote_dev.device_id.device_type = BT_DEVICE_TYPE_BLE;
         
	      remote_dev.device_id.address_type = atoi(argv[3]);
         
              pbuff  = (BufferDesc_t *)STACK_ALLOC_BUFFER( ( sizeof(tbt_discovery_params) + sizeof(tbt_device_id) ));
          
              pdiscovery_params = (tbt_discovery_params *)BML_DATA_PTR(pbuff);
          
              pdiscovery_params->ble_discovery_params.numwhlistdev = 0x01;
          
              oss_memcopy((uint8*)(pdiscovery_params+1),&remote_dev.device_id,sizeof(tbt_device_id));
          
              pdiscovery_params->ble_discovery_params.ble_scan_params.scan_filter_policy = GAP_BLE_SCAN_FP_ACCEPT_WHLIST_DEVS_ADV;
            }
            else
            {
	      pbuff  = (BufferDesc_t *)STACK_ALLOC_BUFFER( ( sizeof(tbt_discovery_params) ) );
          
	      pdiscovery_params = (tbt_discovery_params *)BML_DATA_PTR(pbuff);
          
 	      pdiscovery_params->ble_discovery_params.ble_scan_params.scan_filter_policy = 0x00; //Accept All Filter.
            }

            pdiscovery_params->ble_discovery_params.ble_scan_params.duplicate_filters  = 0x01;
            pdiscovery_params->ble_discovery_params.ble_scan_params.scan_address_type = 0x00;
            pdiscovery_params->ble_discovery_params.ble_scan_params.scan_interval = 18;
            pdiscovery_params->ble_discovery_params.ble_scan_params.scan_window = 18;
            pdiscovery_params->ble_discovery_params.ble_scan_params.scan_type = 0x01; //0x00 No Scan Request 0x01 Scan Req 
            pdiscovery_params->discovery_duration = 4000;
            pdiscovery_params->discovery_mode = 0x04;
   
            GAP_Start_Discovery(pdiscovery_params);

           STACK_FREE_BUFFER(BML_SUB_DESC(pdiscovery_params));
        }
	break;

  	case 4:
	{
	    // Stop Observer
	    GAP_Cancel_Discovery();
	}
	break;
#endif
	default:
	break;
    }

}

static struct cli_command commands[] = {
    {"ble-opt", NULL, ble_app_cmd_menu},
};

static int ble_app_cli_init(void)
{
    int i;
    for (i = 0; i < sizeof(commands) / sizeof(struct cli_command); i++)
	if (cli_register_command(&commands[i]))
		return 1;
    return 0;
}

void ble_app_handler(t_bt_gap_event_id gap_event_id,
	t_bt_gap_events_data *gap_event_data)
{
    switch(gap_event_id)
    {
 	case BT_ENABLE_COMPLETED_EVENT:
	{

        wmprintf("BT_ENABLE_COMPLETED_EVENT\r\n");

        t_bt_enable_complete_event_params *local_address_params =
                       (t_bt_enable_complete_event_params*)gap_event_data;

        wmprintf("BT_GAP_LOCAL_DEVICE_ADDRESS: Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X\r\n",
                     local_address_params->device_id.address[5],local_address_params->device_id.address[4],
                     local_address_params->device_id.address[3],local_address_params->device_id.address[2],
                     local_address_params->device_id.address[1],local_address_params->device_id.address[0]);

        wmprintf("Device Address type : %s \r\n", local_address_params->device_id.address_type ? "Random" : "Public" );

        wmprintf("Device type : %s \r\n", (local_address_params->device_id.device_type== BT_DEVICE_TYPE_BLE) ? "BLE" : 
				  ((local_address_params->device_id.device_type == BT_DEVICE_TYPE_BR_EDR) ? "BR_EDR" : "Dual Mode" ));


	     /* CLI Interface for BLE application */
	     ble_app_cli_init();

	     return;
	}
	break;

	case BT_DISCOVERY_COMPLETED_EVENT:
	{
		wmprintf("Stopped Observer \r\n");
	}
	break;

	case BT_GAP_ADVERTISEMENT_REPORT_EVENT:
	{
		t_bt_adv_report *adv_report = (t_bt_adv_report*)gap_event_data;
		uint8 i;
		wmprintf("BT_GAP_ADVERTISEMENT_REPORT_EVENT Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X""\r\n",
				adv_report->device_id.address[0],adv_report->device_id.address[1],
				adv_report->device_id.address[2],adv_report->device_id.address[3],
				adv_report->device_id.address[4],adv_report->device_id.address[5]);
		wmprintf("Device Type %d Address Type %d \r\n",adv_report->device_id.device_type,
				adv_report->device_id.address_type);
		wmprintf("Report Type: %d Len %d rssi 0x%02x\r\n",adv_report->type, adv_report->len,adv_report->rssi);

		for(i=0;i<adv_report->len;i++)
			wmprintf("0x%02x ",adv_report->value[i]);

		wmprintf("\r\n\n\n");
		os_thread_sleep(os_msec_to_ticks(5));
	}
	break;

	default:
	  break;
     }
}
