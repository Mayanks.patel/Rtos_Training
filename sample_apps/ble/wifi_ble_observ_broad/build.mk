exec-y += wifi_ble_observ_broad
OBSERV_MDNS_ENABLE=y

wifi_ble_observ_broad-objs-y := src/main.c \
	src/reset_prov_helper.c \
	src/wps_helper.c \
	src/ble.c

wifi_ble_observ_broad-cflags-y := -DWM_IOT_PLATFORM=TRUE -I$(d)/src

wifi_ble_observ_broad-objs-$(OBSERV_MDNS_ENABLE) += src/mdns_helper.c
wifi_ble_observ_broad-cflags-$(OBSERV_MDNS_ENABLE) += -DAPPCONFIG_MDNS_ENABLE


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
