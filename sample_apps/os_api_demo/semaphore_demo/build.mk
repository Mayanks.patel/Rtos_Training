

exec-y += semaphore_demo
semaphore_demo-objs-y := \
		src/main.c \
		src/semaphore_demo.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

semaphore_demo-supported-toolchain-y := arm_gcc iar
