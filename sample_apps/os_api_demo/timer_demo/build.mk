
exec-y += timer_demo
timer_demo-objs-y := \
		src/main.c \
		src/timer_demo.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

timer_demo-supported-toolchain-y := arm_gcc iar
