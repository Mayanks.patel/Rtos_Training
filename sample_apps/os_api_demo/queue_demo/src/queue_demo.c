/*
 *  Copyright (C) 2008-2016 Marvell International Ltd.
 *  All Rights Reserved.
 */

#include <wm_os.h>
#include <stdlib.h>
#include <wm_utils.h>
#define WAIT_FOR_MS 1000
#define MAX_SLEEP 5000
#define THREAD_COUNT 2
static os_thread_stack_define(test_thread_stack1, 512);
static os_thread_stack_define(test_thread_stack2, 512);
static os_thread_t test_thread_handle[THREAD_COUNT];
os_queue_t *test_queue;

typedef struct message_type {
	int data;
	int sender;
} message_type;

/*
 *  Name: test_produce
 *
 *  Description: The process emulates a thread writing an element to the queue
 *  then printing the total number of the elements remaining in the queue.
 *  We send an integer number to the queue using os_queue send.
 *  Thread sends a message to queue, then sleeps for a random amount of time
 *  before sending the message again.
 */

static void test_produce(void *arg)
{
	static int data = 1;
	int thread_index = (int) arg;
	while (1) {
		int sleep_time = rand()%MAX_SLEEP;
		message_type message;
		message.data = data;
		message.sender = thread_index;
		int ret = os_queue_send(test_queue, &message, WAIT_FOR_MS);
		if (ret != WM_SUCCESS) {
			wmprintf("Producer process failed\r\n");
		} else {
			wmprintf("Produced %d into the queue by thread-%d.\r\n",
				 message.data, message.sender);
			data += 1;
		}
		os_thread_sleep(os_msec_to_ticks(sleep_time));
	}
	test_thread_handle[thread_index] = NULL;
	os_thread_delete(NULL);
}

/*
 *  Name: test_consume
 *
 *  Description: The process emulates a thread reading an element from the
 *  queue and printing it to the screen. In this process we recieve an integer
 *  from the queue and print that to screen. This process sleeps for random
 *  amount of time before repeating the above procedure.
 */

static void test_consume(void *arg)
{
	int thread_index = (int) arg;
	message_type *message = (message_type *)malloc(sizeof(message_type));
	while (1) {
		int sleep_time = rand()%MAX_SLEEP;
		os_thread_sleep(os_msec_to_ticks(sleep_time));
		int ret = os_queue_recv(test_queue, message, WAIT_FOR_MS);
		if (ret != WM_SUCCESS) {
			wmprintf("Consumer process failed\r\n");
		} else {
			wmprintf("Consumed %d from queue. ", message->data);
			wmprintf("Produced by : ");
			switch ((int)(message->sender)) {
			case 0:
				wmprintf("thread-0.\r\n");
				break;
			case 1:
				wmprintf("thread-1.\r\n");
				break;
			default:
				break;
			}
			wmprintf("Now the queue has %d elements.\r\n",
				 os_queue_get_msgs_waiting(test_queue));
		}
	}
	test_thread_handle[thread_index] = NULL;
	os_thread_delete(NULL);
}

/*
 *  Name: queue_demo()
 *
 *
 *  Description : We start with creating two threads and a queue. These
 *  threads take turns producing and consuming the data in the queue.
 *  using the create and consume functions respectively.
 */
int queue_demo()
{
	test_queue = (os_queue_t *)malloc(sizeof(os_queue_t));
	/* Define a pool of length 10 for the queue using the macro */
	os_queue_pool_define(queue_pool, sizeof(message_type)*10);
	/* Now creating a queue in the pool defined before */
	int ret = os_queue_create(test_queue, "test-queue",
				  sizeof(message_type), &queue_pool);
	if (ret != WM_SUCCESS) {
		wmprintf("Cannot create queue \r\n");
		return ret;
	}
	/* Check the number of Items in the queue  */
	wmprintf("Initially the queue has %d elements.\r\n",
		 os_queue_get_msgs_waiting(test_queue));
	int index;
	index = 0;
	ret = os_thread_create(&test_thread_handle[index],
			       "thread1", test_produce, (void *) index,
			       &test_thread_stack1,
			       OS_PRIO_1);
	if (ret != WM_SUCCESS) {
		wmprintf("Thread creation failed\r\n");
		return ret;
	}
	index += 1;
	ret = os_thread_create(&test_thread_handle[index],
			       "thread2", test_produce, (void *) index,
			       &test_thread_stack1,
			       OS_PRIO_2);
	if (ret != WM_SUCCESS) {
		wmprintf("Thread creation failed\r\n");
		return ret;
	}
	index += 1;
	ret = os_thread_create(&test_thread_handle[index],
			       "thread3", test_consume, (void *) index,
			       &test_thread_stack2,
			       OS_PRIO_3);
	if (ret != WM_SUCCESS) {
		wmprintf("Thread creation failed\r\n");
		return ret;
	}
	return 1;
}
