/*
 *  Copyright (C) 2008-2016 Marvell International Ltd.
 *  All Rights Reserved.
 */
/*
 *  Queue demo
 *
 *  Summary: An app showing the working of the queue API in the SDK.
 *
 *  Description:
 *  Create a queue of size 10 and emulate two threads sending and recieving
 *  data to and from the queue. We use two entities produce and consume to do
 *  so. The produce process sends a message to the queue, and waits for random
 *  time before sending data again. On the other hand there is the consumer
 *  process which recieves an item from the queue and waits for a random
 *  amount of time before receiving from the queue again.
 */

#include <wmstdio.h>
#include <wm_os.h>
#include <stdlib.h>

#define SEED 23

int queue_demo(void);

int main(void)
{
	/* Initialize console on uart0 */
	wmstdio_init(UART0_ID, 0);
	wmprintf("Demo started:\r\n");
	srand(SEED);
	queue_demo();
	return 0;
}
