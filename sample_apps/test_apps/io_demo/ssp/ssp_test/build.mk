# Copyright (C) 2008-2016 Marvell International Ltd.
# All Rights Reserved.

exec-y += ssp_test
ssp_test-objs-y := src/main.c src/ssp.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

ssp_test-supported-toolchain-y := arm_gcc iar
