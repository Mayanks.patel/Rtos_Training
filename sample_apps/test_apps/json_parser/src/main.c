/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */

/* Test Application to test the json_parser
 * Just compile and run on the board and observe results on the serial console
 */

#include <string.h>
#include <wmstdio.h>
#include <wmerrno.h>
#include <json_parser.h>
#include <cli.h>

#define test_obj_bool	"{\"temp\":\"val\",\"name\":true}"
#define test_obj_int	"{\"name\":25, \"name2\":[1,2,{\"n2\":3}]}"
#define test_obj_float	"{\"arr\":[1,2], \"name\":25.58}"
#define test_obj_string	"{\"name\":\"value\"}"
#define test_obj_comp_object	"{\"name\":{\"name2\":3},"\
	"\"name3\":{\"name4\":4}}"

#define test_obj_array_bool	"{\"temp\":3, \"name\":[true,false,false]}"
#define test_obj_array_int	"{\"name\":[1,2,345,76,0,-241]}"
#define test_obj_array_float	"{\"name\":[1.2,3.4,9,56.78,0,-31.2]}"
#define test_obj_array_str	"{\"name\":[\"elm1\",\"elm2\",\"elm 3\"]}"
#define test_obj_arr_obj	"{\"array\":"\
	"[{\"name1\":\"val1\"},{\"name1\":\"val2\"}]}"

#define test_arr_obj	"[{\"name1\":\"val1\"},{\"name1\":\"val2\"}]"
#define test_arr_array	"[[1,2],[3,4]]"

#define test_empty_obj	"{}"
#define test_empty_arr	"[]"

#define test_invalid1	"{\"name\":\"value\""
#define test_invalid2	"{\"name1\":\"val1\",\"name2\"}"
#define test_invalid3	"{\"name\":val}"
#define test_invalid4	"[1,2,3"
#define test_invalid5	"\"name\":\"val}"
#define test_invalid6	"[\"name\":\"val\"]"
#define test_invalid7	"{\"name\":\"val1\":\"val2\"}"
#define test_invalid8	"\"name\":\"val\""

#define test_obj_long	"{\n\"name\" :    \"JSON Parser\",\n" \
			"\t\"version\" : 2.0,\n" \
			"\"copyright\" : 2014,\n" \
			"\"supported_el\" :\t [\"bool\",\"int\","\
			"\"float\",\"str\"" \
			",\"object\",\"array\"],\n" \
			"\"features\" : { \"objects\":true, "\
			"\"arrays\":\"yes\"},\n"\
			"\"long\":132457680911}"

void disp_json_string(char *name, char *data_buf)
{
	int i, j;
	wmprintf("%s:", name);
	for (i = 0, j = 0; data_buf[i] != 0; i++, j++) {
		if (!(j % 80))
			wmprintf("\r\n");
		wmprintf("%c", data_buf[i]);
		if (data_buf[i] == '\n') {
			wmprintf("\r");
			j = 0;
		}
	}
	wmprintf("\r\n");
}
#define PARSE_STOP_AND_RETURN {json_parse_stop(&jobj); \
		return sub_test_cnt; \
		}

static int test_passed;
static int test_failed;

static void test(int (*func)(void), const char *name)
{
	int r = func();
	wmprintf("%s: ", name);
	if (r == 0) {
		test_passed++;
		wmprintf("PASSED\r\n\r\n");
	} else {
		test_failed++;
		wmprintf("FAILED subtest: %d\r\n\r\n", r);
	}
}
bool bool_var;
int int_var;
int64_t int64_var;
float float_var;
char str_var[100];

static int test_bool(jobj_t *jobj, char *key)
{
	if (json_get_val_bool(jobj, key, &bool_var) == WM_SUCCESS) {
		wmprintf("Got Bool: %d\r\n", bool_var);
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}
static int test_int(jobj_t *jobj, char *key)
{
	if (json_get_val_int(jobj, key, &int_var) == WM_SUCCESS) {
		wmprintf("Got Int: %d\r\n", int_var);
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}
static int test_int64(jobj_t *jobj, char *key)
{
	if (json_get_val_int64(jobj, key, &int64_var) == WM_SUCCESS) {
		wmprintf("Got Int: %lld\r\n", int64_var);
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}
static int test_float(jobj_t *jobj, char *key)
{
	if (json_get_val_float(jobj, key, &float_var) == WM_SUCCESS) {
		wmprintf("Got Float: %d.%d\r\n", wm_int_part_of(float_var),
				wm_frac_part_of(float_var, 2));
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}
static int test_str(jobj_t *jobj, char *key)
{
	if (json_get_val_str(jobj, key, str_var, sizeof(str_var))
			== WM_SUCCESS) {
		int len = 0;
		json_get_val_str_len(jobj, key, &len);
		wmprintf("Got String: %s, length = %d\r\n", str_var, len);
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}
static int test_array(jobj_t *jobj, char *key)
{
	int num_elem;
	if (json_get_array_object(jobj, key, &num_elem) == WM_SUCCESS) {
		wmprintf("Got array with %d members\r\n", num_elem);
		json_release_array_object(jobj);
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}
static int test_array_bool_by_index(jobj_t *jobj, int index)
{
	if (json_array_get_bool(jobj, index, &bool_var) == WM_SUCCESS) {
		wmprintf("Index: %d, Bool: %d\r\n", index, bool_var);
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}
static int test_array_int_by_index(jobj_t *jobj, int index)
{
	if (json_array_get_int(jobj, index, &int_var) == WM_SUCCESS) {
		wmprintf("Index: %d, Int: %d\r\n", index, int_var);
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}
static int test_array_float_by_index(jobj_t *jobj, int index)
{
	if (json_array_get_float(jobj, index, &float_var)
			== WM_SUCCESS) {
		wmprintf("Index: %d, Float: %d.%d\r\n", index,
				wm_int_part_of(float_var),
				wm_frac_part_of(float_var, 2));
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}

static int test_array_str_by_index(jobj_t *jobj, int index)
{
	if (json_array_get_str(jobj, index, str_var, sizeof(str_var))
			== WM_SUCCESS) {
		wmprintf("Index: %d, Str: %s\r\n", index, str_var);
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}
static int test_array_str(jobj_t *jobj, char *key)
{
	int num_elem;
	if (json_get_array_object(jobj, key, &num_elem) == WM_SUCCESS) {
		wmprintf("Got array with %d members\r\n", num_elem);
		int i;
		for (i = 0; i < num_elem; i++) {
			if (test_array_str_by_index(jobj, i)
					!= WM_SUCCESS) {
				json_release_array_object(jobj);
				return -WM_FAIL;
			}
		}
		json_release_array_object(jobj);
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}
static int test_object(jobj_t *jobj, char *key)
{
	if (json_get_composite_object(jobj, key) == WM_SUCCESS) {
		wmprintf("Got object\r\n");
		json_release_composite_object(jobj);
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}

static int test_obj_bool_fn(void)
{
	wmprintf("String : %s\r\n", test_obj_bool);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_bool, strlen(test_obj_bool));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	ret = test_bool(&jobj, "name");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	ret = test_int(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 4;
	ret = test_float(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 5;
	ret = test_str(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 6;
	ret = test_object(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 7;
	ret = test_array(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}
static int test_obj_int_fn(void)
{
	wmprintf("String : %s\r\n", test_obj_int);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_int, strlen(test_obj_int));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	ret = test_bool(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	ret = test_int(&jobj, "name");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 4;
	ret = test_float(&jobj, "name");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 5;
	ret = test_str(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 6;
	ret = test_object(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 7;
	ret = test_array(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}
static int test_obj_float_fn(void)
{
	wmprintf("String : %s\r\n", test_obj_float);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_float, strlen(test_obj_float));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	ret = test_bool(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	ret = test_int(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 4;
	ret = test_float(&jobj, "name");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 5;
	ret = test_str(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 6;
	ret = test_object(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 7;
	ret = test_array(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}
static int test_obj_string_fn(void)
{
	wmprintf("String : %s\r\n", test_obj_string);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_string, strlen(test_obj_string));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	ret = test_bool(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	ret = test_int(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 4;
	ret = test_float(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 5;
	ret = test_str(&jobj, "name");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 6;
	ret = test_object(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 7;
	ret = test_array(&jobj, "name");
	if (ret == WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}

static int test_obj_comp_object_fn(void)
{
	wmprintf("String : %s\r\n", test_obj_comp_object);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_comp_object,
			strlen(test_obj_comp_object));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	ret = json_get_composite_object(&jobj, "name");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	ret = test_int(&jobj, "name2");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	json_release_composite_object(&jobj);
	sub_test_cnt = 4;
	ret = json_get_composite_object(&jobj, "name3");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 5;
	ret = test_int(&jobj, "name4");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}

static int test_obj_array_bool_fn(void)
{
	wmprintf("String : %s\r\n", test_obj_array_bool);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_array_bool,
			strlen(test_obj_array_bool));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	int num_elem;
	ret = json_get_array_object(&jobj, "name", &num_elem);
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	int i;
	for (i = 0; i < num_elem; i++) {
		ret = test_array_bool_by_index(&jobj, i);
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;
	}

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}

static int test_obj_array_int_fn(void)
{
	wmprintf("String : %s\r\n", test_obj_array_int);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_array_int,
			strlen(test_obj_array_int));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	int num_elem;
	ret = json_get_array_object(&jobj, "name", &num_elem);
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	int i;
	for (i = 0; i < num_elem; i++) {
		ret = test_array_int_by_index(&jobj, i);
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;
	}

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}
static int test_obj_array_float_fn(void)
{
	wmprintf("String : %s\r\n", test_obj_array_float);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_array_float,
			strlen(test_obj_array_float));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	int num_elem;
	ret = json_get_array_object(&jobj, "name", &num_elem);
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	int i;
	for (i = 0; i < num_elem; i++) {
		ret = test_array_float_by_index(&jobj, i);
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;
	}

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}
static int test_obj_array_str_fn(void)
{
	wmprintf("String : %s\r\n", test_obj_array_str);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_array_str,
			strlen(test_obj_array_str));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	int num_elem;
	ret = json_get_array_object(&jobj, "name", &num_elem);
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	int i;
	for (i = 0; i < num_elem; i++) {
		ret = test_array_str_by_index(&jobj, i);
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;
	}

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}

static int test_obj_array_obj_fn(void)
{
	wmprintf("String : %s\r\n", test_obj_arr_obj);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_arr_obj,
			strlen(test_obj_arr_obj));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	int num_elem = 0;
	ret = json_get_array_object(&jobj, "array", &num_elem);
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	int i;
	for (i = 0; i < num_elem; i++) {
		ret = json_array_get_composite_object(&jobj, i);
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;

		sub_test_cnt++;
		ret = test_str(&jobj, "name1");
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;

		sub_test_cnt++;
		ret = json_array_release_composite_object(&jobj);
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;

		sub_test_cnt++;
	}
	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}

static int test_arr_obj_fn(void)
{
	wmprintf("String : %s\r\n", test_arr_obj);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_arr_obj,
			strlen(test_arr_obj));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	int num_elem = 0;
	num_elem = json_array_get_num_elements(&jobj);

	sub_test_cnt = 3;
	int i;
	for (i = 0; i < num_elem; i++) {
		ret = json_array_get_composite_object(&jobj, i);
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;

		sub_test_cnt++;
		ret = test_str(&jobj, "name1");
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;

		sub_test_cnt++;
		ret = json_array_release_composite_object(&jobj);
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;

		sub_test_cnt++;
	}
	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}

static int test_arr_array_fn(void)
{
	wmprintf("String : %s\r\n", test_arr_array);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_arr_array,
			strlen(test_arr_array));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	int num_elem;
	num_elem = json_array_get_num_elements(&jobj);
	if (num_elem < 0)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	int i;
	for (i = 0; i < num_elem; i++) {
		int n;
		ret = json_array_get_array_object(&jobj, i, &n);
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;
		int j;
		for (j = 0; j < n; j++) {
			ret = test_array_int_by_index(&jobj, j);
			if (ret != WM_SUCCESS)
				PARSE_STOP_AND_RETURN;
		}

		ret = json_array_release_array_object(&jobj);
		if (ret != WM_SUCCESS)
			PARSE_STOP_AND_RETURN;
	}
	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}


static int test_long_fn(void)
{
	disp_json_string("String", test_obj_long);
	jobj_t jobj;
	int ret, sub_test_cnt;

	sub_test_cnt = 1;
	ret = json_parse_start(&jobj, test_obj_long, strlen(test_obj_long));
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 2;
	ret = test_str(&jobj, "name");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 3;
	ret = test_float(&jobj, "version");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 4;
	ret = test_int(&jobj, "copyright");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 5;
	ret = test_array_str(&jobj, "supported_el");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 6;
	ret = test_object(&jobj, "features");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 7;
	ret = json_get_composite_object(&jobj, "features");

	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 8;
	ret = test_bool(&jobj, "objects");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 9;
	ret = test_str(&jobj, "arrays");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	json_release_composite_object(&jobj);

	sub_test_cnt = 10;
	ret = test_int64(&jobj, "long");
	if (ret != WM_SUCCESS)
		PARSE_STOP_AND_RETURN;

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}

static int test_empty_obj_fn(void)
{
	wmprintf("String : %s\r\n", test_empty_obj);
	jobj_t jobj;
	int sub_test_cnt;

	sub_test_cnt = 1;
	int ret = json_parse_start(&jobj, test_empty_obj,
			strlen(test_empty_obj));
	if (ret != WM_SUCCESS) {
		PARSE_STOP_AND_RETURN;
	}

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}

static int test_empty_arr_fn(void)
{
	wmprintf("String : %s\r\n", test_empty_arr);
	jobj_t jobj;
	int sub_test_cnt;

	sub_test_cnt = 1;
	int ret = json_parse_start(&jobj, test_empty_arr,
			strlen(test_empty_arr));
	if (ret != WM_SUCCESS) {
		PARSE_STOP_AND_RETURN;
	}

	sub_test_cnt = 0;
	PARSE_STOP_AND_RETURN;
}

int __test_invalid_jsons(char *json)
{
	wmprintf("String : %s\r\n", json);
	jobj_t jobj;
	int sub_test_cnt = WM_SUCCESS;

	int ret = json_parse_start(&jobj, json, strlen(json));
	if (ret == WM_SUCCESS) {
		sub_test_cnt = 1;
	}
	PARSE_STOP_AND_RETURN;
}

static int test_invalid_json1(void)
{
	return __test_invalid_jsons(test_invalid1);

}
static int test_invalid_json2(void)
{
	return __test_invalid_jsons(test_invalid2);

}
static int test_invalid_json3(void)
{
	return __test_invalid_jsons(test_invalid3);

}
static int test_invalid_json4(void)
{
	return __test_invalid_jsons(test_invalid4);

}
static int test_invalid_json5(void)
{
	return __test_invalid_jsons(test_invalid5);

}
static int test_invalid_json6(void)
{
	return __test_invalid_jsons(test_invalid6);

}
static int test_invalid_json7(void)
{
	return __test_invalid_jsons(test_invalid7);

}
static int test_invalid_json8(void)
{
	return __test_invalid_jsons(test_invalid8);

}

int main(void)
{
	int ret = wmstdio_init(UART0_ID, 0);
	if (ret == -WM_FAIL) {
		wmprintf("Failed to initialize console on uart0\r\n");
		return -1;
	}

	wmprintf("\r\n##### Starting JSON Parser Tests #####\r\n\r\n");
	test(test_obj_bool_fn, "Testing for bool in object");
	test(test_obj_int_fn, "Testing for int in object");
	test(test_obj_float_fn, "Testing for float in object");
	test(test_obj_string_fn, "Testing for string in object");
	test(test_obj_comp_object_fn, "Testing Composite Object");
	test(test_obj_array_bool_fn, "Testing for array of bools");
	test(test_obj_array_int_fn, "Testing for array of integers");
	test(test_obj_array_float_fn, "Testing for array of floats");
	test(test_obj_array_str_fn, "Testing for array of strings");
	test(test_obj_array_obj_fn, "Testing for array of objects, "
			"inside an object");
	test(test_arr_obj_fn, "Testing for array of objects");
	test(test_arr_array_fn, "Testing for array of arrays");
	test(test_long_fn, "Testing for long object");
	test(test_empty_obj_fn, "Testing Empty Object");
	test(test_empty_arr_fn, "Testing Empty Array");
	test(test_invalid_json1, "Testing for Invalid JSON 1");
	test(test_invalid_json2, "Testing for Invalid JSON 2");
	test(test_invalid_json3, "Testing for Invalid JSON 3");
	test(test_invalid_json4, "Testing for Invalid JSON 4");
	test(test_invalid_json5, "Testing for Invalid JSON 5");
	test(test_invalid_json6, "Testing for Invalid JSON 6");
	test(test_invalid_json7, "Testing for Invalid JSON 7");
	test(test_invalid_json8, "Testing for Invalid JSON 8");
	wmprintf("Total PASSED: %d, FAILED: %d\r\n",
			test_passed, test_failed);
	return 0;
}
