# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += rw_lock_demo
rw_lock_demo-objs-y := \
		src/main.c \
		src/rp_rw_demo.c \
		src/rw_lock_demo.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

rw_lock_demo-supported-toolchain-y := arm_gcc iar
