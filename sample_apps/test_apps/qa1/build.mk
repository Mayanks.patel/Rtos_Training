# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += qa1
qa1-objs-y := src/main.c

subdir-y += test_subdir

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

qa1-supported-toolchain-y := arm_gcc iar
