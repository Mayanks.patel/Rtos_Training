# Copyright (C) 2008-2016 Marvell International Ltd.
# All Rights Reserved.

exec-y += jsmn_test
jsmn_test-objs-y := src/tests.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

jsmn_test-supported-toolchain-y := arm_gcc iar
