# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += qa4
qa4-objs-y := \
	src/main.c \
	src/reset_prov_helper.c \
	src/wps_helper.c \
	src/mdns_helper.c

qa4-cflags-y := \
	-DAPPCONFIG_MDNS_ENABLE \
	-I$(d)/src/

# Enable for debugging
#qa4-cflags-y += -DAPPCONFIG_DEBUG_ENABLE

qa4-ftfs-y 	:= qa4.ftfs
qa4-ftfs-dir-y  := $(d)/www
qa4-ftfs-api-y 	:= 100

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

qa4-supported-toolchain-y := arm_gcc iar
