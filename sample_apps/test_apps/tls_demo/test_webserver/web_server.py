#!/usr/bin/python

import os
import BaseHTTPServer, SimpleHTTPServer
import ssl
import sys

# --------------------------------------------------------
# To generate all required cert/key pairs, please follow
# instructions from README.md
# --------------------------------------------------------
# server.key: Server's private key
# server.pem: Server's certificate signed by rootCA
# rootCA.pem: rootCA's self-signed certificate
# --------------------------------------------------------

cdir = os.getcwd()
os.chdir(cdir)

portNo = 4443

httpd = BaseHTTPServer.HTTPServer(
        ('', portNo),                   # Specify port number
        SimpleHTTPServer.SimpleHTTPRequestHandler)

httpd.socket = ssl.wrap_socket(
        httpd.socket,
        keyfile='./server.key',
        certfile='./server.pem',
        server_side=True,               # Run webserver in Server mode
        cert_reqs=ssl.CERT_REQUIRED,    # Make client authentication mandatory
        ca_certs='./rootCA.pem')

httpd.serve_forever()
