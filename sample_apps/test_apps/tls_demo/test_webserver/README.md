[comment]: <> (Copyright 2008-2016, Marvell International Ltd. All Rights Reserved.)

# Introduction
This application demonstrates tls *client cert verification*
and various other combinations and testing cyassl error
conditions.

# Overview of steps

Step 1: Creating cert/key pairs using openssl for rootCA, server, client

Step 2: Setting/configuring Python webserver

Step 3: Update application c code with server ip address.

# Step 1: Create a CA, issue server/client certificates
## Links
[cert setup](www.theheat.dk/blog/?p=1023)

## What to take from here

How to create root CA cert/key pair

How to create server cert/key pair (signed by root CA)

How to create client cert/key pair (signed by root CA)

## Prerequisites
	* sudo apt-get install openssl

## Initialization
	* mkdir -p /tmp/test
	* cd /tmp/test

	* mkdir -p certs private csr newcerts crl

	* echo 00 > serial
	* echo 00 > crlnumber
	* touch index.txt

	* sudo cp /etc/ssl/openssl.cnf .
	* sudo sed -i "s/\.\/demoCA/\/tmp\/test/g" openssl.cnf

## Create rootCA cert/key pair
	* openssl genrsa -des3 -passout pass:qwerty -out  private/rootCA.key 2048
	* openssl rsa -passin pass:qwerty -in private/rootCA.key -out private/rootCA.key
	* openssl req -config openssl.cnf -new -x509 -subj '/C=IN/L=India/O=MRVL CA/CN=rootCA' -days 999 -key private/rootCA.key -out certs/rootCA.crt

## Create server cert/key pair
	* openssl genrsa -des3 -passout pass:qwerty -out private/server.key 2048
	* openssl rsa -passin pass:qwerty -in private/server.key -out private/server.key

Note: Here server ip is *'10.31.131.17'*. Please change as per your configuration.
(This is required if client verifies server domain name)

	* openssl req -config openssl.cnf -new -subj '/C=IN/L=India/O=MRVL/CN=10.31.131.17' -key private/server.key -out csr/server.csr
	* openssl ca -batch -config openssl.cnf -days 999 -in csr/server.csr -out certs/server.crt -keyfile private/rootCA.key -cert certs/rootCA.crt -policy policy_anything

## Create client cert/key pair
	* openssl genrsa -des3 -passout pass:qwerty -out private/client.key 2048
	* openssl rsa -passin pass:qwerty -in private/client.key -out private/client.key

Note: Here client ip is *'10.31.130.75'*. Please change as per your configuration.
(It is assumed that client device is provisioned)

	* openssl req -config openssl.cnf -new -subj '/C=IN/L=India/O=MRVL/CN=10.31.130.75' -key private/client.key -out csr/client.csr
	* openssl ca -batch -config openssl.cnf -days 999 -in csr/client.csr -out certs/client.crt -keyfile private/rootCA.key -cert certs/rootCA.crt -policy policy_anything
## Export the client certificate to pkcs12 (Required to access the server from browser)
	* openssl pkcs12 -export -passout pass:qwerty -in certs/client.crt -inkey private/client.key -certfile certs/rootCA.crt -out certs/clientcert.p12

## Setup for client application
	* openssl x509 -inform pem -in certs/rootCA.crt -out certs/rootCA.pem
	* openssl x509 -inform pem -in certs/server.crt -out certs/server.pem
	* openssl x509 -inform pem -in certs/client.crt -out certs/client.pem

Copy *certs/rootCA.pem*, *certs/client.pem*, *private/client.key* to *www/* folder present in application's directory.
These 3 files are used by Client to create client context.

Copy *certs/rootCA.pem*, *certs/server.pem*, *private/server.key* to *test_webserver/* folder present in application's directory.
These 3 files are used by Python webserver.

# Step 2: How to Set Up Python secure webserver
	* cd test_webserver/
	* python web_server.py

This command creates a web_server at https://your_server_ip:4443. You can change this port number by editing *web_server.py* file.

Note: Here *your_server_ip* refers to ip address of the machine on which
web_server is hosted.

## Browser setup

To access server in your browser import rootCA.crt and clientcert.p12 as shown in below link
[cert setup](www.theheat.dk/blog/?p=1023)

## Test your Setup

Open Firefox, go to https://your_server_ip:4443
e.g. https://10.31.131.17:4443

# Step 3: Update server_url in application

In *tls-auth-client.c*, populate variable *'server_url'* with "https://your_server_ip:4443". Compile
the application. Flash .ftfs, .bin files. Then run *'tls-auth-client-test'*
