# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

ifneq ($(CONFIG_ENABLE_HTTPC_SECURE),y)
$(error "Please enable CONFIG_ENABLE_HTTPC_SECURE using menuconfig")
endif

exec-y += tls_demo
tls_demo-objs-y   := \
	src/main.c \
	src/reset_prov_helper.c \
	src/tls-demo.c \
	src/tls-auth-client.c \
	src/tls-httpc.c \
	src/ntpc.c

tls_demo-cflags-y := \
	-I$(d)/src \
	-DAPPCONFIG_DEBUG_ENABLE=1

tls_demo-objs-$(TLS_DEMO_MDNS_ENABLE)   += src/mdns_helper.c
tls_demo-cflags-$(TLS_DEMO_MDNS_ENABLE) += -DAPPCONFIG_MDNS_ENABLE

tls_demo-objs-$(TLS_DEMO_WPS_ENABLE)    += src/wps_helper.c
tls_demo-cflags-$(TLS_DEMO_WPS_ENABLE)  += -DAPPCONFIG_WPS_ENABLE

tls_demo-ftfs-y		:= tls_demo.ftfs
tls_demo-ftfs-dir-y 	:= $(d)/www
tls_demo-ftfs-api-y 	:= 100


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
