/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

/*
 * HTTP TLS Demo
 *
 * Summary:
 *
 * This application provides a command 'tls-http-client' that can be used to
 * open a connection with an https (HTTP over TLS) website. The command connects
 * to the website, downloads the data and dumps the same on the console.
 *
 * Description:
 *
 * The application is written using Application Framework that
 * simplifies development of WLAN networking applications.
 *
 *
 * WLAN Initialization:
 *
 * When the application framework is started, it starts up the WLAN
 * sub-system and initializes the network stack. We receive the event
 * when the WLAN subsystem has been started and initialized.
 *
 * If a provisioned network is found in PSM, we connect to that network. If not,
 * we connect to a pre-defined network with the following settings:
 *
 * SSID : marvell
 * passphrase: marvellwm
 * security: WPA2
 *
 * TLS Transaction :
 *
 * Once the connection is established, a HTTP-TLS session may be initiated by
 * typing the tls-http-client command on the CLI. Since a TLS transaction takes
 * a much larger stack space than is available in the CLI thread, a new thread
 * is spawned to perform the TLS transaction.
 *
 * The thread starts a HTTP session, reads data and prints it all on the
 * console.
 *
 */
#include <wm_os.h>
#include <app_framework.h>
#include <wmtime.h>
#include <partition.h>
#include <appln_cb.h>
#include <appln_dbg.h>
#include <cli.h>
#include <wmstdio.h>
#include <wmsysinfo.h>
#include <wm_net.h>
#include <wps_helper.h>
#include <reset_prov_helper.h>
#include <httpd.h>
#include <led_indicator.h>
#include <board.h>
#include <dhcp-server.h>
#include <wmtime.h>
#include <psm-utils.h>
#include <ftfs.h>
#include <mdev_gpio.h>

#include "tls-demo.h"

#ifndef CONFIG_ENABLE_HTTPC_SECURE
#warning Please enable secure HTTP client for this demo to work correctly
#endif

extern char ftfs_ver[];
int ftfs_api_version = 100;
char *ftfs_part_name = "ftfs";
struct fs *fs;

struct fs *get_ftfs_handle()
{
	return fs;
}

/*
 * Event: INIT_DONE
 *
 * The application framework is initialized.
 *
 * The data field has information passed by boot2 bootloader
 * when it loads the application.
 *
 */
static void event_init_done(void *data)
{
	struct app_init_state *state;

	state = (struct app_init_state *)data;

	dbg("Event: INIT_DONE");
	dbg("Factory reset bit status: %d", state->factory_reset);
	dbg("Booting from backup firmware status: %d", state->backup_fw);
	dbg("Previous reboot cause: %u", state->rst_cause);

	app_ftfs_init(ftfs_api_version, ftfs_part_name, &fs);
}

/*
 * Handler invoked on WLAN_INIT_DONE event.
 *
 */
static void event_wlan_init_done(void *data)
{
	int ret;
	int provisioned = (int)data;

	if (provisioned)
		app_sta_start();
	else {
		ret = psm_cli_init(sys_psm_get_handle(), NULL);
		if (ret != WM_SUCCESS) {
			dbg("Error: psm_cli_init failed");
			return;
		}

		wmprintf("\r\nPlease provision the device "
				"and then reboot it:\r\n\r\n");
		wmprintf("psm-set network ssid <ssid>\r\n");
		wmprintf("psm-set network security <security_type>"
				"\r\n");
		wmprintf("    where: security_type: 0 if open,"
				" 3 if wpa, 4 if wpa2\r\n");
		wmprintf("psm-set network passphrase <passphrase>"
				" [valid only for WPA and WPA2 security]\r\n");
		wmprintf("psm-set network configured 1\r\n");
	}
	/*
	 * Initialize CLI Commands for some of the modules:
	 *
	 * -- wlan: allows user to explore basic wlan functions
	 */
	ret = wlan_cli_init();
	if (ret != WM_SUCCESS)
		dbg("Error: wlan_cli_init failed");

	tls_demo_init();
}


/*
 * Event: CONNECTING
 *
 * We are attempting to connect to the Home Network
 *
 * This is just a transient state as we will either get
 * CONNECTED or have a CONNECTION/AUTH Failure.
 *
 */
static void event_normal_connecting(void *data)
{
	dbg("Connecting to Home Network");
}

/* Event: AF_EVT_NORMAL_CONNECTED
 *
 * Station interface connected to home AP.
 *
 * Network dependent services can be started here. Note that these
 * services need to be stopped on disconnection and
 * reset-to-provisioning event.
 */
static void event_normal_connected(void *data)
{
	char ip[16];

	app_network_ip_get(ip);
	dbg("Connected to Home Network with IP address =%s", ip);

}

/*
 * Event: CONNECT_FAILED
 *
 * We attempted to connect to the Home AP, but the connection did
 * not succeed.
 *
 * This typically indicates:
 *
 * -- The access point could not be found.
 * -- We did not get a valid IP address from the AP
 *
 */
static void event_connect_failed()
{
	dbg("Application Error: Connection Failed");
}


/*
 * Event: LINK LOSS
 *
 * We lost connection to the AP.
 *
 * The App Framework will attempt to reconnect. We don't
 * need to do anything here.
 */
static void event_normal_link_lost(void *data)
{
	dbg("Link Lost");
}


/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_INIT_DONE:
		event_init_done(data);
		break;
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_NORMAL_CONNECTING:
		event_normal_connecting(data);
		break;
	case AF_EVT_NORMAL_CONNECTED:
		event_normal_connected(data);
		break;
	case AF_EVT_NORMAL_CONNECT_FAILED:
		event_connect_failed(data);
		break;
	case AF_EVT_NORMAL_LINK_LOST:
		event_normal_link_lost(data);
		break;
	default:
		break;
	}

	return 0;
}

static void modules_init()
{
	int ret;

	ret = wmstdio_init(UART0_ID, 0);
	if (ret != WM_SUCCESS) {
		while (1)
			;
		/* do nothing -- stall */
	}

	ret = cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: cli_init failed");
		while (1)
			;
		/* do nothing -- stall */
	}
	/* Initialize time subsystem.
	 *
	 * Initializes time to 1/1/1970 epoch 0.
	 */
	ret = wmtime_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: wmtime_init failed");
		while (1)
			;
		/* do nothing -- stall */
	}

	/*
	 * Register Time CLI Commands
	 */
	ret = wmtime_cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: wmtime_cli_init failed");
		while (1)
			;
		/* do nothing -- stall */
	}

	return;
}

int main()
{
	modules_init();

	dbg("Build Time: " __DATE__ " " __TIME__ "");

	/* Start the application framework */
	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		dbg("Failed to start application framework");
		while (1)
			;
		/* do nothing -- stall */
	}
	return 0;
}
