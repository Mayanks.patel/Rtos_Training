/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */
#include <openssl/ssl.h>

#include <wmtypes.h>
#include <wmstdio.h>
#include <wmtime.h>
#include <string.h>
#include <wmstdio.h>
#include <wm_os.h>
#include <wm_net.h>
#include <httpc.h>
#include "ntpc.h"

#include <cli.h>
#include <cli_utils.h>

#include <cyassl/ctaocrypt/settings.h>

#include <appln_dbg.h>
#include <wlan.h>
#include <wm-tls.h>
#include "tls-demo.h"
#include <mdev_aes.h>
#include <app_framework.h>

/*
 * Here 'server_ca_cert_filename' refers to cert of CA which
 * has authenticated the server.
 */
const char server_ca_cert_filename[] = "rootCA.pem";
const char client_cert_filename[] = "client.pem";
const char client_key_filename[] = "client.key";
const char *server_url = "your_server_ip";

/*
 * In this test case we do not run our code in the cli thread context. This
 * is because tls library requires more stack that what the cli thread
 * has. So we have our own thread.
 */

/* Buffer to be used as stack */
static os_thread_stack_define(tls_auth_client_stack, 8192);

#define MAX_DOWNLOAD_DATA 1024

struct fs *get_ftfs_handle(void);

static void tls_auth_client_test()
{
	if (0 == strcmp("your_server_ip", server_url)) {
		wmprintf("Please perform required configurations\r\n");
		wmprintf("Refer to test_webserver/README.md\r\n");
		return;
	}

	wmprintf("Synchronizing time with NTP\r\n");
	ntpc_sync("asia.pool.ntp.org", 0);

	/*
	 * We as a client want to verify server certificate.
	 * In addition to this in this test we will
	 * connnect to a server which will verify client certificate.
	 */

	app_tls_client_ftfs_t cfg;

	memset(&cfg, 0x00, sizeof(app_tls_client_ftfs_t));

	cfg.ca_cert_filename = server_ca_cert_filename;
	cfg.client_cert_filename = client_cert_filename;
	cfg.client_cert_is_chained = false;
	cfg.client_key_filename = client_key_filename;

	SSL_CTX *ctx = app_tls_create_client_context_ftfs(get_ftfs_handle(),
						      &cfg);
	if (!ctx) {
		wmprintf("%s: failed to create client context\r\n",
			 __func__);
		return;
	}

	http_session_t handle;
	http_resp_t *http_resp;
	httpc_cfg_t httpc_cfg;
	memset(&httpc_cfg, 0x00, sizeof(httpc_cfg_t));
	httpc_cfg.ctx = ctx;
	int rv = httpc_get(server_url, &handle,
			   &http_resp, &httpc_cfg);
	if (rv != WM_SUCCESS) {
		tls_purge_client_context(ctx);
		wmprintf("%s: httpc_get: failed\r\n", __func__);
		return;
	}

	wmprintf("Status code: %d\r\n", http_resp->status_code);

	int dsize;
	char buf[MAX_DOWNLOAD_DATA];
	while (1) {
		/* Keep reading data over the HTTPS session and print it out on
		 * the console. */
		dsize = http_read_content(handle, buf, MAX_DOWNLOAD_DATA);
		if (dsize < 0) {
			dbg("Unable to read data on http session: %d", dsize);
			break;
		}

		if (dsize == 0) {
			TLSD("********* All data read **********");
			break;
		}

		int i;
		for (i = 0; i < dsize; i++) {
			if (buf[i] == '\r')
				continue;
			if (buf[i] == '\n') {
				wmprintf("\n\r");
				continue;
			}
			wmprintf("%c", buf[i]);
		}
	}

	http_close_session(&handle);
	tls_purge_client_context(ctx);
}

static void tls_auth_client_main(os_thread_arg_t data)
{
	wmprintf("Heap before: %d\r\n", os_get_free_size());
	tls_auth_client_test();
	wmprintf("Heap after: %d\r\n", os_get_free_size());
	/* Delete myself */
	os_thread_delete(NULL);
}

static void cmd_auth_client_test(int argc, char **argv)
{
	os_thread_create(NULL, /* thread handle */
			 "tls_auth_client", /* thread name */
			 tls_auth_client_main, /* entry function */
			 NULL, /* argument */
			 &tls_auth_client_stack, /* stack */
			 OS_PRIO_2); /* priority - medium low */
}

static struct cli_command tls_test_cmds[] = {
	{ "tls-auth-client-test", "", cmd_auth_client_test},
};

void tls_auth_client_cli_init(void)
{
	/* Register the commands */
	if (cli_register_commands(&tls_test_cmds[0],
		sizeof(tls_test_cmds) / sizeof(struct cli_command))) {
		dbg("Unable to register client auth CLI command");
		while (1)
			;
		/* do nothing -- stall */
	}
}
