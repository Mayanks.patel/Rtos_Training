Audio SDK audio player demo
==============================

This is an example implementation of Marvell's Audio SDK based MP3 player.
This example showcases following components of WMSDK:
* MP3 Decoder
* Audio Play Pipeline

Compilation:

$ cd wmsdk_bundle-3.5.x
$ make mw300_audio_player_demo_defconfig
$ make boot2
$ make -j<no_of_parallel_threads> audio_player_demo.app

Resultant binaries get generated in bin/mw300_audio_player_demo_defconfig/mw300_rd

Programming the board:

$ ./sdk/tools/OpenOCD/flashprog.py -l sample_apps/audio/audio_player_demo/layout.txt \
		--boot2 bin/mw300_audio_player_demo_defconfig/boot2.bin \
		--mcufw bin/mw300_audio_player_demo_defconfig/mw300_rd/audio_player_demo.bin \
		--ftfs bin/mw300_audio_player_demo_defconfig/mw300_rd/audio_player_demo.ftfs \
		--wififw wifi-firmware/mw30x/mw30x_uapsta_14.76.36.p103.bin.xz

Hardware Setup
===============

This section describes the hardware connections between MW300 dev board and WM8731 audio
codec board. WM8731 audio codec board can be purchased from the below URL:
http://www.mouser.com/search/ProductDetail.aspx?R=0virtualkey0virtualkeyMIKROE-506

================================
WM8731		|	MW300	|
================================|
SCK		|	GPIO_46	|
--------------------------------|
MISO		|	GPIO_49	|
--------------------------------|
MOSI		|	GPIO_48	|
--------------------------------|
ADCL		|	GPIO_47	|
--------------------------------|
DACL		|	GPIO_47	|
--------------------------------|
SDA		|	GPIO_4	|
--------------------------------|
SCL		|	GPIO_5	|
--------------------------------|
3.3 Vcc		|	3.3 Vcc	|
--------------------------------|
Gnd		|	Gnd	|
================================

Mp3 decoder
===============
The WMSDK includes a fully functioning mp3 decoder capable of decoding all fixed bitrate
mp3 files. The decoder accepts arbitrary sized chunks of mp3 data (from the file) and thus
user does not need to do any kind of parsing of the file. If the file is being retrieved from
the network, the partial received data can be fed to the decoder as it arrives from the
network.

The CPU consumption (running at 200 MHz) ranges from 30% to 38% for various MP3s depending
on bitrate and sampling freq.

Current supported and tested mp3 types
-------------------------------------------------------
Sample Rate	Bit Rate	Mp3 type	Tested
-------------------------------------------------------
22.05 KHz	56 kbps		V2L3		Yes
44.1 KHz	128 kbps	V1L3		Yes
48 KHz		256 kbps	V1L3		Yes
44.1 KHz	96 kbps		V1L3		No
48 KHz		320 kbps	V1L3		No
-------------------------------------------------------


Audio Play Pipeline
====================
The audio pipeline is a module which helps handles the data flow from the mp3 data source
(network/local filesystem) to the audio codec. For a new product, the user needs to provide:

* Implementation of a callback which returns mp3 data chunk to the pipeline.
* Audio codec driver which sends PCM data from the pipeline to the audio codec.

The SDK provides SSP based drivers for two popular audio codecs viz. NAU8810 and
WM8731. By default, audio_player_demo uses WM8731 audio codec, this can be changed via
menuconfig.

audio_play.h header file contains APIs of all audio play pipeline specific functions.

