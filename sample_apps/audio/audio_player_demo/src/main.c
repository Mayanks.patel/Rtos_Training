/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */
/*
 * A simple audio player demo
 */
#include <wm_os.h>
#include <app_framework.h>
#include <cli.h>
#include <wmstdio.h>
#include <board.h>
#include <wmtime.h>
#include <psm-v2.h>
#include <psm-utils.h>
#include <critical_error.h>
#ifdef CONFIG_WM8731_DRIVER
#include <peripherals/wm8731.h>
#endif
#ifdef CONFIG_NAU881X_DRIVER
#include <peripherals/nau881x.h>
#endif
#include <httpc.h>
#include <ftfs.h>
#include <audio_play.h>
#include <work-queue.h>
#include <system-work-queue.h>

#define m_e(...)				\
	wmlog_e("SSP-I2S", ##__VA_ARGS__)
#define m_w(...)				\
	wmlog_w("SSP-I2S", ##__VA_ARGS__)

#ifdef CONFIG_MAIN_DEBUG
#define m_d(...)				\
	wmlog("SSP-I2S", ##__VA_ARGS__)
#else
#define m_d(...)
#endif /* ! CONFIG_MAIN_DEBUG */

extern char ftfs_ver[];
int ftfs_api_version = 100;
char *ftfs_part_name = "ftfs";
struct fs *fs;

static int play_done;

bool debug_mode;

#ifdef CONFIG_NAU881X_DRIVER
mdev_t *mdev_nau881x;
#endif
#ifdef CONFIG_WM8731_DRIVER
mdev_t *mdev_wm8731;
#endif

struct fs *get_ftfs_handle()
{
	return fs;
}

/* App defined critical error */
enum app_crit_err {
	CRIT_ERR_APP = CRIT_ERR_LAST + 1,
};

/* This function is defined for handling critical error.
 * For this application, we just stall and do nothing when
 * a critical error occurs.
 */
void critical_error(int crit_errno, void *data)
{
	wmprintf("Critical Error %d: %s\r\n", crit_errno,
		 critical_error_msg(crit_errno));
	while (1)
		;
	/* do nothing -- stall */
}

/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	int ret;
	switch (event) {
	case AF_EVT_WLAN_INIT_DONE:
		ret = psm_cli_init(sys_psm_get_handle(), NULL);
		if (ret != WM_SUCCESS)
			wmprintf("Error: psm_cli_init failed\r\n");
		int i = (int)data;

		if (i == APP_NETWORK_NOT_PROVISIONED) {
			wmprintf("\r\nPlease provision the device "
				 "and then reboot it:\r\n\r\n");
			wmprintf("psm-set network ssid <ssid>\r\n");
			wmprintf("psm-set network security <security_type>"
				 "\r\n");
			wmprintf("    where: security_type: 0 if open,"
				 " 3 if wpa, 4 if wpa2\r\n");
			wmprintf("psm-set network passphrase <passphrase>"
				 " [valid only for WPA and WPA2 security]\r\n");
			wmprintf("psm-set network configured 1\r\n");
			wmprintf("pm-reboot\r\n\r\n");
		} else
			app_sta_start();

		break;
	case AF_EVT_NORMAL_CONNECTED:{
			char ip[16];
			app_network_ip_get(ip);
			wmprintf("Connected with IP address = %s\r\n", ip);

		}
		break;
	default:
		break;
	}

	return 0;
}

static void modules_init()
{
	int ret;

	ret = wmstdio_init(UART0_ID, 0);
	if (ret != WM_SUCCESS) {
		wmprintf("Error: wmstdio_init failed\r\n");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	ret = cli_init();
	if (ret != WM_SUCCESS) {
		wmprintf("Error: cli_init failed\r\n");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	ret = pm_cli_init();
	if (ret != WM_SUCCESS) {
		wmprintf("Error: pm_cli_init failed\r\n");
		critical_error(-CRIT_ERR_APP, NULL);
	}
	/* Initialize time subsystem.
	 *
	 * Initializes time to 1/1/1970 epoch 0.
	 */
	ret = wmtime_init();
	if (ret != WM_SUCCESS) {
		wmprintf("Error: wmtime_init failed\r\n");
		critical_error(-CRIT_ERR_APP, NULL);
	}
	ret = wlan_cli_init();
	if (ret != WM_SUCCESS) {
		wmprintf("Error: wlan_cli_init failed\r\n");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	ret = sys_work_queue_init();
	if (ret != WM_SUCCESS) {
		wmprintf("Error: sys wq init failed\r\n");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	app_ftfs_init(ftfs_api_version, ftfs_part_name, &fs);
	ret = ftfs_cli_init(fs);
	if (ret != WM_SUCCESS) {
		wmprintf("Error: wlan_cli_init failed\r\n");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	return;
}

static http_session_t handle;

static void play_event_handler(play_event_t event)
{
	wmprintf("event is %d\r\n", event);
	if (event == AUDIO_PLAY_DONE || event == AUDIO_PLAY_ERROR) {
		if (handle) {
			http_close_session(&handle);
		}
		play_done = 1;
	}
}

static int play_fetch_mp3_handler(void *buffer, int max_len, int *eof)
{
	if (!buffer || !max_len)
		return -WM_E_INVAL;

	int ret = http_read_content(handle, buffer, max_len);
	if (ret == 0)
		*eof = 1;
	return ret;
}

static void audio_http_handler(int argc, char **argv)
{
	http_resp_t *resp = NULL;
	int status = 0;

	if (argc != 2)
		return;

	wmprintf("URL is %s\r\n", argv[1]);

	status = httpc_get(argv[1], &handle, &resp, NULL);
	if (status != WM_SUCCESS) {
		wmprintf("Getting URL failed");
		return;
	}

	audio_play_stop();
	audio_play_start(play_fetch_mp3_handler, play_event_handler);
}

static void audio_stop_handler(int argc, char **argv)
{
	audio_play_stop();
}

static int audio_loop_test_job_func(void *param);

static int max_rand_secs;

static int get_next_trigger_ms()
{
	return rand() % (max_rand_secs * 1000);
}

static void enqueue_next_job(int initial_delay_ms)
{
	wq_job_t wq_job;
	memset(&wq_job, 0x00, sizeof(wq_job_t));
	wq_job.job_func = audio_loop_test_job_func;
	wq_job.initial_delay_ms = initial_delay_ms;
	work_enqueue(sys_work_queue_get_handle(), &wq_job, NULL);
}

static char *url, *ftfs_name;

static int audio_loop_test_job_func(void *param)
{
	static bool toggle_play;
	audio_play_stop();

	if (toggle_play) {
		http_resp_t *resp = NULL;
		int status = httpc_get(url, &handle, &resp, NULL);
		if (status != WM_SUCCESS) {
			wmprintf("Getting URL failed. Aborting");
			return -WM_FAIL;
		}

		audio_play_start(play_fetch_mp3_handler, play_event_handler);
		enqueue_next_job(get_next_trigger_ms());
	} else {
		/* audio_play_local_file(ftfs_name, LOCAL_FILETYPE_MP3); */
		enqueue_next_job(0);
	}

	toggle_play = !toggle_play;
	return WM_SUCCESS;
}

static void audio_play_test_loop(int argc, char **argv)
{
	if (argc != 4) {
		wmprintf("Usage:\r\n%s <http-url> <ftfs-mp3-file-name>"
			 "<rand-max-secs>\r\n", argv[0]);
		return;
	}

	url = argv[1];
	ftfs_name = argv[2];
	max_rand_secs = strtol(argv[3], NULL, 0);
	enqueue_next_job(0);
	while (1)
		os_thread_sleep(5000);
}

static void audio_play_batch_test(int argc, char **argv)
{
	http_resp_t *resp = NULL;
	int status = 0;
	int test_count = 0;
	int i = 0;

	char *test_file[] = {
		"/test1.mp3",
		"/test2.mp3",
		"/test3.mp3",
		"/test4.mp3"
	};

	char url[256] = { 0 };

	if (argc < 3) {
		wmprintf("Invalid arguments\r\n");
		return;
	}

	wmprintf("Server IP Address: %s\r\n", argv[1]);
	wmprintf("Server Port: %s\r\n", argv[2]);
	test_count = sizeof(test_file) / sizeof(test_file[0]);

	wmprintf("No of tests: %d\r\n", test_count);

	for (i = 0; i < test_count; i++) {

		wmprintf("-------------------------------------\r\n\r\n", url);

		snprintf(url, sizeof(url), "%s%s:%s%s", "https://", argv[1],
			 argv[2], test_file[i]);

		wmprintf("URL %s\r\n", url);

		wmprintf("Free Size: %d\r\n", os_get_free_size());

		status = httpc_get(url, &handle, &resp, NULL);
		if (status != WM_SUCCESS) {
			wmprintf("Getting URL failed");
			return;
		}

		audio_play_stop();
		audio_play_start(play_fetch_mp3_handler, play_event_handler);

		while (!play_done) {
			os_thread_sleep(200);
		}

		play_done = 0;
	}

	wmprintf("%s outside loop\r\n", __func__);
}

static struct cli_command cmds[] = {
	{"ap", "<url>", audio_http_handler},
	{"as", NULL, audio_stop_handler},
	{"audio-play-test-loop", "<http-url> <ftfs-mp3-file-name>",
	 audio_play_test_loop},
	{"apbatch", "<Server IP Addr> <Server Port>", audio_play_batch_test},
};

int audio_cli_init(void)
{
	if (cli_register_commands(&cmds[0],
				  sizeof(cmds) / sizeof(struct cli_command))) {
		wmprintf("Unable to register commands\r\n");
	}
	return 0;
}

void init_acodec(void)
{
	/* Initialize AudioCodec H/W */

#ifdef CONFIG_NAU881X_DRIVER
	nau881x_drv_init();
	mdev_nau881x = nau881x_drv_open("nau881x", 0);
	if (mdev_nau881x) {
		struct nau881x_config *acodec_cfg;
		acodec_cfg = nau881x_drv_getconfig(mdev_nau881x);
		nau881x_drv_config(mdev_nau881x, acodec_cfg);
	}
#endif

#ifdef CONFIG_WM8731_DRIVER
	struct wm8731_config *acodec_appcfg;

	/* Initialize AudioCodec H/W, WM8731 is used here */
	wm8731_drv_init();
	mdev_wm8731 = wm8731_drv_open("wm8731", 0);
	if (mdev_wm8731) {
		/* Read default Audio Codec configuration */
		acodec_appcfg = wm8731_drv_getconfig(mdev_wm8731);
		m_d("Default AudioCode Configurations are-");
		m_d("	MCLK Frequency = %d Hz", acodec_appcfg->mclk_freq);
		m_d("	SSP Used = SSP%d_ID", acodec_appcfg->ssp_id);
		m_d("	I2C Used = I2C%d_ID", acodec_appcfg->i2c_id);
		m_d("	MCLK GPIO = GPIO_%d", acodec_appcfg->mclk.gpio);
		/* You can change the configuration here if needed */

		/* Configure MCLK to achive 16KHz as Sampling rate */
		acodec_appcfg->mclk_freq = (16 * 1000 * 256);
		acodec_appcfg->i2c_id = I2C0_PORT;
		/* Configure the Autio Codec */
		wm8731_drv_config(mdev_wm8731, acodec_appcfg);

		m_d("Default AudioCode Configurations are-");
		m_d("	MCLK Frequency = %d Hz", acodec_appcfg->mclk_freq);
		m_d("	SSP Used = SSP%d_ID", acodec_appcfg->ssp_id);
		m_d("	I2C Used = I2C%d_ID", acodec_appcfg->i2c_id);
		m_d("	MCLK GPIO = GPIO_%d", acodec_appcfg->mclk.gpio);
	}
#endif

	wmprintf("%s Audio coded initialized \r\n", __func__);
}

void dump_pipeline_stats(void);

extern uint8_t s_audioblk;
extern uint8_t e_audioblk;

#define AUDIO_BUFFER_PTR (void *)(&s_audioblk)
#define AUDIO_BUFFER_SIZE ((int)&e_audioblk - (int)&s_audioblk)

int main()
{
	wmprintf("Build Time: " __DATE__ " " __TIME__ "\r\n");
	wmprintf("%s Free size:%d\r\n", __func__, os_get_free_size());

	modules_init();

	/* Initialize AudioCodec H/W */
	init_acodec();

	audio_play_cfg_t cfg;
	memset(&cfg, 0x00, sizeof(audio_play_cfg_t));

	cfg.audio_buffer = AUDIO_BUFFER_PTR;
	cfg.audio_buffer_size = AUDIO_BUFFER_SIZE;

#ifdef CONFIG_NAU881X_DRIVER
	nau881x_set_speaker_volume(2);
	cfg.acodec_handle = mdev_nau881x;
#endif
#ifdef CONFIG_WM8731_DRIVER
	cfg.acodec_handle = mdev_wm8731;
#endif
	audio_play_init(cfg);
	audio_cli_init();

	/* Start the application framework */
	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		wmprintf("Failed to start application framework\r\n");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	while (1) {
		os_thread_sleep(2000);
	}
	return 0;
}
