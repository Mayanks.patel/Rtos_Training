# Copyright (C) 2008-2015, Marvell International Ltd.
# All Rights Reserved.

prj_name := audio_player_demo

exec-$(tc-arm_gcc-env-y) += $(prj_name)

$(prj_name)-objs-y := src/main.c

$(prj_name)-ftfs-y 		:= $(prj_name).ftfs
$(prj_name)-ftfs-dir-y     	:= $(d)/www
$(prj_name)-ftfs-api-y 	:= 100

# Applications could also define custom board files if required using following:
#audio_player_demo-board-y := /path/to/boardfile
$(prj_name)-linkerscript-y := $(d)/mw300-xip.ld
