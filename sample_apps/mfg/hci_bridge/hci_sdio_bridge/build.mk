# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y := hci_sdio_bridge
hci_sdio_bridge-objs-y := src/main.c
hci_sdio_bridge-cflags-y := \
		-I$(d)/src \
		-DAPPCONFIG_DEBUG_ENABLE=1
