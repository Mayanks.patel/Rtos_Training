/*
 *  Copyright (C)2008 -2014, Marvell International Ltd.
 *  All Rights Reserved.
 */

/**
 *  @brief This is the HCI Bridge Application
 */
#include <wm_os.h>
#include <mdev.h>
#include <mdev_uart.h>
#include <mdev_sdio.h>
#include <cli.h>
#include <wlan.h>
#include <wmstdio.h>
#include <bt.h>
#include <bt_uart.h>

static os_thread_stack_define(uart_thread_stack, 1024);
static os_thread_t uart_recv_thread;
mdev_t *dev;

static uint8_t out_data, in_data;
static int in_len, out_len;

static void uart_recv(void *t)
{
	while (1) {
		out_len = bt_drv_uart_recv(&out_data, 1);
		uart_drv_write(dev, &out_data, out_len);
		out_len = 0;
	}
}

static int handle_bt()
{
	while (1) {
		in_len = uart_drv_read(dev, &in_data, 1);
		if (in_len) {
			bt_drv_uart_send(&in_data, 1);
			in_len = 0;
		} else
			os_thread_sleep(1);
	}
	return 0;
}

/**
 * All application specific initialization is performed here
 */

int main()
{
	/* Redirect debug prints on UART2 */
	wmstdio_init(UART2_ID, 0);
	wmprintf("HCI UART Bridge Application Started\r\n");
	/* BLE Firmware download */
	ble_interface_t ble_interface = board_ble_interface();
	if (ble_interface.ble_init != NULL)
		ble_interface.ble_init();
	else
		return -WM_FAIL;
	/* HCI UART Init */
	bt_drv_uart_init();

	/* Wait for MB300 to bootup after firmware download */
	os_thread_sleep(500);

	/* Initialize UART1 with 8bit. */
	int rv = uart_drv_init(UART0_ID, UART_8BIT);
	if (rv != WM_SUCCESS)
		return rv;
	/* Open UART1 with 115200 baud rate. */
	dev = uart_drv_open(UART0_ID, 115200);

	/* Create UART recv thread */
	rv = os_thread_create(&uart_recv_thread,
			      "UART-RECV", uart_recv, NULL,
			      &uart_thread_stack,
			      OS_PRIO_3);
	if (rv != WM_SUCCESS)
		wmprintf("Thread Creation Failed\r\n");

	rv = handle_bt();
	if (rv != WM_SUCCESS)
		wmprintf("handle_bt failed\r\n");

	return rv;
}
