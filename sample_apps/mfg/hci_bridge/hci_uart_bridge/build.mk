# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y := hci_uart_bridge
hci_uart_bridge-objs-y := src/main.c
hci_uart_bridge-cflags-y := \
		-I$(d)/src \
		-DAPPCONFIG_DEBUG_ENABLE=1
