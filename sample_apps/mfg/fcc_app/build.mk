# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += fcc_app

fcc_app-objs-y   := src/main.c

fcc_app-cflags-y := \
		-I$(d)/src/ \
		-DAPPCONFIG_DEBUG_ENABLE=1

fcc_app-cflags-$(CONFIG_BT_SUPPORT) += -DAPPCONFIG_BT_SUPPORT



#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

fcc_app-supported-toolchain-y := arm_gcc iar
