# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += uart_wifi_bridge

uart_wifi_bridge-objs-y := src/main.c
uart_wifi_bridge-cflags-y := \
		-I$(d)/src \
		-DAPPCONFIG_DEBUG_ENABLE=1

uart_wifi_bridge-cflags-$(CONFIG_BT_SUPPORT) += \
		-DAPPCONFIG_BT_SUPPORT


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

uart_wifi_bridge-supported-toolchain-y := arm_gcc iar
