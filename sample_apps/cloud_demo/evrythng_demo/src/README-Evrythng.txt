This README explains the procedure to be followed to setup evrythng_demo
application with Evrythng cloud.

Setup Details :

Application : evrythngdemo


Steps:

1. Build the application.
2. Flash it using flashprog.
3. Reset the device.
4. If the device is not provisioned,
   do provisioning using psm commands and reboot the device
5. Device will reboot and  connect to AP.


   Now the device connects to AP and communicates with
   the cloud server.

Cloud Setup Steps:
-----------------
1.  You can use the Evrythng Dashboard to monitor.
    and control individual devices.
2.  To connect to the Dashboard go to:
	https://dev.evrythng.com/
3.  Login using below credentials:
    Login : smartenergymarvell@gmail.com
    Password: marvell88

4.  This will take you to the page where you will find a tab
    "Resources"
    In "Resources" select Products and then "Thngs"
    Thng to be  viewed : Demo-Thng
    Following action can be done on the device:
    a. Press pushbutton 1/2  to turn on/off LED 1/2
    b. LED1/2 is set as an action on teh  Cloud
       on Dashboard action will be registered
