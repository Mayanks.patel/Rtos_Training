# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += evrythng_demo
evrythng_demo-objs-y :=  src/main.c

evrythng_demo-cflags-y := \
		-DMARVELL_PLATFORM \
		-DAPPCONFIG_DEBUG_ENABLE=1 \
		-I$(d)/src


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

evrythng_demo-supported-toolchain-y := arm_gcc iar
