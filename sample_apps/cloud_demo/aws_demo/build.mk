# Copyright (C) 2008-2016 Marvell International Ltd.
# All Rights Reserved.
#

exec-y += aws_demo
aws_demo-objs-y := \
		src/main.c \
		src/device_config.c \
		src/aws_config.c \
		src/event_handler.c

aws_demo-cflags-y :=  \
		-I$(d)/src \
		-DAPPCONFIG_DEBUG_ENABLE=1

ifneq ($(wildcard $(d)/www),)
  aws_demo-ftfs-y 	:= aws_demo.ftfs
  aws_demo-ftfs-dir-y   := $(d)/www
  aws_demo-ftfs-api-y 	:= 100
endif

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

aws_demo-supported-toolchain-y := arm_gcc iar
