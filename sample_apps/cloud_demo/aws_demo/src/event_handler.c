/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */

#include <wmstdio.h>
#include <wm_os.h>
#include <wmtime.h>
/* default configuration parameters */
#include <aws_iot_config.h>

#include "appln_internal.h"

/* Thread handle */
static os_thread_t aws_starter_thread;
/* Buffer to be used as stack */
static os_thread_stack_define(aws_starter_stack, 12 * 1024);

void wlan_event_normal_link_lost(void *data)
{
	/* fast blink led indication to indicate link loss */
}

void wlan_event_normal_connect_failed(void *data)
{
	/* fast blink led indication to indicate connect failed */
}

/* This function gets invoked when station interface connects to home AP.
 * Network dependent services can be started here.
 */
void wlan_event_normal_connected(void *data)
{
	int ret;
	/* Default time set to 1 September 2016 */
	time_t time = 1472688000;
	wmprintf("Connected successfully to the configured network\r\n");

	if (!aws_starter_thread) {
		/* set system time */
		wmtime_time_set_posix(time);

		/* create cloud thread */
		ret = os_thread_create(
			/* thread handle */
			&aws_starter_thread,
			/* thread name */
			"awsStarterDemo",
			/* entry function */
			aws_starter_demo,
			/* argument */
			0,
			/* stack */
			&aws_starter_stack,
			/* priority */
			OS_PRIO_3);
		if (ret != WM_SUCCESS) {
			wmprintf("Failed to start cloud_thread: %d\r\n", ret);
			return;
		}
	}
}
