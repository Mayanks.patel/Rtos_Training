/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */

#ifndef __APPLN_INTERNAL_H__
#define __APPLN_INTERNAL_H__

#include <aws_iot_mqtt_client_interface.h>
#include <aws_iot_shadow_interface.h>

enum state {
	AWS_CONNECTED = 1,
	AWS_RECONNECTED,
	AWS_DISCONNECTED
};

/* Initialize uart, gpio driver and reset to factory push button */
int appln_configure_device();

/* Application thread */
void aws_starter_demo(os_thread_arg_t data);

/* Initialize MQTT and AWS IoT shadow params */
int appln_aws_shadow_config_init(AWS_IoT_Client *mqtt_client,
				 ShadowInitParameters_t *sp,
				 ShadowConnectParameters_t *scp);

/* De-allocate MQTT and AWS IoT shadow params */
void appln_aws_shadow_config_free(AWS_IoT_Client *mqtt_client,
				  ShadowInitParameters_t *sp,
				  ShadowConnectParameters_t *scp);

#endif  /* ! __APPLN_INTERNAL_H__ */
