/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */

/* Sample Application demonstrating bi-directional communication with the AWS
 * IoT Cloud using aws shadow APIs based over MQTT.
 * Using the web application, configure the device to thing shadow or
 * any other shadow using its Thing name.
 *
 * Device publishes the changed state of the device LED controlled by the
 * push-button on Thing Shadow. Also it subscribes to the Thing Shadow delta and
 * when state change of LED is requested from AWS IOT web console, it toggles
 * the LED state.
 *
 * main.c: Application logic
 * device_config.c: Device configuration API definitions
 * aws_config.c: AWS IoT agent configuration API definitions
 * event_handler.c: Network event handlers like connected, link-lost etc.
 */

#include <wm_os.h>
#include <wmstdio.h>
#include <wmtime.h>
#include <wmsdk.h>
#include <led_indicator.h>
#include <push_button.h>
#include <aws_iot_mqtt_client_interface.h>
#include <aws_iot_shadow_interface.h>
#include <aws_utils.h>
/* default configuration parameters */
#include <aws_iot_config.h>

#include "appln_internal.h"

static volatile bool led_state;
static volatile bool led_requested_state;
static output_gpio_cfg_t led_1;
extern enum state device_state;

#define MICRO_AP_SSID           "aws_demo"
#define MICRO_AP_PASSPHRASE     "marvellwm"
#define VAR_LED_1_PROPERTY      "led"
static void pb_cb(int pin, void *data)
{
	led_requested_state = !led_requested_state;
	if (led_requested_state)
		led_on(led_1);
	else
		led_off(led_1);
}
/* Configure led and pushbuttons with callback functions */
static void configure_led_and_button()
{
	led_1 = board_led_1();
	input_gpio_cfg_t pb = {
		.gpio = board_button_1(),
		.type = GPIO_ACTIVE_LOW
	};
	push_button_set_cb(pb, pb_cb, 100, 0, NULL);
}

/* This function will get invoked when led state change request is received */
void led_indicator_cb(const char *p_json_string,
		      uint32_t json_string_datalen,
		      jsonStruct_t *p_context) {
	int state;
	char *ptr;
	unsigned long time;
	static unsigned long delta_timestamp;

	/* To extract timestamp value, parsing string in the form:
	 * 1},"metadata":{"led":{"timestamp":1460703773}}}
	 */
	ptr = strstr(p_json_string, "\"timestamp\":");
	ptr += sizeof("\"timestamp\":") - 1;
	sscanf(ptr, "%lu}}}", &time);

	/* Not updating device state if delta timestamp is already processed */
	if (time <= delta_timestamp)
		return;
	delta_timestamp = time;

	if (p_context != NULL) {
		state = *(int *)(p_context->pData);
		if (state) {
			led_on(led_1);
			led_requested_state = 1;
		} else {
			led_off(led_1);
			led_requested_state = 0;
		}
	}
}

void shadow_update_status_cb(const char *pThingName, ShadowActions_t action,
			     Shadow_Ack_Status_t status,
			     const char *pReceivedJsonDocument,
			     void *pContextData) {

	if (status == SHADOW_ACK_TIMEOUT) {
		wmprintf("Shadow publish state change timeout occurred\r\n");
	} else if (status == SHADOW_ACK_REJECTED) {
		wmprintf("Shadow publish state change rejected\r\n");
	} else if (status == SHADOW_ACK_ACCEPTED) {
		wmprintf("Shadow publish state change accepted\r\n");
	}
}

#define BUFSIZE 256
static AWS_IoT_Client mqtt_client;
static int shadow_publish_device_state(ShadowConnectParameters_t *scp,
				       bool device_state)
{
	char buf_out[BUFSIZE];

	led_state = led_requested_state;
	snprintf(buf_out, BUFSIZE, "{\"state\": {\"reported\":{"
		 "\"%s\":%d}}}", VAR_LED_1_PROPERTY, device_state);
	wmprintf("Publishing '%s' to AWS\r\n", buf_out);
	return aws_iot_shadow_update(&mqtt_client,
				     scp->pMyThingName,
				     buf_out,
				     shadow_update_status_cb,
				     NULL, 10, true);
}

/* application thread gets invoked when device successfully connects to
 * configured network
 */
void aws_starter_demo(os_thread_arg_t data)
{
	int led_delta_state = 0, ret;
	jsonStruct_t led_indicator;
	ShadowInitParameters_t sp;
	ShadowConnectParameters_t scp;

	ret = appln_aws_shadow_config_init(&mqtt_client, &sp, &scp);
	if (ret != WM_SUCCESS) {
		wmprintf("aws shadow config init failed : %d\r\n", ret);
		goto out;
	}

	wmprintf("Shadow Connect\r\n");
	ret = aws_iot_shadow_connect(&mqtt_client, &scp);
	if(ret != AWS_SUCCESS) {
		wmprintf("Shadow Connection Error\r\n");
		goto out;
	}

	ret = aws_iot_shadow_set_autoreconnect_status(&mqtt_client, true);
	if(ret != AWS_SUCCESS) {
		wmprintf("Unable to set Auto Reconnect to true : %d", ret);
		goto out;
	}

	/* indication that device is connected and cloud is started */
	led_on(board_led_2());
	wmprintf("Cloud Started\r\n");

	/* configures property of a thing */
	led_indicator.cb = led_indicator_cb;
	led_indicator.pData = &led_delta_state;
	led_indicator.pKey = "led";
	led_indicator.type = SHADOW_JSON_INT8;

	/* subscribes to delta topic of the configured thing */
	ret = aws_iot_shadow_register_delta(&mqtt_client, &led_indicator);
	if (ret != AWS_SUCCESS) {
		wmprintf("Failed to subscribe to shadow delta %d\r\n", ret);
		goto out;
	}

	/* Once the device connects to the cloud, init values are published
	 * to the cloud. If cloud holds a state which differs from init state,
	 * cloud sends desired state to the device and device will then update
	 * it's state
	 */
	ret = shadow_publish_device_state(&scp, led_state);
	if (ret != WM_SUCCESS) {
		wmprintf("Publishing property failed: %d\r\n", ret);
	}
	led_off(led_1);

	while (1) {

		ret = aws_iot_shadow_yield(&mqtt_client, 200);
		if (ret == NETWORK_ATTEMPTING_RECONNECT) {
			wmprintf("Device trying to reconnect to AWS IoT cloud"
				 "\r\n");
			led_fast_blink(board_led_2());
			os_thread_sleep(os_msec_to_ticks(5000));
			continue;
		} else if (ret == NETWORK_RECONNECTED) {
			wmprintf("Device reconnected to AWS IoT cloud\r\n");
			led_on(board_led_2());
			/* On reconnection, publish device latest state to
			 * cloud
			 */
			led_state = !led_requested_state;
		} else if (ret != AWS_SUCCESS) {
			wmprintf("AWS IoT shadow yield failure %d\r\n", ret);
			goto out;
		}

		if (led_state != led_requested_state) {
			led_state = led_requested_state;
			ret = shadow_publish_device_state(&scp, led_state);
			if (ret != WM_SUCCESS)
				wmprintf("Publishing property failed: "
					 "%d\r\n", ret);
		}
		os_thread_sleep(os_msec_to_ticks(1000));
	}

	ret = aws_iot_shadow_disconnect(&mqtt_client);
	if (ret != WM_SUCCESS) {
		wmprintf("aws iot shadow error %d\r\n", ret);
	}
out:
	appln_aws_shadow_config_free(&mqtt_client, &sp, &scp);
	os_thread_self_complete(NULL);
	return;
}

int main()
{
	appln_configure_device();

	wmprintf("Build Time: " __DATE__ " " __TIME__ "\r\n");
	wmprintf("\r\n#### AWS STARTER DEMO ####\r\n\r\n");

	/* configure led and pushbutton to communicate with cloud */
	configure_led_and_button();

	/* This api adds aws iot configuration support in web application.
	 * Configuration details are then stored in persistent memory.
	 */
	enable_aws_config_support();

	/* This api starts micro-AP if device is not configured, else connects
	 * to configured network stored in persistent memory. Function
	 * wlan_event_normal_connected() is invoked on successful connection.
	 */
	wm_wlan_start(MICRO_AP_SSID, MICRO_AP_PASSPHRASE);
	return 0;
}
