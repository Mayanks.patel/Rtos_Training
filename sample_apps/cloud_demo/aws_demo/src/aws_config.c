/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */

#include <wmstdio.h>
#include <wm_os.h>
#include <aws_iot_mqtt_client_interface.h>
#include <aws_iot_shadow_interface.h>
#include <aws_utils.h>
/* default configuration parameters */
#include <aws_iot_config.h>

#include "aws_starter_root_ca_cert.h"
#include "appln_internal.h"

#define BUFLEN        128
#define REGION_LEN    16
#define MAX_MAC_BYTES 6

/* populate aws shadow configuration details.
 * Default values are defined in aws_iot_config.h
 */
static int aws_starter_load_configuration(AWS_IoT_Client *mqtt_client,
					  ShadowInitParameters_t *sp,
					  ShadowConnectParameters_t *scp)
{
	int ret;
	uint8_t device_mac[MAX_MAC_BYTES];
	/* aws iot thing name */
	char *thing_name = NULL;
	/* aws iot url */
	char *url = NULL;
	/* aws iot client id */
	char *client_id = NULL;
	/* aws iot public certificate */
	char *client_cert_buffer = NULL;
	/* aws iot private key */
	char *private_key_buffer = NULL;

	/* create url by embedding configured region from persistent memory */
	char region[REGION_LEN];
	memset(region, 0, sizeof(region));
	url = os_mem_alloc(BUFLEN);
	if (!url)
		return -WM_E_NOMEM;
	memset(url, 0, sizeof(BUFLEN));
	ret = read_aws_region(region, REGION_LEN);
	if (ret == WM_SUCCESS) {
		snprintf(url, BUFLEN, "data.iot.%s.amazonaws.com",
			 region);
	} else {
		/* if not found in memory, take the default region */
		snprintf(url, BUFLEN, "data.iot.%s.amazonaws.com",
			 AWS_IOT_MY_REGION_NAME);
	}
	sp->pHost = url;
	/* read configured thing name from the persistent memory */
	thing_name = os_mem_alloc(MAX_SIZE_OF_THING_NAME);
	if (!thing_name) {
		ret = -WM_E_NOMEM;
		goto exit;
	}
	memset(thing_name, 0, MAX_SIZE_OF_THING_NAME);
	ret = read_aws_thing(thing_name, MAX_SIZE_OF_THING_NAME);
	if (ret != WM_SUCCESS) {
		wmprintf("Failed to read thing-name. Returning!\r\n");
		goto exit;
	}
	scp->pMyThingName = thing_name;

	/* client ID should be unique for every device */
	client_id = os_mem_alloc(MAX_SIZE_OF_UNIQUE_CLIENT_ID_BYTES);
	if (!client_id) {
		ret = -WM_E_NOMEM;
		goto exit;
	}
	/* read device MAC address */
	ret = read_aws_device_mac(device_mac);
	if (ret != WM_SUCCESS) {
		wmprintf("Failed to read device mac address. Returning!\r\n");
		goto exit;
	}
	/* Unique client ID in the format prefix-6 byte MAC address */
	snprintf(client_id, MAX_SIZE_OF_UNIQUE_CLIENT_ID_BYTES,
		 "%s-%02x%02x%02x%02x%02x%02x", AWS_IOT_MQTT_CLIENT_ID,
		 device_mac[0], device_mac[1], device_mac[2],
		 device_mac[3], device_mac[4], device_mac[5]);
	scp->pMqttClientId = client_id;
	scp->mqttClientIdLen = (uint16_t) strlen(client_id);

	/* read configured certificate from the persistent memory */
	client_cert_buffer = os_mem_alloc(AWS_PUB_CERT_SIZE);
	if (!client_cert_buffer) {
		ret = -WM_E_NOMEM;
		goto exit;
	}
	memset(client_cert_buffer, 0, AWS_PUB_CERT_SIZE);
	ret = read_aws_certificate(client_cert_buffer, AWS_PUB_CERT_SIZE);
	if (ret != WM_SUCCESS) {
		wmprintf("Failed to read certificate. Returning!\r\n");
		goto exit;
	}
	sp->pClientCRT = client_cert_buffer;

	/* read configured private key from the persistent memory */
	private_key_buffer = os_mem_alloc(AWS_PRIV_KEY_SIZE);
	if (!private_key_buffer) {
		ret = -WM_E_NOMEM;
		goto exit;
	}
	memset(private_key_buffer, 0, AWS_PRIV_KEY_SIZE);
	ret = read_aws_key(private_key_buffer, AWS_PRIV_KEY_SIZE);
	if (ret != WM_SUCCESS) {
		wmprintf("Failed to read key. Returning!\r\n");
		goto exit;
	}
	sp->pClientKey = private_key_buffer;

	sp->port = AWS_IOT_MQTT_PORT;
	sp->pRootCA = rootCA;
	sp->enableAutoReconnect = true;
	sp->disconnectHandler = NULL;

	return ret;
exit:
	appln_aws_shadow_config_free(mqtt_client, sp, scp);
	return ret;
}

/* Initialize MQTT client and AWS shadow parameters */
int appln_aws_shadow_config_init(AWS_IoT_Client *mqtt_client,
				 ShadowInitParameters_t *sp,
				 ShadowConnectParameters_t *scp)
{
	if (!mqtt_client || !sp)
		return -WM_FAIL;

	int ret;

	ret = aws_starter_load_configuration(mqtt_client, sp, scp);
	if (ret != WM_SUCCESS) {
		wmprintf("aws shadow configuration failed : %d\r\n", ret);
		return -WM_FAIL;
	}

	ret = aws_iot_shadow_init(mqtt_client, sp);
	if (ret != WM_SUCCESS) {
		wmprintf("aws shadow init failed : %d\r\n", ret);
		return -WM_FAIL;
	}

	return ret;
}

/* Free buffers allocated for aws pubCert and privKey */
void appln_aws_shadow_config_free(AWS_IoT_Client *mqtt_client,
				  ShadowInitParameters_t *sp,
				  ShadowConnectParameters_t *scp)
{
	if (sp->pHost) {
		os_mem_free(sp->pHost);
		sp->pHost = NULL;
	}
	if (sp->pClientCRT) {
		os_mem_free(sp->pClientCRT);
		sp->pClientCRT = NULL;
	}
	if (sp->pClientKey) {
		os_mem_free(sp->pClientKey);
		sp->pClientKey = NULL;
	}
	if (scp->pMyThingName) {
		os_mem_free(scp->pMyThingName);
		scp->pMyThingName = NULL;
	}
	if (scp->pMqttClientId) {
		os_mem_free(scp->pMqttClientId);
		scp->pMqttClientId = NULL;
	}
}
