# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += assembly_demo
assembly_demo-objs-y := \
		src/main.c

assembly_demo-objs-$(tc-arm_gcc-env-y) += src/add_them_gcc.s
assembly_demo-objs-$(tc-iar-env-y) += src/add_them_iar.s

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

assembly_demo-supported-toolchain-y := arm_gcc iar
