/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */

/*
 * Sample application for the Wi-Fi certification activity.
 *
 * Summary:
 *
 * This application can be used to carry out Wi-Fi Certification
 * activity.
 *
 * Description:
 *
 * The application is written using Application Framework that
 * simplifies development of WLAN networking applications.
 *
 * WLAN Initialization:
 *
 * When the application framework is started, it starts up the WLAN
 * sub-system and initializes the network stack. The app receives the event
 * when the WLAN subsystem has been started and initialized.
 *
 * Application registers Wi-Fi Certification related HTTP handlers to
 * carry out various tests mentioned in ASD #1029.
 *
 */

#include <wm_os.h>
#include <app_framework.h>
#include <wmstdio.h>
#include <wm_net.h>
#include <appln_cb.h>
#include <appln_dbg.h>
#include <cli.h>
#include <psm-utils.h>
#include <httpd.h>
#include <ftfs.h>
#include <rfget.h>
#include <critical_error.h>
#include <json_parser.h>
#include <json_generator.h>
#include <telnetd.h>

#define MAX_RETRY_TICKS 50

static struct json_str jstr;
char zero_buf[256];
char scratch[512];

/* This function is defined for handling critical error.
 * For this application, we just stall and do nothing when
 * a critical error occurs.
 */
void critical_error(int crit_errno, void *data)
{
	dbg("Critical Error %d: %s\r\n", crit_errno,
			critical_error_msg(crit_errno));
	while (1)
		;
	/* do nothing -- stall */
}

/*
 * A simple HTTP Web-Service Handler
 *
 * Returns the string "Hello World" when a GET on http://<IP>/hello
 * is done.
 */
static int sys_anchor_start(struct json_str *jptr, char *buf, int len)
{
	json_str_init(jptr, buf, len);
	json_start_object(jptr);
	return 0;
}

static int sys_anchor_end(struct json_str *jptr)
{
	json_close_object(jptr);
	return 0;
}

int http_temp_handler(httpd_request_t *req)
{
	char content[30];
	sys_anchor_start(&jstr, content, sizeof(content));
	json_set_val_int(&jstr, "temp", 26);
	sys_anchor_end(&jstr);
	return httpd_send_response(req, HTTP_RES_200, content,
			strlen(content), HTTP_CONTENT_JSON_STR);

	return 0;
}

int http_data_handler(httpd_request_t *req)
{
	int err;

	/* Invoke the server provided handlers to process HTTP headers */
	err = httpd_parse_hdr_tags(req, req->sock, scratch, HTTPD_MAX_MESSAGE);
	if (err != WM_SUCCESS) {
		dbg("Unable to parse header tags");
	}

	/* Send 200 OK and plain/text */
	httpd_send_hdr_from_code(req->sock, 0, HTTP_CONTENT_PLAIN_TEXT);

	/* Keep sending the chunk in a loop until the other end closes
	 * connection. */
	while (1) {
		if (httpd_send_chunk(req->sock, zero_buf,
					sizeof(zero_buf)) != WM_SUCCESS)
			break;
	}
	return HTTPD_DONE;
}

struct httpd_wsgi_call g_http_handlers[] = {
	{"/demo/temp", HTTPD_DEFAULT_HDR_FLAGS, 0,
		http_temp_handler, NULL, NULL, NULL},
	{"/data", HTTPD_DEFAULT_HDR_FLAGS, 0,
		http_data_handler, NULL, NULL, NULL},
};

static int g_handlers_no =
	sizeof(g_http_handlers) / sizeof(struct httpd_wsgi_call);

/*
 * Register Web-Service handlers
 *
 */
int register_httpd_handlers()
{
	int rc;

	rc = httpd_register_wsgi_handlers(g_http_handlers,
				g_handlers_no);
	if (rc)
		dbg("cert_demo: failed to register web handlers");

	return rc;
}

/* Get AMPDU STA tx/rx enable/disable configuration from psm */

#define MAX_PSM_VAL		65
#define VAR_NET_STA_AMPDU_TX	"network.sta_ampdu_tx"
#define VAR_NET_STA_AMPDU_RX	"network.sta_ampdu_rx"

static void wlan_get_ampdu_config(int *sta_ampdu_tx, int *sta_ampdu_rx)
{
	int var_size;
	char psm_val[MAX_PSM_VAL];

	var_size = psm_get_variable_str(sys_psm_get_handle(),
			VAR_NET_STA_AMPDU_TX,
			psm_val, sizeof(psm_val));
	if (var_size > 0)
		*sta_ampdu_tx = atoi(psm_val);

	var_size = psm_get_variable_str(sys_psm_get_handle(),
			VAR_NET_STA_AMPDU_RX,
			psm_val, sizeof(psm_val));
	if (var_size > 0)
		*sta_ampdu_rx = atoi(psm_val);
}

/*
 * Handler invoked when WLAN subsystem is ready.
 *
 */
void event_wlan_init_done(void *data)
{
	int ret = -WM_FAIL;
	int provisioned;
	int sta_ampdu_tx = 0, sta_ampdu_rx = 0;

	/* We receive provisioning status in data */
	provisioned = (int)data;

	dbg("AF_EVT_WLAN_INIT_DONE provisioned=%d", provisioned);

	wlan_get_ampdu_config(&sta_ampdu_tx, &sta_ampdu_rx);

	dbg("STA AMPDU => TX: %s, RX: %s",
		sta_ampdu_tx == 1 ? "Enabled" : "Disabled",
		sta_ampdu_rx == 1 ? "Enabled" : "Disabled");

	/* Start http server */
	ret = app_httpd_start();
	if (ret != WM_SUCCESS)
		dbg("Failed to start HTTPD");

	ret = register_httpd_handlers();
	if (ret != WM_SUCCESS)
		dbg("Failed to register HTTPD handlers");

	/* Start telnet daemon for console access without UART */
	telnetd_start(23);

	if (provisioned) {
		if (sta_ampdu_tx && sta_ampdu_rx)
			dbg("STA AMPDU TX and RX both are enabled,"
			    " please set as per test configuration");
		else {
			if (sta_ampdu_tx)
				wlan_sta_ampdu_tx_enable();
			else
				wlan_sta_ampdu_tx_disable();

			if (sta_ampdu_rx)
				wlan_sta_ampdu_rx_enable();
			else
				wlan_sta_ampdu_rx_disable();
		}
		app_sta_start();
	} else {
		dbg("This device is not configured. Please set and reboot:");
		dbg("network.ssid=ssid");
		dbg("network.security=0/1/3/4/5");
		dbg("network.passphrase=passphrase");
		dbg("network.configured=1");
		dbg("For dynamic ip address assignment:");
		dbg("network.lan=DYNAMIC");
		dbg("For static ip address assignment:");
		dbg("network.lan=STATIC");
		dbg("network.ip_address=<ip_address>");
		dbg("network.gateway=<gateway>");
		dbg("network.subnet_mask=<subnet_mask>");
		dbg("For STA AMPDU enable/disable");
		dbg("network.sta_ampdu_tx=0/1");
		dbg("network.sta_ampdu_rx=0/1");
	}

	/*
	 * Initialize CLI Commands for some of the modules:
	 *
	 * -- psm:  allows user to check data in psm partitions
	 * -- wlan: allows user to explore basic wlan functions
	 * -- ttcp: allows user to test TCP/UDP TX/RX throughput
	 */

	ret = psm_cli_init(sys_psm_get_handle(), NULL);
	if (ret != WM_SUCCESS)
		dbg("Error: psm_cli_init failed");
	ret = wlan_cli_init();
	if (ret != WM_SUCCESS)
		dbg("Error: wlan_cli_init failed");
}

void event_normal_connecting(void *data)
{
	dbg("Connecting to Home Network");
}

/* Event: AF_EVT_NORMAL_CONNECTED
 *
 * Station interface connected to home AP.
 *
 * Network dependent services can be started here. Note that these
 * services need to be stopped on disconnection and
 * reset-to-provisioning event.
 */
void event_normal_connected(void *data)
{
	char ip[16];

	app_network_ip_get(ip);
	dbg("Connected to provisioned network with ip address = %s", ip);
}

/* Event handler for AF_EVT_NORMAL_DISCONNECTED - Station interface
 * disconnected.
 * Stop the network services which need not be running in disconnected mode.
 */
void event_normal_user_disconnect(void *data)
{
	dbg("Disconnected from Home Network");
}

void event_normal_link_lost(void *data)
{
	dbg("Link lost from Home Network");
}

/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_NORMAL_CONNECTING:
		event_normal_connecting(data);
		break;
	case AF_EVT_NORMAL_CONNECTED:
		event_normal_connected(data);
		break;
	case AF_EVT_NORMAL_LINK_LOST:
		event_normal_link_lost(data);
		break;
	case AF_EVT_NORMAL_USER_DISCONNECT:
		event_normal_user_disconnect(data);
		break;
	default:
		break;
	}

	return 0;
}

static void modules_init()
{
	int ret;
	/*
	 * Initialize wmstdio prints
	 */
	ret = wmstdio_init(UART0_ID, 0);

	if (ret != WM_SUCCESS) {
		dbg("Error: wmstdio_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	/*
	 * Initialize CLI Commands
	 */
	ret = cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: cli_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}
	/* Initialize time subsystem.
	 *
	 * Initializes time to 1/1/1970 epoch 0.
	 */
	ret = wmtime_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: wmtime_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	/*
	 * Register Power Management CLI Commands
	 */
	ret = pm_cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: pm_cli_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	return;
}

int main()
{
	modules_init();

	dbg("Build Time: " __DATE__ " " __TIME__ "");

	wifi_set_packet_retry_count(MAX_RETRY_TICKS);

	/* Start the application framework */
	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		dbg("Failed to start application framework");
		critical_error(-CRIT_ERR_APP, NULL);
	} else {
		dbg("app_framework started");
	}
	return 0;
}
