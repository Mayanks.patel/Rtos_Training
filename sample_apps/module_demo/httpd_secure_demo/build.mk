# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.


ifneq ($(CONFIG_ENABLE_HTTPS_SERVER), y)
$(error "Please enable CONFIG_ENABLE_HTTPS_SERVER option using menuconfig")
endif

exec-y += httpd_secure_demo
httpd_secure_demo-objs-y   := \
		src/main.c \
		src/text_webhandler.c

httpd_secure_demo-cflags-y := -I$(d)/src

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
