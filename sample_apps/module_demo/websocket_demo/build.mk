# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += websocket_demo
websocket_demo-objs-y := \
		src/main.c \
		src/websocket.c


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

websocket_demo-supported-toolchain-y := arm_gcc iar
