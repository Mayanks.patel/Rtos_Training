# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

include $(d)/config.mk

exec-y += fw_upgrade_demo
fw_upgrade_demo-objs-y := src/main.c
fw_upgrade_demo-cflags-y                       := -I$(d)/src -DAPPCONFIG_DEBUG_ENABLE=1
fw_upgrade_demo-cflags-$(MID_LEVEL_FW_UPGRADE) += -DAPPCONFIG_MID_LEVEL_FW_UPGRADE
fw_upgrade_demo-cflags-$(HIGH_LEVEL_FW_UPGRADE) += -DAPPCONFIG_HIGH_LEVEL_FW_UPGRADE


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

fw_upgrade_demo-supported-toolchain-y := arm_gcc iar
