# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += low_level_fw_upgrade_demo
low_level_fw_upgrade_demo-objs-y := \
			 src/main.c
low_level_fw_upgrade_demo-cflags-y := \
			-I$(d)/src \
			-DAPPCONFIG_DEBUG_ENABLE=1


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

low_level_fw_upgrade_demo-supported-toolchain-y := arm_gcc iar
