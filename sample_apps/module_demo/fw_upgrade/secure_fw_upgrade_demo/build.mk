# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

include $(d)/config.mk

exec-y += secure_fw_upgrade_demo
secure_fw_upgrade_demo-objs-y := src/main.c
secure_fw_upgrade_demo-cflags-y                       := -I$(d)/src -DAPPCONFIG_DEBUG_ENABLE=1
secure_fw_upgrade_demo-cflags-$(FWUPG_DEMO_ED_CHACHA) += -DAPPCONFIG_FWUPG_ED_CHACHA
secure_fw_upgrade_demo-cflags-$(FWUPG_DEMO_RSA_AES)   += -DAPPCONFIG_FWUPG_RSA_AES


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

secure_fw_upgrade_demo-supported-toolchain-y := arm_gcc iar
