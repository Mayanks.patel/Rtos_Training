# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += webhandler_demo
webhandler_demo-objs-y := \
		src/main.c \
		src/json_webhandler.c \
		src/text_webhandler.c

webhandler_demo-cflags-y := -I$(d)/src


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

webhandler_demo-supported-toolchain-y := arm_gcc iar
