# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

ifneq ($(CONFIG_ENABLE_HTTPC_SECURE),y)
$(error "Please enable CONFIG_ENABLE_HTTPC_SECURE using menuconfig")
endif

exec-y += httpc_demo
httpc_demo-objs-y := \
		src/main.c \
		src/httpc_post.c \
		src/httpc_post_callback.c  \
		src/httpc_secure_post.c \
		src/wikipedia-server.cert.pem.c \
		src/httpc_get.c \
		src/set_device_time.c \
		src/httpc_secure_get.c

httpc_demo-cflags-y := -I$(d)/src



#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
