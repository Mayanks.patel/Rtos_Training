/*
 *  Copyright (C) 2008-2016 Marvell International Ltd.
 *  All Rights Reserved.
 */
/*
 * Internal Temperature measurement Application
 *
 * Summary:
 * This application measures the internal temperature of MW300.
 *
 * A serial terminal program like HyperTerminal, putty, or
 * minicom can be used to see the program output.
 */
#include <wmstdio.h>
#include <wm_os.h>
#include <mdev_adc.h>
#include <mdev_pinmux.h>

#define SAMPLES	10
#define TS_OFFSET 462
#define TS_GAIN 1.66

/*
 * The application entry point is main(). At this point, minimal
 * initialization of the hardware is done, and also the RTOS is
 * initialized.
 */
int main(void)
{
	int count = 0;
	float result;
	uint16_t sample;
	mdev_t *adc_dev;

	/* Initialize console on uart0 */
	wmstdio_init(UART0_ID, 0);

	wmprintf("Internal temperature measurement application started\r\n");
	/* adc initialization */
	if (adc_drv_init(ADC0_ID) != WM_SUCCESS) {
		wmprintf("[Error] Cannot init ADC\n\r");
		return -WM_FAIL;
	}

	adc_dev = adc_drv_open(ADC0_ID, ADC_TEMPP);
	if (!adc_dev)
		return -WM_FAIL;
	adc_drv_tsensor_config(ADC0_ID, adc_dev, ADC_SENSOR_INTERNAL,
			ADC_INPUT_SINGLE_ENDED);

	if (adc_drv_selfcalib(adc_dev, vref_internal) == WM_SUCCESS)
		wmprintf("Calibration successful!\r\n");
	else
		wmprintf("Calibration failed!\r\n");

	while (1) {
		count++;
		sample = adc_drv_result(adc_dev);
		result =
			((float)sample - (float)TS_OFFSET) /
			((float)TS_GAIN);
		wmprintf("Iteration %d: count %d - %d.%d C\r\n",
					count, sample,
					wm_int_part_of(result),
					wm_frac_part_of(result, 2));
		os_thread_sleep(os_msec_to_ticks(5000));
	}

	/* It will never reach here. Done just to show how to cleanly close the
	 * driver */
	adc_drv_close(adc_dev);
	adc_drv_deinit(ADC0_ID);

	return 0;
}
