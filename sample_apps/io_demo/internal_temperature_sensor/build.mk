# Copyright (C) 2008-2016 Marvell International Ltd.
# All Rights Reserved.

exec-y += internal_temperature_sensor
internal_temperature_sensor-objs-y := src/main.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
