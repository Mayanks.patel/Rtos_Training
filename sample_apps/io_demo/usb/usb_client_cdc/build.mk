# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.


ifneq ($(CONFIG_USB_DRIVER), y)
$(error " Please enable CONFIG_USB_DRIVER option using menuconfig")
endif

exec-y += usb_client_cdc

usb_client_cdc-objs-y += src/main.c

usb_client_cdc-cflags-y := -I$(d)/src

usb_client_cdc-linkerscript-$(CONFIG_CPU_MC200) := $(d)/../mc200_usb.ld

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
