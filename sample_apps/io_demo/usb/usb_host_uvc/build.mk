# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += usb_host_uvc

usb_host_uvc-objs-y := src/main.c

usb_host_uvc-cflags-y := -I$(d)/src

usb_host_uvc-linkerscript-$(CONFIG_CPU_MC200) := $(d)/../mc200_usb.ld

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
