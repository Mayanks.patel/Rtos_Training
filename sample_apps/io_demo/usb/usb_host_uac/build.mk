# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += usb_host_uac
usb_host_uac-objs-y := src/main.c
usb_host_uac-linkerscript-$(CONFIG_CPU_MC200) := $(d)/../mc200_usb.ld
usb_host_uac-cflags-y := -I$(d)/src/

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
