# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

prj_name := uart_dma_rx_demo

exec-y += $(prj_name)
$(prj_name)-objs-y := src/main.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

$(prj_name)-supported-toolchain-y := arm_gcc iar
