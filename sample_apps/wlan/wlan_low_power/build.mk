# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += wlan_low_power

wlan_low_power-objs-y   := src/main.c
wlan_low_power-cflags-y := -I$(d)/src


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

wlan_low_power-supported-toolchain-y := arm_gcc iar
