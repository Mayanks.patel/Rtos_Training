# Copyright (C) 2008-2016 Marvell International Ltd.
# All Rights Reserved.
#

WM_DEMO_CONFIG_MDNS_ENABLE=y

exec-y += wm_demo

wm_demo-objs-y := \
		src/main.c \
		src/reset_prov_helper.c \
		src/led_indications.c \
		src/utility.c

wm_demo-cflags-y := \
		-I$(d)/src \
		-DAPPCONFIG_DEBUG_ENABLE=1

wm_demo-objs-$(WM_DEMO_CONFIG_MDNS_ENABLE) += \
		 src/mdns_helper.c

wm_demo-cflags-$(WM_DEMO_CONFIG_MDNS_ENABLE) += \
		-DAPPCONFIG_MDNS_ENABLE

wm_demo-ftfs-y 		:= wm_demo.ftfs
wm_demo-ftfs-dir-y     	:= $(d)/www
wm_demo-ftfs-api-y 	:= 100


wm_demo-supported-toolchain-y := arm_gcc iar

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
