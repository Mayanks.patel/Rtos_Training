# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += wpa2_enterprise
wpa2_enterprise-objs-y := \
	src/wpa2_network.c \
	src/main.c  \
	src/power_mgr_helper.c \
	src/mdns_helper.c

wpa2_enterprise-cflags-y := -DAPPCONFIG_MDNS_ENABLE -I$(d)/src

# Enable for debug logs
#wpa2_enterprise-cflags-y += -DAPPCONFIG_DEBUG_ENABLE



#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
