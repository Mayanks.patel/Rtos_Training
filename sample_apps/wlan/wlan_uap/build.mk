# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += wlan_uap

wlan_uap-objs-y := src/main.c
wlan_uap-cflags-y := -I$(d)/src

WLAN_UAP_MDNS_ENABLE=y
wlan_uap-objs-$(WLAN_UAP_MDNS_ENABLE)   += src/mdns_helper.c
wlan_uap-cflags-$(WLAN_UAP_MDNS_ENABLE) += -DAPPCONFIG_MDNS_ENABLE

# Enable for debugging
# wlan_uap-cflags-y += -DAPPCONFIG_DEBUG_ENABLE



#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

wlan_uap-supported-toolchain-y := arm_gcc iar
