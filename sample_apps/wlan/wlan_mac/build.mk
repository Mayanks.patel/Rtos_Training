# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += wlan_mac
wlan_mac-objs-y   := src/main.c
wlan_mac-cflags-y := -I$(d)/src

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

wlan_mac-supported-toolchain-y := arm_gcc iar
