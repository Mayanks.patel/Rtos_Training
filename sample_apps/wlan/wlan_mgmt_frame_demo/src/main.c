/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */

/*
 * Simple Wireless Management frame demo application
 *
 * Summary:
 *
 * This application calls an API to receive intended management frame from
 * Wi-Fi firmware to host. The callback given in API receives the frame
 * speficied in bitmap.
 *
 * Description:
 *
 * The application is written using Application Framework that
 * simplifies development of WLAN networking applications.
 *
 * WLAN Initialization:
 *
 * When the application framework is started, it starts up the WLAN
 * sub-system and initializes the network stack. The app receives the event
 * when the WLAN subsystem has been started and initialized.
 *
 * The application calls the RX management frame indication API and specifies
 * the interface, bitmap and receive callback.
 *
 * The receive callback will receives the specified management frames on the
 * given interface as per bitmap.
 *
 */

#include <wm_os.h>
#include <app_framework.h>
#include <wmtime.h>
#include <partition.h>
#include <appln_cb.h>
#include <appln_dbg.h>
#include <cli.h>
#include <cli_utils.h>
#include <wlan.h>
#include <wlan_smc.h>
#include <psm.h>
#include <wmstdio.h>
#include <wmsysinfo.h>
#include <wm_net.h>
#include <httpd.h>
#include <wlan_11d.h>
#include <critical_error.h>

/*-----------------------Global declarations----------------------*/
#define BUFFER_LENGTH       (1024)
#define MAX_EVENTS	50

typedef struct PACK_START mgmt_frame_msg {
	enum wlan_bss_type bss_type;
	wlan_mgmt_frame_t *frame;
	size_t len;
} PACK_END mgmt_frame_msg_t;

/* message queue and thread for state machine */
static os_queue_pool_define(mgmt_frame_queue_data,
			    sizeof(mgmt_frame_msg_t *) * MAX_EVENTS);
static os_queue_t mgmt_frame_queue;
static os_thread_stack_define(mgmt_frame_stack, 2048);
static os_thread_t mgmt_frame_thread;
static mgmt_frame_msg_t *q_msg;
unsigned char mac[6] = { 0 };
bool probe_resp_sent;
int uap_ie_index;

uint8_t test_ie[] = {0xAA, 0xBB, 0xBB, 0xDD, 0xEE, 0xFF,
		0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF};

appln_config_t appln_cfg = {
	.ssid = "MGMT_FRAME_DEMO",
	.passphrase = "marvellwm",
	.hostname = "MGMT_FRAME_DEMO"
};

/* This function must initialize the variables required (network name,
 * passphrase, etc.) It should also register all the event handlers that are of
 * interest to the application.
 */
int appln_config_init()
{
	/* Initialize service name for mdns */
	snprintf(appln_cfg.servname, MAX_SRVNAME_LEN, "MGMT_FRAME_DEMO");
	return 0;
}

/* This function is defined for handling critical error.
 * For this application, we just stall and do nothing when
 * a critical error occurs.
 */
void critical_error(int crit_errno, void *data)
{
	dbg("Critical Error %d: %s\r\n", crit_errno,
			critical_error_msg(crit_errno));
	while (1)
		;
	/* do nothing -- stall */
}

static void send_frame(const enum wlan_bss_type bss_type,
			const wifi_frame_type_t frame_type,
			const t_u8 frame_ctrl_flags,
			const t_u8 *dest, const t_u8 *src,
			const t_u8 *bssid, const t_u8 *payload,
			const size_t payload_len)
{
	size_t data_len = sizeof(wifi_mgmt_frame_t);
	uint8_t *buffer = NULL;
	int ret;

	wifi_mgmt_frame_t *pmgmt_frame = NULL;

	buffer = (uint8_t *)os_mem_alloc(BUFFER_LENGTH);
	if (!buffer) {
		wmprintf("ERR:Cannot allocate memory!\r\n");
		return;
	}

	memset(buffer, 0, BUFFER_LENGTH);

	wlan_get_mac_address(mac);

	pmgmt_frame = (wifi_mgmt_frame_t *)(buffer);

	pmgmt_frame->frame_type = frame_type;

	memcpy(pmgmt_frame->addr1, dest, MLAN_MAC_ADDR_LENGTH);
	memcpy(pmgmt_frame->addr2, src, MLAN_MAC_ADDR_LENGTH);
	memcpy(pmgmt_frame->addr3, bssid, MLAN_MAC_ADDR_LENGTH);

	memcpy(pmgmt_frame->payload, payload, payload_len);
	data_len += payload_len;

	pmgmt_frame->frame_ctrl_flags = frame_ctrl_flags;

	pmgmt_frame->frm_len = data_len - sizeof(pmgmt_frame->frm_len);

	ret = wlan_inject_frame(bss_type, buffer, data_len);

	if (ret != WM_SUCCESS)
		dbg("Error: Failed to inject frame");

	if (buffer)
		os_mem_free(buffer);
}

static void process_frame(const enum wlan_bss_type bss_type,
			const wlan_mgmt_frame_t *frame, const size_t len)
{
	uint16_t payload_len = 0;

	if (frame->frame_type == PROBE_REQ_FRAME) {
		wmprintf("%s: Received probe req from ",
			bss_type == 1 ? "uAP" : "sta");
		print_mac((const char *)frame->addr2);
		wmprintf("with payload \r\n");
		dump_hex((uint8_t *) frame + sizeof(wlan_mgmt_frame_t),
				len - sizeof(wlan_mgmt_frame_t));
		wmprintf("\r\n");
		if (!probe_resp_sent) {
			wmprintf("Send probe response\r\n");
			payload_len = len - sizeof(wlan_mgmt_frame_t);
			send_frame(bss_type, PROBE_RESP_FRAME,
				frame->frame_ctrl_flags,
				frame->addr2, mac, mac,
				frame->payload, payload_len);
			probe_resp_sent = true;
		}
	} else if (frame->frame_type == ASSOC_REQ_FRAME) {
		wmprintf("%s: Received association req from ",
			bss_type == 1 ? "uAP" : "sta");
		print_mac((const char *)frame->addr2);
		wmprintf("\r\n");
	} else if (frame->frame_type == PROBE_RESP_FRAME) {
		wmprintf("%s: Received probe resp from ",
			bss_type == 1 ? "uAP" : "sta");
		print_mac((const char *)frame->addr2);
		wmprintf("\r\n");
	} else if (frame->frame_type == ASSOC_RESP_FRAME) {
		wmprintf("%s: Received association resp from ",
			bss_type == 1 ? "uAP" : "sta");
		print_mac((const char *)frame->addr2);
		wmprintf("\r\n");
	}
}

/**
 * This RX Management frame callback sends the received
 * frame to the processing thread.
 */
void rx_mgmt_cb(const enum wlan_bss_type bss_type,
		const wlan_mgmt_frame_t *frame, const size_t len)
{
	mgmt_frame_msg_t *msg = NULL;

	if (frame) {

		msg = (mgmt_frame_msg_t *)
			os_mem_calloc(sizeof(mgmt_frame_msg_t));

		if (msg == NULL) {
			dbg("Error: Failed to allocated memory for msg");
			return;
		}

		msg->frame = (wlan_mgmt_frame_t *)
			os_mem_calloc(len);

		if (msg->frame == NULL) {
			dbg("Error: Failed to allocated memory for frame");
			os_mem_free(msg);
			return;
		}

		msg->bss_type = bss_type;
		memcpy(msg->frame, frame, len);
		msg->len = len;

		if (os_queue_send(&mgmt_frame_queue, &msg, OS_NO_WAIT)
				!= WM_SUCCESS) {
			dbg("Error: Failed to send frame for processing");
			os_mem_free(msg->frame);
			os_mem_free(msg);
		}
	}
}

static void mgmt_frame_state_machine(os_thread_arg_t unused)
{
	int ret;

	dbg("mgmt frame processing state machine");

	while (1) {

		ret = os_queue_recv(&mgmt_frame_queue, &q_msg, OS_WAIT_FOREVER);

		if (ret != WM_SUCCESS)
			continue;

		process_frame(q_msg->bss_type, q_msg->frame, q_msg->len);

		if (q_msg->frame)
			os_mem_free(q_msg->frame);
		if (q_msg)
			os_mem_free(q_msg);
	}

	os_thread_self_complete(&mgmt_frame_thread);
}

int mgmt_frame_process_start()
{
	int ret;

	dbg("mgmt frame process start");

	ret = os_queue_create(&mgmt_frame_queue, "mgmt_frame_queue",
				sizeof(mgmt_frame_msg_t *),
				&mgmt_frame_queue_data);
	if (ret) {
		dbg("Error: failed to create queue for mgmt frame processor"
			" thread: %d", ret);
		goto fail;
	}

	ret = os_thread_create(&mgmt_frame_thread,
			       "mgmt_frame_processor",
			       mgmt_frame_state_machine,
			       (void *) &mgmt_frame_thread,
			       &mgmt_frame_stack,
			       OS_PRIO_3);
	if (ret) {
		dbg("Error: failed to launch mgmt frame processor"
			" thread: %d", ret);
		goto fail;
	}

	return WM_SUCCESS;

fail:
	if (mgmt_frame_thread)
		os_thread_delete(&mgmt_frame_thread);
	if (mgmt_frame_queue)
		os_queue_delete(&mgmt_frame_queue);

	return -WM_FAIL;
}

void mgmt_frame_process_stop(void)
{
	int ret;

	dbg("mgmt frame process stop");

	if (mgmt_frame_thread) {
		ret = os_thread_delete(&mgmt_frame_thread);
		if (ret != WM_SUCCESS)
			dbg("failed to delete thread: %d", ret);
	}

	if (mgmt_frame_queue) {
		ret = os_queue_delete(&mgmt_frame_queue);
		if (ret != WM_SUCCESS)
			dbg("failed to delete queue: %d", ret);
	}
}

/*
 * Handler invoked when the Micro-AP Network interface
 * is ready.
 *
 */

void event_uap_started(void *data)
{
	dbg("Event: Micro-AP Started");
}

void event_uap_stopped(void *data)
{
	dbg("uap interface stopped");
}

/*
 * Handler invoked when WLAN subsystem is ready.
 *
 */
void event_wlan_init_done(void *data)
{
	int ret;
	/*
	 * Initialize CLI Commands for some of the modules:
	 *
	 * -- wlan: allows user to explore basic wlan functions
	 */

	ret = wlan_cli_init();
	if (ret != WM_SUCCESS)
		dbg("Error: wlan_cli_init failed");

	ret = wlan_iw_cli_init();
	if (ret != WM_SUCCESS)
		dbg("Error: wlan_iw_init failed");

	wlan_get_mac_address(mac);

	/* To start processing of Management frame in application context. */
	mgmt_frame_process_start();

	/* API call to remain on a specific channel for provided
	 * timeout in milliseconds.
	 *
	 * Please refer API documentation for more details.
	 */
	/* wlan_remain_on_channel(WLAN_BSS_TYPE_STA, true, 1, 50000); */

	/* API call to cancel the remain on a specific channel and
	 * switch back to previous channel.
	 */
	/* wlan_remain_on_channel(WLAN_BSS_TYPE_STA, false, 1, 1000); */

	/* API call to set IE in probe response of uAP, this IE will get added
	 * automatically in outgoing probe responses until not cleared
	 * explicitly using below metioned API.
	 *
	 * Please note that the probe response sent from host will have to add
	 * the IE explicitly, because the wlan_set_mgmt_ie API do not have
	 * control over the probe responses sent from host using
	 * wlan_inject_frame API.
	 *
	 * The same API can also be used to add IE in station related
	 * frames like probe request, association request.
	 *
	 */
	uap_ie_index = wlan_set_mgmt_ie(WLAN_BSS_TYPE_UAP,
					MGMT_VENDOR_SPECIFIC_221,
					test_ie, sizeof(test_ie));

	/* API call to clear the IE in probe response */
	/* wlan_clear_mgmt_ie(WLAN_BSS_TYPE_UAP, uap_ie_index); */

	/* API call to start Management frames probe request and association
	 * request reception on uap interface, the received frames will
	 * be provided in the callback passed to this API.
	 */
	wlan_rx_mgmt_indication(WLAN_BSS_TYPE_UAP, WLAN_MGMT_PROBE_REQ |
				WLAN_MGMT_ASSOC_REQ, rx_mgmt_cb);

	/* API call to start Management frames probe response and association
	 * response reception on station interface, the received frames will
	 * be provided in the callback passed to this API.
	 */
	/* wlan_rx_mgmt_indication(WLAN_BSS_TYPE_STA, WLAN_MGMT_PROBE_RESP |
	 *			WLAN_MGMT_ASSOC_RESP, rx_mgmt_cb); */


	/* API call to set beacon period of uAP */
	/* wlan_uap_set_beacon_period(400); */

	/* API call to set the uAP ssid hidden or visible */
	/* wlan_uap_set_hidden_ssid(true); */

	/* API call to start uAP with provided SSID and passphrase
	 * along with DHCP server
	 */
	/* app_uap_start_with_dhcp(appln_cfg.ssid, appln_cfg.passphrase); */

	/* API call to start uAP with provided SSID, pasphrase
	 * and channel along with DHCP server
	 */
	app_uap_start_on_channel_with_dhcp(appln_cfg.ssid,
				appln_cfg.passphrase, 1);
}

/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_UAP_STARTED:
		event_uap_started(data);
		break;
	case AF_EVT_UAP_STOPPED:
		event_uap_stopped(data);
		break;
	default:
		break;
	}

	return 0;
}

static void modules_init()
{
	int ret;

	/*
	 * Initialize wmstdio prints
	 */
	ret = wmstdio_init(UART0_ID, 0);
	if (ret != WM_SUCCESS) {
		dbg("Error: wmstdio_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	/*
	 * Initialize CLI Commands
	 */
	ret = cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: cli_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}
	/* Initialize time subsystem.
	 *
	 * Initializes time to 1/1/1970 epoch 0.
	 */
	ret = wmtime_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: wmtime_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	/*
	 * Register Power Management CLI Commands
	 */
	ret = pm_cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: pm_cli_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	/*
	 * Register Time CLI Commands
	 */
	ret = wmtime_cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: wmtime_cli_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	return;
}

int main()
{
	modules_init();

	dbg("Build Time: " __DATE__ " " __TIME__ "");

	appln_config_init();

	/* Start the application framework */
	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		dbg("Failed to start application framework");
				critical_error(-CRIT_ERR_APP, NULL);
	}

	return 0;
}
