# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += wlan_power_save
wlan_power_save-objs-y   := src/main.c
wlan_power_save-cflags-y := -I$(d)/src


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

wlan_power_save-supported-toolchain-y := arm_gcc iar
