# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += wlan_uap_power_save

wlan_uap_power_save-objs-y := \
		src/main.c \
		src/power_mgr_helper.c

wlan_uap_power_save-cflags-y := -I$(d)/src

WLAN_UAP_MDNS_ENABLE=y

wlan_uap_power_save-objs-$(WLAN_UAP_MDNS_ENABLE) += \
		src/mdns_helper.c

wlan_uap_power_save-cflags-$(WLAN_UAP_MDNS_ENABLE) += \
		-DAPPCONFIG_MDNS_ENABLE

# Enable for debugging
# wlan_uap_power_save-cflags-y += -DAPPCONFIG_DEBUG_ENABLE



#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

wlan_uap_power_save-supported-toolchain-y := arm_gcc iar
