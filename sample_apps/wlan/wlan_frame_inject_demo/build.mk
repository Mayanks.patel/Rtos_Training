# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += wlan_frame_inject_demo
wlan_frame_inject_demo-objs-y   := src/main.c
wlan_frame_inject_demo-cflags-y := -DAPPCONFIG_DEBUG_ENABLE -I$(d)/src


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

wlan_frame_inject_demo-supported-toolchain-y := arm_gcc iar
