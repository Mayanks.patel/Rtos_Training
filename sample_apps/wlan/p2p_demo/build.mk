# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += p2p_demo
p2p_demo-objs-y := \
		src/main.c \
		src/power_mgr_helper.c

p2p_demo-cflags-y := \
		-I$(d)/src \
		-DAPPCONFIG_DEBUG_ENABLE=1

p2p_demo-ftfs-y  	:= p2p_demo.ftfs
p2p_demo-ftfs-dir-y     := $(d)/www
p2p_demo-ftfs-api-y 	:= 100


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
