# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += wlan_rx_mgmt_frame_indication_demo
wlan_rx_mgmt_frame_indication_demo-objs-y := src/main.c

wlan_rx_mgmt_frame_indication_demo-cflags-y := \
		-I$(d)/src \
		-DAPPCONFIG_DEBUG_ENABLE=1

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
