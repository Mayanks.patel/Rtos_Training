/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */

/*
 * Simple Wireless RX Management frame indication application
 *
 * Summary:
 *
 * This application calls an API to receive intended management frame from
 * Wi-Fi firmware to host. The callback given in API receives the frame
 * speficied in bitmap.
 *
 * Description:
 *
 * The application is written using Application Framework that
 * simplifies development of WLAN networking applications.
 *
 * WLAN Initialization:
 *
 * When the application framework is started, it starts up the WLAN
 * sub-system and initializes the network stack. The app receives the event
 * when the WLAN subsystem has been started and initialized.
 *
 * The application calls the RX management frame indication API and specifies
 * the interface, bitmap and receive callback.
 *
 * The receive callback will receives the specified management frames on the
 * given interface as per bitmap.
 *
 */

#include <wm_os.h>
#include <app_framework.h>
#include <wmtime.h>
#include <partition.h>
#include <appln_cb.h>
#include <appln_dbg.h>
#include <cli.h>
#include <cli_utils.h>
#include <wlan.h>
#include <wlan_smc.h>
#include <psm.h>
#include <wmstdio.h>
#include <wmsysinfo.h>
#include <wm_net.h>
#include <httpd.h>
#include <wlan_11d.h>
#include <critical_error.h>

/*-----------------------Global declarations----------------------*/

appln_config_t appln_cfg = {
	.ssid = "RX_MGMT_IND_demo",
	.passphrase = "marvellwm",
	.hostname = "RX_MGMT_IND_demo"
};

/* This function must initialize the variables required (network name,
 * passphrase, etc.) It should also register all the event handlers that are of
 * interest to the application.
 */
int appln_config_init()
{
	/* Initialize service name for mdns */
	snprintf(appln_cfg.servname, MAX_SRVNAME_LEN, "RX_MGMT_IND_demo");
	return 0;
}

/* This function is defined for handling critical error.
 * For this application, we just stall and do nothing when
 * a critical error occurs.
 */
void critical_error(int crit_errno, void *data)
{
	dbg("Critical Error %d: %s\r\n", crit_errno,
			critical_error_msg(crit_errno));
	while (1)
		;
	/* do nothing -- stall */
}

static void process_frame(const enum wlan_bss_type bss_type,
			const wlan_mgmt_frame_t *frame, const uint16_t len)
{
	if (frame->frame_type == PROBE_REQ_FRAME) {
		wmprintf("%s: Received probe req from ",
			bss_type == 1 ? "uAP" : "sta");
		print_mac((const char *)frame->addr2);
		wmprintf("with payload \r\n");
		dump_hex((uint8_t *) frame + sizeof(wlan_mgmt_frame_t),
			len - sizeof(wlan_mgmt_frame_t));
		wmprintf("\r\n");
	} else if (frame->frame_type == ASSOC_REQ_FRAME) {
		wmprintf("%s: Received association req from ",
			bss_type == 1 ? "uAP" : "sta");
		print_mac((const char *)frame->addr2);
		wmprintf("\r\n");
	} else if (frame->frame_type == PROBE_RESP_FRAME) {
		wmprintf("%s: Received probe resp from ",
			bss_type == 1 ? "uAP" : "sta");
		print_mac((const char *)frame->addr2);
		wmprintf("\r\n");
	} else if (frame->frame_type == ASSOC_RESP_FRAME) {
		wmprintf("%s: Received association resp from ",
			bss_type == 1 ? "uAP" : "sta");
		print_mac((const char *)frame->addr2);
		wmprintf("\r\n");
	}
}

/** This RX Management frame callback is called from a thread with small
 * stack size, So do minimal memory allocations for correct behaviour.
 */
void rx_mgmt_cb(const enum wlan_bss_type bss_type,
		const wlan_mgmt_frame_t *frame, const uint16_t len)
{
	if (frame)
		process_frame(bss_type, frame, len);
}

/*
 * Handler invoked when the Micro-AP Network interface
 * is ready.
 *
 */

void event_uap_started(void *data)
{
	dbg("Event: Micro-AP Started");
}

void event_uap_stopped(void *data)
{
	dbg("uap interface stopped");
}

/*
 * Handler invoked when WLAN subsystem is ready.
 *
 */
void event_wlan_init_done(void *data)
{
	int ret;
	/*
	 * Initialize CLI Commands for some of the modules:
	 *
	 * -- wlan: allows user to explore basic wlan functions
	 */

	ret = wlan_cli_init();
	if (ret != WM_SUCCESS)
		dbg("Error: wlan_cli_init failed");

	ret = wlan_iw_cli_init();
	if (ret != WM_SUCCESS)
		dbg("Error: wlan_iw_init failed");

	wlan_rx_mgmt_indication(WLAN_BSS_TYPE_UAP, WLAN_MGMT_PROBE_REQ |
			WLAN_MGMT_ASSOC_REQ, rx_mgmt_cb);

	wlan_rx_mgmt_indication(WLAN_BSS_TYPE_STA, WLAN_MGMT_PROBE_RESP |
			WLAN_MGMT_ASSOC_RESP, rx_mgmt_cb);

	app_uap_start_with_dhcp(appln_cfg.ssid, appln_cfg.passphrase);
}

/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_UAP_STARTED:
		event_uap_started(data);
		break;
	case AF_EVT_UAP_STOPPED:
		event_uap_stopped(data);
		break;
	default:
		break;
	}

	return 0;
}

static void modules_init()
{
	int ret;

	/*
	 * Initialize wmstdio prints
	 */
	ret = wmstdio_init(UART0_ID, 0);
	if (ret != WM_SUCCESS) {
		dbg("Error: wmstdio_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	/*
	 * Initialize CLI Commands
	 */
	ret = cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: cli_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}
	/* Initialize time subsystem.
	 *
	 * Initializes time to 1/1/1970 epoch 0.
	 */
	ret = wmtime_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: wmtime_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	/*
	 * Register Power Management CLI Commands
	 */
	ret = pm_cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: pm_cli_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	/*
	 * Register Time CLI Commands
	 */
	ret = wmtime_cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: wmtime_cli_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	return;
}

int main()
{
	modules_init();

	dbg("Build Time: " __DATE__ " " __TIME__ "");

	appln_config_init();

	/* Start the application framework */
	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		dbg("Failed to start application framework");
				critical_error(-CRIT_ERR_APP, NULL);
	}

	return 0;
}
