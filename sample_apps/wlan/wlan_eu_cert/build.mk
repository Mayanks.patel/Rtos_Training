# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += wlan_eu_cert
wlan_eu_cert-objs-y   := src/main.c
wlan_eu_cert-cflags-y := -I$(d)/src


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

wlan_eu_cert-supported-toolchain-y := arm_gcc iar
