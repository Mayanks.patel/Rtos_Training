# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += wlan_cal
wlan_cal-objs-y   := src/main.c
wlan_cal-cflags-y := -I$(d)/src


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

wlan_cal-supported-toolchain-y := arm_gcc iar
