# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

subdir-app-y := hello_world
ifneq ($(CONFIG_ENABLE_CPP_SUPPORT),)
ifneq ($(CONFIG_ENABLE_MCU_PM3),y)
subdir-app-$(CONFIG_CPU_MW300)       += hello_world_cpp
endif
endif
subdir-app-y      += baremetal
subdir-app-y      += wifi_cert_demo
subdir-app-y      += network_performance/ttcp

ifneq ($(CONFIG_ENABLE_CPP_SUPPORT),)
ifneq ($(CONFIG_ENABLE_MCU_PM3),y)
subdir-app-$(CONFIG_CPU_MW300)        += network_performance/iperf-2.0.5
endif
endif

subdir-app-y      += assembly_demo

subdir-app-$(aws-agent-y)            += cloud_demo/aws_demo
subdir-app-$(evt-agent-y)            += cloud_demo/evrythng_demo


subdir-app-$(CONFIG_CPU_MW300)   += audio/audio_player_demo


subdir-app-y      += io_demo/adc
subdir-app-y      += io_demo/gpio
subdir-app-y      += io_demo/dac_demo
subdir-app-y      += io_demo/i2c/advanced/master_demo
subdir-app-y      += io_demo/i2c/advanced/slave_demo
subdir-app-y      += io_demo/i2c/simple/master_demo
subdir-app-y      += io_demo/i2c/simple/slave_demo
subdir-app-y      += io_demo/ssp/simple/master_demo
subdir-app-y      += io_demo/ssp/simple/slave_demo
subdir-app-y      += io_demo/ssp/ssp_full_duplex_demo
subdir-app-y      += io_demo/uart/uart_dma_rx_demo
subdir-app-y      += io_demo/uart/uart_dma_tx_demo
subdir-app-y      += io_demo/uart/uart_echo_demo

subdir-app-$(CONFIG_CPU_MW300)	 += power_profiles/deepsleep_pm4
subdir-app-$(CONFIG_CPU_MW300)	 += power_profiles/pdn_pm4
subdir-app-$(CONFIG_CPU_MW300)   += test_apps/io_demo/uart/uart_test
subdir-app-$(CONFIG_CPU_MW300)   += test_apps/io_demo/i2c/i2c_test
subdir-app-$(CONFIG_CPU_MW300)   += test_apps/io_demo/ssp/ssp_test
subdir-app-$(CONFIG_USB_DRIVER)  += io_demo/usb/usb_client_cdc

subdir-app-y    += io_demo/usb/usb_host_cdc
subdir-app-y    += io_demo/usb/usb_host_uac
subdir-app-y    += io_demo/usb/usb_host_uvc
subdir-app-y    += io_demo/usb/usb_host_msc

subdir-app-y 	  += os_api_demo/mutex_demo
subdir-app-y      += os_api_demo/semaphore_demo
subdir-app-y      += os_api_demo/timer_demo
subdir-app-y      += os_api_demo/queue_demo


subdir-app-y      += module_demo/cli_demo
subdir-app-y      += module_demo/webhandler_demo
subdir-app-y      += module_demo/websocket_demo
subdir-app-y      += module_demo/fw_upgrade/secure_fw_upgrade_demo
subdir-app-y      += module_demo/fw_upgrade/low_level_fw_upgrade_demo
subdir-app-y      += module_demo/fw_upgrade/fw_upgrade_demo
subdir-app-$(CONFIG_ENABLE_HTTPC_SECURE) += module_demo/httpc_demo
subdir-app-$(CONFIG_ENABLE_HTTPS_SERVER) += module_demo/httpd_secure_demo

subdir-app-y      += mfg/uart_wifi_bridge

subdir-app-$(CONFIG_BT_SUPPORT)      += mfg/hci_bridge/hci_sdio_bridge
subdir-app-$(CONFIG_BT_SUPPORT)      += mfg/hci_bridge/hci_uart_bridge
subdir-app-y                         += mfg/fcc_app

subdir-app-y      += net_demo/ntpc_demo
subdir-app-y      += net_demo/server_demo
subdir-app-y      += net_demo/client_demo

subdir-app-y      += power_measure_demo/pm_mcu_wifi_demo
subdir-app-$(CONFIG_CPU_MC200)       += power_measure_demo/pm_mc200_demo

ifeq (n, $(CONFIG_ENABLE_MCU_PM3))
	subdir-app-$(CONFIG_CPU_MW300)     += power_measure_demo/pm_mw300_demo
endif

ifeq (y, $(CONFIG_ENABLE_MCU_PM3))
	subdir-app-$(CONFIG_CPU_MW300)     += power_profiles/deepsleep_pm3
endif

subdir-app-$(CONFIG_CMSIS_DSPLIB)    += dsp_demo/dsp_dotproduct_demo
subdir-app-$(CONFIG_CMSIS_DSPLIB)    += dsp_demo/dsp_fft_bin_demo
subdir-app-$(CONFIG_CMSIS_DSPLIB)    += dsp_demo/dsp_matrix_demo

subdir-app-$(CONFIG_ENABLE_HTTPC_SECURE) += test_apps/tls_demo
subdir-app-y      += test_apps/qa1
subdir-app-y      += test_apps/qa2
subdir-app-y      += test_apps/qa3
subdir-app-y      += test_apps/qa4
subdir-app-y      += test_apps/os/rw_lock_demo
subdir-app-y      += test_apps/os/recursive_mutex_demo
subdir-app-y      += test_apps/os/heap_alloc_demo
subdir-app-y      += test_apps/pm_i2c_demo/pm_i2c_master
subdir-app-y      += test_apps/pm_i2c_demo/pm_i2c_slave
subdir-app-y      += test_apps/jsmn
subdir-app-y      += test_apps/json_parser

subdir-app-y      += wifi_driver_demo
subdir-app-y      += wlan/wlan_uap
subdir-app-y      += wlan/wlan_uap_with_custom_auto_channel
subdir-app-y      += wlan/wlan_uap_power_save
subdir-app-y      += wlan/wm_demo
subdir-app-y      += wlan/wlan_11d
subdir-app-y      += wlan/wlan_mac
subdir-app-y      += wlan/wlan_eu_cert
subdir-app-y      += wlan/wlan_cal

subdir-app-y      += wlan/uap_prov
subdir-app-y      += wlan/wlan_sniffer
subdir-app-y      += wlan/wlan_trpc_demo
subdir-app-y      += wlan/wlan_frame_inject_demo
subdir-app-y	  += wlan/wlan_mgmt_frame_demo

subdir-app-$(CONFIG_WiFi_8801)       += wlan/wlan_low_power
subdir-app-$(CONFIG_CPU_MW300)       += wlan/wlan_power_save
subdir-app-$(CONFIG_CPU_MW300)       += wlan/wlan_pdn
subdir-app-$(CONFIG_P2P)             += wlan/p2p_demo
subdir-app-$(CONFIG_P2P)             += wlan/raw_p2p_demo

subdir-app-y      += tutorials


ifeq (y,$(CONFIG_WPA2_ENTP))
subdir-app-y      := wlan/wpa2_enterprise
subdir-app-y      += test_apps/qa1
subdir-app-y      += test_apps/qa2
endif

subdir-y += $(subdir-app-y)
