# Copyright (C) 2008-2016 Marvell International Ltd.
# All Rights Reserved.

exec-y += hello_world
hello_world-objs-y := src/main.c

# Application can specify all supported toolchains,
# using which it can be compiled
# If this is variable is not defined,
# default toolchain used to compile is gcc
hello_world-supported-toolchain-y := arm_gcc iar

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
