# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += deepsleep_pm4
deepsleep_pm4-objs-y := src/main.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

deepsleep_pm4-supported-toolchain-y := arm_gcc iar
