# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += pdn_pm4
pdn_pm4-objs-y := src/main.c

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

pdn_pm4-supported-toolchain-y := arm_gcc iar
