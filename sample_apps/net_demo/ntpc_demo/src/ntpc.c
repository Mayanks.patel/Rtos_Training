/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */

#include "lwip/sockets.h"
#include "lwip/netdb.h"
#include <wmtime.h>
#include <wm_net.h>
#include <string.h>
#include <wm_os.h>

/*
 * Convert time to unix standard time NTP is number of seconds since 0000
 * UT on 1 January 1900 unix time is seconds since 0000 UT on 1 January
 * 1970 There has been a trend to add a 2 leap seconds every 3 years.
 * Leap seconds are only an issue the last second of the month in June and
 * December if you don't try to set the clock then it can be ignored but
 * this is importaint to people who coordinate times with GPS clock sources.
 */
#define EPOCH_TIME_IN_SECS 2208988800U

#define NTP_RESP_TIMEOUT 2

#define RECV_BUF_SIZE 100

/*NTP is port 123*/
#define NTP_PORT 123

#define DEFAULT_MAX_XCHANGE 5

static int ntp_time_get(const char *hostname,
			uint32_t *serv_rx_ts,
			uint32_t *serv_tx_ts)
{

	/* Get the  host address from input name */
	struct hostent *hostinfo = gethostbyname(hostname);
	if (!hostinfo) {
		wmprintf("gethostbyname Failed\r\n");
		return -WM_FAIL;
	}

	/* socket for ntp */
	int ntp_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (ntp_sockfd < 0)
		return -WM_FAIL;

	int one = 1;
	int rv = setsockopt(ntp_sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&one,
			    sizeof(int));

	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(NTP_PORT);
	server_addr.sin_addr = *((struct in_addr *) hostinfo->h_addr);

	/*
	 * build a message.The message contains all zeros except
	 * for a one in the protocol version field
	 * it should be a total of 48 bytes long
	 */

	unsigned char msg[48] = {010, 0, 0, 0, 0, 0, 0, 0, 0};
	rv = sendto(ntp_sockfd, msg, sizeof(msg), 0,
		    (struct sockaddr *) &server_addr,
		    sizeof(server_addr));
	if (rv == -1) {
		wmprintf("Send Data failed\r\n");
		net_close(ntp_sockfd);
		return -WM_FAIL;
	}

	fd_set readfds;
	FD_ZERO(&readfds);
	FD_SET(ntp_sockfd, &readfds);

	struct timeval timeout;
	memset(&timeout, 0x00, sizeof(struct timeval));
	timeout.tv_sec = NTP_RESP_TIMEOUT;

	/* Wait for response from NTP server */
	rv = select(ntp_sockfd + 1, &readfds,
		    NULL, NULL,
		    &timeout);
	if (rv <= 0) {
		wmprintf("ntp: no resp received\r\n");
		net_close(ntp_sockfd);
		return -WM_FAIL;
	}

	/* get the data back */
	if (!FD_ISSET(ntp_sockfd, &readfds)) {
		wmprintf("ntp: no read resp received\r\n");
		net_close(ntp_sockfd);
		return -WM_FAIL;
	}

	uint32_t *buf = os_mem_calloc(RECV_BUF_SIZE);
	if (!buf) {
		net_close(ntp_sockfd);
		return -WM_E_NOMEM;
	}

	rv = recv(ntp_sockfd, buf, RECV_BUF_SIZE, MSG_DONTWAIT);
	if (rv <= 0) {
		wmprintf("ntp: recv failed\r\n");
		os_mem_free(buf);
		net_close(ntp_sockfd);
		return -WM_FAIL;
	}

	/* 10th word has the time in seconds */
	*serv_tx_ts = ntohl(buf[10]);
	*serv_rx_ts = ntohl(buf[8]);

	os_mem_free(buf);
	close(ntp_sockfd);

	return WM_SUCCESS;
}

int ntpc_sync(const char *ntp_server)
{
	int rv;
	uint32_t client_tx_ts, serv_tx_ts, serv_rx_ts, rtt, final_time;
	if (!ntp_server)
		return -WM_E_INVAL;

	client_tx_ts = wmtime_time_get_posix();
	rv = ntp_time_get(ntp_server,
			&serv_rx_ts,
			&serv_tx_ts);

	if (rv != WM_SUCCESS)
		return rv;

	rtt = (wmtime_time_get_posix() - client_tx_ts) -
		(serv_tx_ts - serv_rx_ts);

	final_time = serv_tx_ts - (rtt >> 1) ;

	wmtime_time_set_posix(final_time - EPOCH_TIME_IN_SECS);

	return rv;
}

