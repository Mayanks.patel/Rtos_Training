# Copyright (C) 2008-2016 Marvell International Ltd.
# All Rights Reserved.

exec-cpp-y += hello_world_cpp

hello_world_cpp-objs-y := \
		src/main.c \
		src/wmsdk-cpp-helper.cc

