
exec-y += dsp_matrix_demo
ifneq ($(CONFIG_CMSIS_DSPLIB),y)
$(error "Please enable CONFIG_CMSIS_DSPLIB option using menuconfig")
endif


dsp_matrix_demo-objs-y := src/main.c src/math_helper.c src/arm_matrix_example_f32.c

dsp_matrix_demo-cflgs-y += -Isrc/

dsp_matrix_demo-prebuilt-libs-y += -lm


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
