exec-y += dsp_fft_bin_demo
ifneq ($(CONFIG_CMSIS_DSPLIB),y)
$(error "Please enable CONFIG_CMSIS_DSPLIB option using menuconfig")
endif

dsp_fft_bin_demo-objs-y := \
		src/main.c \
		src/arm_fft_bin_data.c \
		src/arm_fft_bin_example_f32.c

dsp_fft_bin_demo-prebuilt-libs-y += -lm


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
