

ifneq ($(CONFIG_CMSIS_DSPLIB),y)
$(error "Please enable CONFIG_CMSIS_DSPLIB option using menuconfig")
endif

exec-y += dsp_dotproduct_demo
dsp_dotproduct_demo-objs-y := \
		src/main.c \
		src/arm_dotproduct_example_f32.c

dsp_dotproduct_demo-prebuilt-libs-y += -lm


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript
