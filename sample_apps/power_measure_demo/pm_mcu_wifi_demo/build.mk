# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += pm_mcu_wifi_demo

pm_mcu_wifi_demo-objs-y   := src/main.c
pm_mcu_wifi_demo-cflags-y := -I$(d)/src

#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

pm_mcu_wifi_demo-supported-toolchain-y := arm_gcc iar
