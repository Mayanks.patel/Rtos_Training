# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += pm_mw300_demo
pm_mw300_demo-objs-y := src/pm_mw300_demo.c

pm_mw300_demo-linkerscript-y := $(d)/src/pm_mw300_demo.ld



#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
