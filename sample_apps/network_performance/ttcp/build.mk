# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

exec-y += ttcp
ttcp-objs-y   := src/main.c
ttcp-cflags-y := \
		-I$(d)/src \
		-DAPPCONFIG_DEBUG_ENABLE=1


#Application specific entities can be specified as follows
#<app-name>-board-y := /path/to/boardfile
#<app-name>-linkerscript-y := /path/to/linkerscript

ttcp-supported-toolchain-y := arm_gcc iar
