/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 */

/*
 * Simple application using TTCP CLIs to measure the network throughput.
 *
 * Summary:
 *
 * This application tests TCP/UDP connections for network throughput
 * measurement.
 *
 * Description:
 *
 * The application is written using Application Framework that
 * simplifies development of WLAN networking applications.
 *
 * WLAN Initialization:
 *
 * When the application framework is started, it starts up the WLAN
 * sub-system and initializes the network stack. The app receives the event
 * when the WLAN subsystem has been started and initialized.
 *
 * Application registers TTCP CLIs so that user can measure the TCP/UDP
 * transmit and receive throughput.
 *
 * Equipments required:
 * 1. 88MC200/88MW300 board (called DUT henceforth)
 * 2. Linux machine with Fedora or Ubuntu installation
 * 3. Wireless access point
 * 4. Shielded environment (suggested but optional)
 *
 * Tools used:
 * The current application has a ttcp tool to get the network performance
 * numbers as described below. TTCP should be available on your Linux machine.
 * If you have a fedora machine, you can install TTCP as: yum install ttcp.
 *
 * ttcp options:
 *		-t Transmit
 *		-r Receive
 *		-4 use IPv4
 *		-l length of bufs read from or written to network (default 8192)
 *		-u use UDP instead of TCP
 *		-p port number to send to or listen at (default 5001)
 *		-s	-t: source a pattern to network
 *			-r: sink (discard) all data from network
 *		-f X
 *		format for rate: k,K = kilo(bit,byte); m,M = mega; g,G = giga
 *
 *	Options specific to -t:
 *		-n number of source bufs written to network (default 2048)
 *
 * Setup:
 * 1. Start the wireless access point in Open mode(No WPA/WPA2 security).
 * 2. Flash this application binary.
 * 3. Provision the 88MC200/88MW300 board to the wireless access point.
 * 4. Connect a Linux machine to the same access point through wired network.
 * 5. Use following commands to measure network throughput.
 *
 * Measuring TCP receive performance
 * Command on DUT:
 * ttcp -r -f m
 * Command on Linux machine:
 * ttcp -t -4 -s -n 10000 -l 1460 -f m <DUT IP address>
 *
 * Measuring TCP transmit performance
 * Command on DUT:
 * ttcp -t -f m -n 10000 -l 1460 <Linux machines IP address>
 * Command on Linux machine:
 * ttcp -r -f m -s
 *
 * Measuring UDP receive performance
 * Command on DUT:
 * ttcp -r -f m -u
 * Command on Linux machine:
 * ttcp -t -4 -s -l 1460 -n 100 -f m -u <DUT IP address>
 *
 * Measuring UDP transmit performance
 * Command on DUT:
 * ttcp -t -f m -l 1460 -n 100 -u <Linux machines IP address>
 * Command on Linux machine:
 * ttcp -r -f m -u -s
 *
 */

#include <wm_os.h>
#include <app_framework.h>
#include <wmstdio.h>
#include <wm_net.h>
#include <mdev_gpio.h>
#include <appln_cb.h>
#include <appln_dbg.h>
#include <cli.h>
#include <psm-utils.h>
#include <ttcp.h>
#include <critical_error.h>

#define MAX_RETRY_TICKS 50

/* This function is defined for handling critical error.
 * For this application, we just stall and do nothing when
 * a critical error occurs.
 */
void critical_error(int crit_errno, void *data)
{
	dbg("Critical Error %d: %s\r\n", crit_errno,
			critical_error_msg(crit_errno));
	while (1)
		;
	/* do nothing -- stall */
}

/*
 * Handler invoked when WLAN subsystem is ready.
 *
 */
void event_wlan_init_done(void *data)
{
	int ret = -WM_FAIL;
	int provisioned;

	/* We receive provisioning status in data */
	provisioned = (int)data;

	dbg("AF_EVT_WLAN_INIT_DONE provisioned=%d", provisioned);

	if (provisioned) {
		app_sta_start();
	} else {
		dbg("This device is not configured. Please set and reboot:");
		dbg("network.ssid=ssid");
		dbg("network.security=0");
		dbg("network.configured=1");
		dbg("For dynamic ip address assignment:");
		dbg("network.lan=DYNAMIC");
		dbg("For static ip address assignment:");
		dbg("network.lan=STATIC");
		dbg("network.ip_address=<ip_address>");
		dbg("network.gateway=<gateway>");
		dbg("network.subnet_mask=<subnet_mask>");
	}

	/*
	 * Initialize CLI Commands for some of the modules:
	 *
	 * -- psm:  allows user to check data in psm partitions
	 * -- wlan: allows user to explore basic wlan functions
	 * -- ttcp: allows user to test TCP/UDP TX/RX throughput
	 */

	ret = psm_cli_init(sys_psm_get_handle(), NULL);
	if (ret != WM_SUCCESS)
		dbg("Error: psm_cli_init failed");
	ret = wlan_cli_init();
	if (ret != WM_SUCCESS)
		dbg("Error: wlan_cli_init failed");
	ret = ttcp_init();
	if (ret != WM_SUCCESS)
		dbg("Error: ttcp_init failed");
}

void event_normal_connecting(void *data)
{
	dbg("Connecting to Home Network");
}

/* Event: AF_EVT_NORMAL_CONNECTED
 *
 * Station interface connected to home AP.
 *
 * Network dependent services can be started here. Note that these
 * services need to be stopped on disconnection and
 * reset-to-provisioning event.
 */
void event_normal_connected(void *data)
{
	char ip[16];

	app_network_ip_get(ip);
	dbg("Connected to provisioned network with ip address =%s", ip);

	dbg("This device is configured. Please use following commands:");
	dbg("");
	dbg("Measuring TCP receive performance");
	dbg("Command on DUT:");
	dbg("ttcp -r -f m");
	dbg("Command on Linux machine:");
	dbg("ttcp -t -4 -s -n 10000 -l 1460 -f m <DUT IP address>");
	dbg("");
	dbg("Measuring TCP transmit performance");
	dbg("Command on DUT:");
	dbg("ttcp -t -f m -n 10000 -l 1460 <Linux machines IP address>");
	dbg("Command on Linux machine:");
	dbg("ttcp -r -f m -s");
	dbg("");
	dbg("Measuring UDP receive performance");
	dbg("Command on DUT:");
	dbg("ttcp -r -f m -u");
	dbg("Command on Linux machine:");
	dbg("ttcp -t -4 -s -l 1460 -n 100 -f m -u <DUT IP address>");
	dbg("");
	dbg("Measuring UDP transmit performance");
	dbg("Command on DUT:");
	dbg("ttcp -t -f m -l 1460 -n 100 -u <Linux machines IP address>");
	dbg("Command on Linux machine:");
	dbg("ttcp -r -f m -u -s");
}

/* Event handler for AF_EVT_NORMAL_DISCONNECTED - Station interface
 * disconnected.
 * Stop the network services which need not be running in disconnected mode.
 */
void event_normal_user_disconnect(void *data)
{
	dbg("Disconnected from Home Network");
}

void event_normal_link_lost(void *data)
{
	dbg("Link lost from Home Network");
}

/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_NORMAL_CONNECTING:
		event_normal_connecting(data);
		break;
	case AF_EVT_NORMAL_CONNECTED:
		event_normal_connected(data);
		break;
	case AF_EVT_NORMAL_LINK_LOST:
		event_normal_link_lost(data);
		break;
	case AF_EVT_NORMAL_USER_DISCONNECT:
		event_normal_user_disconnect(data);
		break;
	default:
		break;
	}

	return 0;
}

static void modules_init()
{
	int ret;
	/*
	 * Initialize wmstdio prints
	 */
	ret = wmstdio_init(UART0_ID, 0);

	if (ret != WM_SUCCESS) {
		dbg("Error: wmstdio_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	/*
	 * Initialize CLI Commands
	 */
	ret = cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: cli_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}
	/* Initialize time subsystem.
	 *
	 * Initializes time to 1/1/1970 epoch 0.
	 */
	ret = wmtime_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: wmtime_init failed");
		critical_error(-CRIT_ERR_APP, NULL);
	}

	return;
}

int main()
{
	modules_init();

	dbg("Build Time: " __DATE__ " " __TIME__ "");

	wifi_set_packet_retry_count(MAX_RETRY_TICKS);

	/* Start the application framework */
	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		dbg("Failed to start application framework");
		critical_error(-CRIT_ERR_APP, NULL);
	} else {
		dbg("app_framework started");
	}
	return 0;
}
