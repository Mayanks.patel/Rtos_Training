/*
 * Copyright 2008-2016, Marvell International Ltd.
 * All Rights Reserved.
 *
 * Derived from:
 * https://sourceforge.net/p/iperf2/code-0/HEAD/tree/iperf2/COPYING
 */
#include <stdlib.h>

#include <wm_os.h>
#include <wmtime.h>
#include <wmstdio.h>
#include <unistd.h>
#include <sys/time.h>

int gettimeofday(struct timeval *tv, void *tz)
{
	if (tv == NULL)
		return -1;

	tv->tv_sec = wmtime_time_get_posix();
	/* fixme; This could be improved */
	tv->tv_usec = 0;

	if (tz)
		memset(tz, 0x00, sizeof(struct timezone));

	return 0;// Unix value for successful
}

void exit(int status)
{
			/*	wmprintf("exit called from %p. Status: %d\r\n", __builtin_return_address(0),
		 status); */

	os_enter_critical_section();
	while(1) {
		os_thread_sleep(1000);
	}
}

int usleep(useconds_t usec)
{
	int ms = usec / 1000;
	os_thread_sleep(ms);
	return 0;
}
