# Copyright (C) 2008-2016 Marvell International Ltd.
# All Rights Reserved.

exec-cpp-y += iperf

# Core
iperf-objs-y := \
	src/Client.cpp src/Extractor.c src/Launch.cpp 	src/List.cpp src/Listener.cpp \
	src/Locale.c src/PerfSocket.cpp src/ReportCSV.c src/ReportDefault.c \
	src/Reporter.c src/Server.cpp src/Settings.cpp src/SocketAddr.c src/gnu_getopt.c \
	src/gnu_getopt_long.c src/main.cpp src/service.c src/sockets.c src/stdio.c \
	src/tcp_window_size.c

# Compat
iperf-objs-y += compat/Thread.c compat/delay.cpp \
	compat/error.c compat/string.c # compat/gettimeofday.c compat/inet_ntop.c compat/inet_pton.c \
	compat/signal.c compat/snprintf.c

# WMSDK interface
iperf-objs-y += src/wmsdk_main.c \
	compat/wmsdk-iperf.c  \
	compat/wmsdk-cpp-helper.cc

iperf-cflags-y := -I$(d) -I$(d)/src -I$(d)/include -DHAVE_CONFIG_H -Wno-sequence-point -Wno-char-subscripts



