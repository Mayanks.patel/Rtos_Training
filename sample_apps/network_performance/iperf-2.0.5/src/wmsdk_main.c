/*
 *  Copyright (C) 2008-2016, Marvell International Ltd.
 *  All Rights Reserved.
 *
 *  Derived from:
 *  https://sourceforge.net/p/iperf2/code-0/HEAD/tree/iperf2/COPYING
 */

/*
 * iperf demo
 *
 */
#include <wm_os.h>
#include <app_framework.h>
#include <wmtime.h>
#include <psm-utils.h>
#include <appln_cb.h>
#include <appln_dbg.h>
#include <cli.h>
#include <wmstdio.h>
#include <wm_net.h>
#include <led_indicator.h>
#include <board.h>
#include <wmtime.h>
#include <ttcp.h>

#include <work-queue.h>
#include <system-work-queue.h>


void iperf_cli_init(void);

/*
 * Event: INIT_DONE
 *
 * The application framework is initialized.
 *
 * The data field has information passed by boot2 bootloader
 * when it loads the application.
 *
 */
static void event_init_done(void *data)
{
	struct app_init_state *state;

	state = (struct app_init_state *)data;

	dbg("Event: INIT_DONE");
	dbg("Factory reset bit status: %d", state->factory_reset);
	dbg("Booting from backup firmware status: %d", state->backup_fw);
	dbg("Previous reboot cause: %u", state->rst_cause);

}

/*
 * Handler invoked on WLAN_INIT_DONE event.
 *
 */
static void event_wlan_init_done(void *data)
{
	int ret;
	struct wlan_network network;
	int provisioned = (int)data;

	if (provisioned)
		app_sta_start();
	else {
#define DEF_NW_NAME          "station"
#define DEF_NW_SSID          "marvell"
#define DEF_NW_PASSPHRASE    "marvellwm"
#define DEF_NW_SECURITY      WLAN_SECURITY_WPA2
		wmprintf("\n\rDevice is not provisioned\n\r");
		wmprintf("Connecting using hard-coded settings:\n\r"
			 "AP SSID: %s\n\r"
			 "Passphrase: %s\n\n\r", DEF_NW_SSID,
			 DEF_NW_PASSPHRASE);

		memset(&network, 0, sizeof(network));
		strncpy(network.name, DEF_NW_NAME, sizeof(network.name));
		strncpy(network.ssid, DEF_NW_SSID, sizeof(network.ssid));
		strncpy(network.security.psk, DEF_NW_PASSPHRASE,
			sizeof(network.security.psk));
		network.security.psk_len = strlen(network.security.psk);
		network.security.type = DEF_NW_SECURITY;
		network.ip.ipv4.addr_type = ADDR_TYPE_DHCP;
		network.type = WLAN_BSS_TYPE_STA;
		network.role = WLAN_BSS_ROLE_STA;

		app_sta_start_by_network(&network);
	}
	/*
	 * Initialize CLI Commands for some of the modules:
	 *
	 * -- wlan: allows user to explore basic wlan functions
	 */
	ret = wlan_cli_init();
	if (ret != WM_SUCCESS)
		dbg("Error: wlan_cli_init failed");

	ret = psm_cli_init(sys_psm_get_handle(), NULL);
	if (ret != WM_SUCCESS)
		dbg("Error: wlan_cli_init failed");

#if 0
	ret = ttcp_init();
	if (ret != WM_SUCCESS)
		dbg("Error: ttcp_init failed");
#endif
}


/*
 * Event: CONNECTING
 *
 * We are attempting to connect to the Home Network
 *
 * This is just a transient state as we will either get
 * CONNECTED or have a CONNECTION/AUTH Failure.
 *
 */
static void event_normal_connecting(void *data)
{
	dbg("Connecting to Home Network");
}

#if 0
void cmd_perf_main( int argc, char **argv );
static int run_command(void *param)
{
	char *argv[10];
	/* TCP client */
	/* argv[0] = "cmd-name"; */
	/* argv[1] = "-c"; */
	/* argv[2] = "10.31.130.30"; */
	/* argv[3] = NULL; */
	/* cmd_perf_main(3, argv); */

	/* UDP client */
	argv[0] = "cmd-name";
	argv[1] = "-c";
	argv[2] = "10.31.130.30";
	argv[3] = "-u";
	argv[4] = NULL;
	cmd_perf_main(4, argv);

	/* TCP server */
	/* argv[0] = "cmd-name"; */
	/* argv[1] = "-s"; */
	/* argv[2] = NULL; */
	/* cmd_perf_main(2, argv); */

	wmprintf("WQ JOB DONE\r\n");
	return 0;
}
#endif

/* Event: AF_EVT_NORMAL_CONNECTED
 *
 * Station interface connected to home AP.
 *
 * Network dependent services can be started here. Note that these
 * services need to be stopped on disconnection and
 * reset-to-provisioning event.
 */
static void event_normal_connected(void *data)
{
	char ip[16];

	app_network_ip_get(ip);
	dbg("Connected to Home Network with IP address =%s", ip);

	sys_work_queue_init();
#if 0
	static wq_handle_t wq_handle;
	wq_handle = sys_work_queue_get_handle();
	wq_job_t wq_job;
	memset(&wq_job, 0x00, sizeof(wq_job_t));
	wq_job.job_func = run_command;
	wq_job.initial_delay_ms = 1000;
	work_enqueue(wq_handle, &wq_job, NULL);
#endif

	iperf_cli_init();
}

/*
 * Event: CONNECT_FAILED
 *
 * We attempted to connect to the Home AP, but the connection did
 * not succeed.
 *
 * This typically indicates:
 *
 * -- The access point could not be found.
 * -- We did not get a valid IP address from the AP
 *
 */
static void event_connect_failed()
{
	dbg("Application Error: Connection Failed");
}


/*
 * Event: LINK LOSS
 *
 * We lost connection to the AP.
 *
 * The App Framework will attempt to reconnect. We don't
 * need to do anything here.
 */
static void event_normal_link_lost(void *data)
{
	dbg("Link Lost");
}


/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_INIT_DONE:
		event_init_done(data);
		break;
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_NORMAL_CONNECTING:
		event_normal_connecting(data);
		break;
	case AF_EVT_NORMAL_CONNECTED:
		event_normal_connected(data);
		break;
	case AF_EVT_NORMAL_CONNECT_FAILED:
		event_connect_failed(data);
		break;
	case AF_EVT_NORMAL_LINK_LOST:
		event_normal_link_lost(data);
		break;
	default:
		break;
	}

	return 0;
}

static void modules_init()
{
	int ret;

	ret = wmstdio_init(UART0_ID, 0);
	if (ret != WM_SUCCESS) {
		while (1)
			;
		/* do nothing -- stall */
	}

	ret = cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: cli_init failed");
		while (1)
			;
		/* do nothing -- stall */
	}
	/* Initialize time subsystem.
	 *
	 * Initializes time to 1/1/1970 epoch 0.
	 */
	ret = wmtime_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: wmtime_init failed");
		while (1)
			;
		/* do nothing -- stall */
	}

	/*
	 * Register Time CLI Commands
	 */
	ret = wmtime_cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: wmtime_cli_init failed");
		while (1)
			;
		/* do nothing -- stall */
	}

	return;
}

#define MAX_RETRY_TICKS 50

int main()
{
	modules_init();

	dbg("Build Time: " __DATE__ " " __TIME__ "");

	wifi_set_packet_retry_count(MAX_RETRY_TICKS);

	/* Start the application framework */
	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		dbg("Failed to start application framework");
		while (1)
			;
		/* do nothing -- stall */
	}
	return 0;
}
